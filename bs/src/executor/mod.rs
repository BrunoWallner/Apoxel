mod context;
mod functions;
pub mod value;
pub mod error;

use crate::parser::Ast;
pub use value::IntoValue;

use context::Context;

use self::{error::Error, value::Value};

#[derive(Clone, Debug)]
pub struct Executor {
    pub ast: Ast,
    pub context: Context,
}
impl Executor {
    pub fn new(ast: Ast) -> Result<Self, Error> {
        let context = Context::build(&ast)?;
        Ok(Self { ast, context })
    }

    pub fn execute(
        &mut self,
        entry: &str,
        arguments: Vec<&dyn IntoValue>,
    ) -> Result<Option<Value>, Error> {
        let values = arguments.into_iter().map(|v| v.into_value()).collect();
        self.context.call_function(entry, values)
    }
}
