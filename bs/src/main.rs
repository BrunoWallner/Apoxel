use bs::executor::Executor;
use bs::lexer::tokenize;
use bs::parser::{self, Ast};
use std::fs;

fn main() {
    let input = fs::read_to_string("./example.bs").unwrap();
    let tokens = tokenize(&input);
    // println!("{:?}", tokens);
    let ast = Ast::new(parser::parse(tokens));
    // println!("{:#?}", ast);
    let mut executor = Executor::new(ast).unwrap();
    // let result = executor.execute("main", vec![Value::Data(DataType::Num(0.1))]);
    let result = executor.execute("main", vec![&["hello", "world"]]);
    match result {
        Ok(r) => println!("{:?}", r),
        Err(e) => println!("{}", e.format_with(&input))
    }
}
