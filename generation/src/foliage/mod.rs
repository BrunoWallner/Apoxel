pub mod cacti;
pub mod trees;

pub use cacti::get_cactus;
pub use trees::get_tree;

pub fn generate(seed: u32) {
    trees::generate(seed);
    cacti::generate(seed);
}
