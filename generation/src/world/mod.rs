mod biome;
pub mod noise;

use common::blocks::Block;
use common::chunk::{Chunk, SuperChunk, BASE_CHUNK_SIZE};
use common::Coord;

// use crate::foliage;

// const WATER_HEIGHT: i64 = 50;
// const BASE_HEIGHT: i64 = 30;
// const BASE_DEPTH: i64 = -10;

pub fn generate(chunk: Chunk, seed: u32, coord: Coord) -> SuperChunk {
    // let biome_noise = noise::BiomeNoise::new(seed);
    // let terrain_noise = noise::TerrainNoise::new(seed);
    let cave_noise = noise::CaveNoise::new(seed);

    let mut chunks: SuperChunk = SuperChunk::new(chunk, coord);

    for x in 0..BASE_CHUNK_SIZE {
        for y in 0..BASE_CHUNK_SIZE {
            for z in 0..BASE_CHUNK_SIZE {
                let coord = [
                    x as i64 + coord[0] * BASE_CHUNK_SIZE as i64,
                    y as i64 + coord[1] * BASE_CHUNK_SIZE as i64,
                    z as i64 + coord[2] * BASE_CHUNK_SIZE as i64,
                ];
                if cave_noise.is_cave(coord) {
                    // let color = if (x + y + z) % 2 == 0 {
                    //     255
                    // } else {
                    //     0
                    // };

                    chunks.place(
                        BASE_CHUNK_SIZE,
                        [x as i64, y as i64, z as i64],
                        Block::Color(common::prelude::Color::new(x as u8 * 2 + 32, y as u8  * 2 + 32, z as u8 * 2 + 32)),
                    );
                }
            }
        }
    }

    // let mut chunks: SuperChunk = SuperChunk::new(chunk, coord);
    // for x in 0..BASE_CHUNK_SIZE {
    //     for z in 0..BASE_CHUNK_SIZE {
    //         let global_x = (coord[0] * BASE_CHUNK_SIZE as i64) + x as i64;
    //         let global_z = (coord[2] * BASE_CHUNK_SIZE as i64) + z as i64;

    //         let biome = biome::get([global_x, global_z], &biome_noise, &terrain_noise);
    //         let height = biome.height;
    //         let block = biome.blocks[0];

    //         /* rest of generation */
    //         let global_y = height + BASE_HEIGHT;
    //         let local_xyz = [
    //             global_x - BASE_CHUNK_SIZE as i64 * coord[0],
    //             global_y - BASE_CHUNK_SIZE as i64 * coord[1],
    //             global_z - BASE_CHUNK_SIZE as i64 * coord[2],
    //         ];

    //         // if check_position(BASE_CHUNK_SIZE as i64, &local_xyz) {
    //         //     if local_xyz[0] == 32 && local_xyz[2] == 32 {
    //         //         chunks.merge(&foliage::get_tree(0.5), [0.0, 0.85, 0.0], None);
    //         //     }
    //         // }

    //         // biome terrain
    //         for y in BASE_DEPTH..=global_y {
    //             let local_xyz = [
    //                 global_x - BASE_CHUNK_SIZE as i64 * coord[0],
    //                 y - BASE_CHUNK_SIZE as i64 * coord[1],
    //                 global_z - BASE_CHUNK_SIZE as i64 * coord[2],
    //             ];

    //             let cave = cave_noise.is_cave([global_x, global_y + y, global_z]);

    //             if check_position(BASE_CHUNK_SIZE as i64, &local_xyz) && !cave {
    //                 chunks.place(BASE_CHUNK_SIZE, local_xyz, block)
    //             }
    //         }

    //         // Walter
    //         for global_y in global_y..WATER_HEIGHT {
    //             let local_xyz = [
    //                 global_x - BASE_CHUNK_SIZE as i64 * coord[0],
    //                 global_y - BASE_CHUNK_SIZE as i64 * coord[1],
    //                 global_z - BASE_CHUNK_SIZE as i64 * coord[2],
    //             ];
    //             if check_position(BASE_CHUNK_SIZE as i64, &local_xyz) {
    //                 chunks.place(BASE_CHUNK_SIZE, local_xyz, Block::None)
    //             }
    //         }
    //     }
    // }

    chunks.pack();
    chunks
}
// pub fn generate(chunk: Chunk, seed: u32, coord: Coord) -> SuperChunk {
//     let mut chunks = SuperChunk::new(chunk, coord);

//     let rock = include_bytes!("./rock.vox");
//     let rock = storage::voxel::parse(rock).unwrap();

//     if coord[1] != 1 {
//         return chunks;
//     };

//     for x in 0..BASE_CHUNK_SIZE as i64 {
//         for z in 0..BASE_CHUNK_SIZE as i64 {
//             let y = 16;
//             chunks.place(BASE_CHUNK_SIZE, [x, y, z], Block::Color(Color::splat(200)));

//             if x == 16 && z == 16 {
//                 chunks.place_chunk(&rock, [0.0, 0.5, 0.0], None, Some(BASE_CHUNK_SIZE));
//             }
//         }
//     }

//     chunks
// }

// pub fn generate(chunk: Chunk, seed: u32, coord: Coord) -> SuperChunk {
//     let mut rng = rand::thread_rng();
//     let cave_noise = noise::CaveNoise::new(seed);
//     let mut chunks = SuperChunk::new(chunk, coord);

//     let rock = include_bytes!("./rock.vox");
//     let rock = storage::voxel::parse(rock).unwrap();

//     let multidimensional_iterator = (0..BASE_CHUNK_SIZE).flat_map(move |x| {
//         (0..BASE_CHUNK_SIZE).flat_map(move |y| (0..BASE_CHUNK_SIZE).map(move |z| (x, y, z)))
//     });

//     for (x, y, z) in multidimensional_iterator {
//         let global_coord = [
//             coord[0] * BASE_CHUNK_SIZE as i64 + x as i64,
//             coord[1] * BASE_CHUNK_SIZE as i64 + y as i64,
//             coord[2] * BASE_CHUNK_SIZE as i64 + z as i64,
//         ];
//         let local_coord = [x as i64, y as i64, z as i64];
//         if cave_noise.is_cave(global_coord) {
//             chunks.place(BASE_CHUNK_SIZE, local_coord, Block::None);

//             if rng.gen::<f32>() > 0.9 {
//                 for side in Side::all() {
//                     let offset = side.get_offset();
//                     let offset_coord = [
//                         global_coord[0] + offset[0] as i64,
//                         global_coord[1] + offset[1] as i64,
//                         global_coord[2] + offset[2] as i64,
//                     ];
//                     if !cave_noise.is_cave(offset_coord) {
//                         let offset = [
//                             local_coord[0] as f32 / BASE_CHUNK_SIZE as f32,
//                             local_coord[1] as f32 / BASE_CHUNK_SIZE as f32,
//                             local_coord[2] as f32 / BASE_CHUNK_SIZE as f32,
//                         ];
//                         chunks.place_chunk(&rock, offset, None, Some(BASE_CHUNK_SIZE));
//                     }
//                 }
//             }
//         } else {
//             chunks.place(
//                 BASE_CHUNK_SIZE,
//                 local_coord,
//                 Block::Color(Color::splat(100)),
//             )
//         }
//     }
//     chunks.pack();
//     chunks
// }

// fn check_position(resolution: i64, xyz: &[i64; 3]) -> bool {
//     let mut ok: bool = true;
//     for p in xyz {
//         if p >= &resolution || p < &0 {
//             ok = false;
//         }
//     }
//     ok
// }
