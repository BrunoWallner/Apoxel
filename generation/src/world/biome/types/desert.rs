use super::super::noise::Noise;
use super::Biome;
use super::VerticalBlocks;
use crate::foliage::cacti::get_cactus;
use common::blocks::Block;
use common::chunk::SuperChunk;

const AMPLITUDE: f64 = 10.0;
const HEIGHT: f64 = 2.0;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Desert;
impl Biome for Desert {
    fn height(&self, xz: [i64; 2], noise: &Noise) -> i64 {
        let coord = [xz[0] as f64, xz[1] as f64];
        ((noise.get_2d(coord, 100.0, 8, 0.0) + 0.5) * AMPLITUDE) as i64 + HEIGHT as i64
    }

    fn vertical_blocks(&self) -> VerticalBlocks {
        VerticalBlocks {
            primary: Block::Sand,
            secondary: Block::RedSand,
            primary_height: 2,
        }
    }

    fn structure(&self, xyz: [i64; 3], noise: &Noise) -> Option<SuperChunk> {
        let p = noise.get_2d_simple([xyz[0] as f64, xyz[2] as f64]);
        if p > 0.99 {
            Some(get_cactus(0.0))
        } else {
            None
        }
    }
}
