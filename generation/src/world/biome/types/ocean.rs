use super::super::noise::Noise;
use super::Biome;
use super::VerticalBlocks;
use common::blocks::Block;
use common::chunk::SuperChunk;

const AMPLITUDE: f64 = 0.0;
const HEIGHT: f64 = -30.0;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Ocean;
impl Biome for Ocean {
    fn height(&self, xz: [i64; 2], noise: &Noise) -> i64 {
        let coord = [xz[0] as f64, xz[1] as f64];
        ((noise.get_2d(coord, 100.0, 8, 0.0) + 0.5) * AMPLITUDE) as i64 + HEIGHT as i64
    }

    fn vertical_blocks(&self) -> VerticalBlocks {
        VerticalBlocks {
            primary: Block::Sand,
            secondary: Block::Stone,
            primary_height: 3,
        }
    }

    fn structure(&self, _xyz: [i64; 3], _noise: &Noise) -> Option<SuperChunk> {
        None
    }
}
