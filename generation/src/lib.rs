// INPORTANT: you MUST first call `foliage::generate()` before generating worlds
// this must only be done once at the start

pub mod foliage;
pub mod world;

pub use world::generate;
