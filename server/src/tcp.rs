use async_std::io;
use async_std::net::{TcpListener, ToSocketAddrs};
use common::io::{communicator, InputCommunicator, OutputCommunicator};
use std::net::SocketAddr;

pub struct Tcp {
    listener: TcpListener,
}
impl Tcp {
    pub async fn init<A: ToSocketAddrs>(addr: A) -> io::Result<Self> {
        let listener = TcpListener::bind(addr).await?;
        Ok(Self { listener })
    }

    // blockingly accept new client
    // returns tokio channels instead of the ones included in common, because of async
    pub async fn accept(&self) -> io::Result<((OutputCommunicator, InputCommunicator), SocketAddr)> {
        let (stream, addr) = self.listener.accept().await?;

        let communicator = communicator::init(stream);

        Ok((communicator, addr))
    }
}
