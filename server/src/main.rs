mod allocator;
mod io;
mod logger;

use async_std::main;
use colored::{Color, Colorize};

use crate::io::terminal::Terminal;

#[main]
async fn main() {
    // blocks and returns on Ctrl+C
    let logger = logger::Logger::new().unwrap();

    let _ = server::start().await;

    let mut terminal = Terminal::new().unwrap();
    terminal.run(&logger);
    terminal.exit();
    logger::ShutdownLogger::run_detached(logger);
    log::info!(
        "shutting down, do {} press {}",
        "NOT".color(Color::Red),
        "CTRL+C".color(Color::Cyan)
    );

    server::shut_down();

    // fixes weird problem of deletion of Shutdownlogger messages when normally exited
    std::process::exit(0);
}
