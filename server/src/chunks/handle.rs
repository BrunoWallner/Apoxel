use crate::shutdown;
use crate::{config, hooks::chunk::*};
use common::{channel::*, prelude::Block, types::Octree, Coord};
use generation::generate;
use std::time::Duration;
use std::{collections::HashSet, thread};
use storage::chunks::{ChunkStorage, StoredChunk};
use threadpool::ThreadPool;

const SHUTDOWN_CHECK_INTERVALL: Duration = Duration::from_secs(1);

pub fn init() {
    let hook = CHUNK_HOOK.clone();
    let event_receiver = hook.input.get_receiver().unwrap();
    let config = config::get();
    let (chunk_request_sender, chunk_request_receiver) = channel(None);
    init_chunk_generator(chunk_request_receiver, hook.input.clone());

    let Some((shutdown_receiver, shutdown_marker)) = shutdown::get() else {
        log::warn!("failed to initialize chunk handler");
        return
    };

    let mut chunk_storage = ChunkStorage::new(&config.chunks.save_path);
    let mut load_lock: HashSet<Coord> = HashSet::default();

    thread::spawn(move || loop {
        // check for shutdown event when no chunk events occur
        check_shutdown(&shutdown_receiver, &mut chunk_storage, &shutdown_marker);

        while let Some(chunk_event) = event_receiver.recv_timeout(SHUTDOWN_CHECK_INTERVALL) {
            // check if shutdown event
            check_shutdown(&shutdown_receiver, &mut chunk_storage, &shutdown_marker);

            match chunk_event {
                // it is assumed that the chunks in question are already generated
                ChunkInputEvent::GetChunks { chunks, sender } => {
                    for coord in chunks {
                        if let Some(chunk) = chunk_storage.get(&coord) {
                            let update_chunk = UpdateChunk {
                                chunk: chunk.chunk.clone(),
                                coord,
                                fully_loaded: chunk.fully_loaded,
                            };
                            let _ = sender.send(update_chunk);
                        }
                    }
                }

                // chunks can be ungenerated
                ChunkInputEvent::RequestChunks {
                    chunks,
                    optional_sender,
                } => {
                    for coord in chunks {
                        if let Some(chunk) = chunk_storage.get(&coord) {
                            if let Some(sender) = &optional_sender {
                                let update_chunk = UpdateChunk {
                                    chunk: chunk.chunk.clone(),
                                    coord,
                                    fully_loaded: chunk.fully_loaded,
                                };
                                let _ = sender.send(update_chunk);
                            }
                        } else {
                            // request async chunkgeneration
                            chunk_request_sender.send(coord).unwrap();
                        }
                    }
                }
                ChunkInputEvent::SuperChunkModified {
                    super_chunk,
                    from_generation,
                } => {
                    load_lock.remove(&super_chunk.coord);
                    // merge
                    for (s_c_coord, s_c) in super_chunk.chunks {
                        let fully_loaded: bool = s_c_coord == [0, 0, 0] && from_generation;

                        let absolute_coord = [
                            s_c_coord[0] + super_chunk.coord[0],
                            s_c_coord[1] + super_chunk.coord[1],
                            s_c_coord[2] + super_chunk.coord[2],
                        ];
                        // merge new chunk with existing one, or insert it
                        if let Some(stored_chunk) = chunk_storage.get_mut(&absolute_coord) {
                            if fully_loaded {
                                stored_chunk.fully_loaded = true;
                            }
                            if !from_generation {
                                stored_chunk.modified = true;
                            }
                            stored_chunk
                                .chunk
                                .merge_with_blacklist(&s_c, None, |block| block == Block::None);
                            stored_chunk.chunk.pack();
                        } else {
                            let mut stored_chunk = StoredChunk::new(s_c.clone());
                            stored_chunk.fully_loaded = fully_loaded;
                            chunk_storage.insert(absolute_coord, stored_chunk);
                        }

                        // send update
                        hook.output.send(ChunkOutputEvent::ChunkUpdate {
                            coord: absolute_coord,
                        })
                    }
                }
            }
        }
    });
}

fn check_shutdown(
    shutdown_receiver: &Receiver<()>,
    chunk_storage: &mut ChunkStorage,
    shutdown_marker: &shutdown::ShutdownHandle,
) {
    if shutdown_receiver.try_recv().is_ok() {
        chunk_storage.unload_all().ok();
        chunk_storage.save().ok();
        log::info!("saved all chunks to disk");
        shutdown_marker.mark_safe();
    }
}

fn init_chunk_generator(receiver: Receiver<Coord>, sender: Broadcast<ChunkInputEvent>) {
    let seed = config::get().chunks.seed;
    let threadpool = ThreadPool::new(4);
    thread::spawn(move || {
        while let Some(coord) = receiver.recv() {
            let sender = sender.clone();
            threadpool.execute(move || {
                let super_chunk = generate(Octree::default(), seed, coord);
                sender.send(ChunkInputEvent::SuperChunkModified {
                    super_chunk,
                    from_generation: true,
                })
            });
        }
    });
}
