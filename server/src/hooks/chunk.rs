use super::Hook;
use common::prelude::*;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref CHUNK_HOOK: Hook<ChunkInputEvent, ChunkOutputEvent> = {
        let input = Broadcast::new();
        let output = Broadcast::new();
        Hook { input, output }
    };
}

#[derive(Clone, Debug)]
pub struct UpdateChunk {
    pub chunk: Chunk,
    pub coord: Coord,
    pub fully_loaded: bool,
}

#[derive(Clone, Debug)]
pub enum ChunkInputEvent {
    // chunks that are not yet generated
    // they get notified via ChunkUpdate
    // and must then be fetched via GetChunks

    // if they are already generated and loaded they will not be notified via ChunkUpdate
    // they can be optionally received

    // Stage 1 of getting chunks
    RequestChunks {
        chunks: Vec<Coord>,
        optional_sender: Option<Sender<UpdateChunk>>,
    },
    // stage 2 of getting chunks
    GetChunks {
        chunks: Vec<Coord>,
        sender: Sender<UpdateChunk>,
    },
    SuperChunkModified {
        super_chunk: SuperChunk,
        from_generation: bool,
    },
}

#[derive(Clone, Debug)]
pub enum ChunkOutputEvent {
    ChunkUpdate { coord: Coord },
}
