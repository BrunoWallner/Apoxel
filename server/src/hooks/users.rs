use super::Hook;
use common::{channel::*, error::LoginError, PlayerCoord, Token, User};
use lazy_static::lazy_static;

lazy_static! {
    pub static ref USER_HOOK: Hook<UserInputEvent, UserOutputEvent> = {
        let input = Broadcast::new();
        let output = Broadcast::new();
        Hook { input, output }
    };
}

#[derive(Debug, Clone)]
pub enum UserInputEvent {
    Register {
        name: String,
        token: Sender<Option<Token>>,
    },
    Login {
        token: Token,
        success: Sender<Result<(), LoginError>>,
    },
    Logoff {
        token: Token,
        success: Sender<bool>,
    },
    GetUser {
        token: Token,
        user: Sender<Option<User>>,
    },
    ModUser {
        token: Token,
        mod_instruction: UserModInstruction,
    },
}

#[derive(Debug, Clone)]
pub enum UserModInstruction {
    Move(PlayerCoord),
}

#[derive(Debug, Clone)]
pub enum UserOutputEvent {}
