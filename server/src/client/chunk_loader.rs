use crate::config;
use crate::hooks::chunk::*;
use common::prelude::*;
use std::collections::BTreeSet;

pub struct ChunkLoader {
    communicator: OutputCommunicator,
    chunk_event_broadcast: Broadcast<ChunkInputEvent>,
    chunk_event_receiver: Receiver<ChunkOutputEvent>,
    chunk_position: Coord,
    loaded_chunks: BTreeSet<Coord>,
    load_lock: BTreeSet<Coord>,
    chunk_load_receiver: Receiver<UpdateChunk>,
    chunk_load_sender: Sender<UpdateChunk>,
    render_distance: u16,
    initial_load: bool,
}
impl ChunkLoader {
    pub fn new(communicator: OutputCommunicator) -> Self {
        let chunk_hook = CHUNK_HOOK.clone();
        let chunk_event_receiver = chunk_hook.output.get_receiver().unwrap();
        let (chunk_load_sender, chunk_load_receiver) = channel(None);
        let render_distance = config::get().chunks.render_distance;

        Self {
            communicator,
            chunk_event_broadcast: chunk_hook.input,
            chunk_event_receiver,
            chunk_position: [0, 0, 0],
            loaded_chunks: BTreeSet::default(),
            load_lock: BTreeSet::default(),
            chunk_load_receiver,
            chunk_load_sender,
            render_distance,
            initial_load: true,
        }
    }

    pub async fn update(&mut self, position: PlayerCoord) {
        // request chunks
        let position = &[position[0] as i64, position[1] as i64, position[2] as i64];
        let new_chunk_position = get_chunk_coords(1, position).0;
        // if moved entire chunk request_chunks
        if self.chunk_position != new_chunk_position || self.initial_load {
            self.initial_load = false;
            self.chunk_position = new_chunk_position;
            self.request_chunks();
        }

        self.receive_chunks().await;

        let mut chunks_to_get: Vec<Coord> = Vec::new();
        while let Ok(chunk_event) = self.chunk_event_receiver.try_recv() {
            match chunk_event {
                ChunkOutputEvent::ChunkUpdate { coord } => {
                    self.load_lock.remove(&coord);
                    if calculate_chunk_distance(&self.chunk_position, &coord)
                        <= self.render_distance.into()
                    {
                        chunks_to_get.push(coord)
                    }
                }
            }
        }
        self.chunk_event_broadcast.send(ChunkInputEvent::GetChunks {
            chunks: chunks_to_get,
            sender: self.chunk_load_sender.clone(),
        })
    }

    fn request_chunks(&mut self) {
        let mut load_coords: Vec<Coord> = Vec::new();
        let offset: i64 = self.render_distance.into();
        for x in (self.chunk_position[0] - offset)..(self.chunk_position[0] + offset) {
            for y in (self.chunk_position[1] - offset)..(self.chunk_position[1] + offset) {
                for z in (self.chunk_position[2] - offset)..(self.chunk_position[2] + offset) {
                    let coord = [x, y, z];
                    if calculate_chunk_distance(&self.chunk_position, &coord) <= offset {
                        let already_loaded =
                            self.loaded_chunks.contains(&coord) || self.load_lock.contains(&coord);
                        if !already_loaded {
                            load_coords.push(coord);
                            self.load_lock.insert(coord);
                        }
                    }
                }
            }
        }
        load_coords.sort_unstable_by_key(|key| calculate_chunk_distance(key, &self.chunk_position));

        self.chunk_event_broadcast
            .send(ChunkInputEvent::RequestChunks {
                chunks: load_coords.clone(),
                optional_sender: None,
            });

        self.chunk_event_broadcast.send(ChunkInputEvent::GetChunks {
            chunks: load_coords,
            sender: self.chunk_load_sender.clone(),
        })
    }

    async fn receive_chunks(&mut self) {
        while let Ok(update_chunk) = self.chunk_load_receiver.try_recv() {
            self.load_lock.remove(&update_chunk.coord);
            if calculate_chunk_distance(&update_chunk.coord, &self.chunk_position)
                <= self.render_distance.into()
            {
                if update_chunk.fully_loaded {
                    self.loaded_chunks.insert(update_chunk.coord);
                }
                self.communicator
                    .send_stc(STC::ChunkUpdates(vec![(
                        update_chunk.chunk,
                        update_chunk.coord,
                    )]))
                    .await
            }
        }
    }
}
