mod chunk_loader;

use crate::hooks::{chunk::*, users::*};
use common::{event::Event, prelude::*};
use log::*;
use std::net::SocketAddr;

// this function acts as kind of like a bridge between sync and async code
// the client part runs in the tokio runtime, to make many simultanious connections possible
// all the handles run on seperate os threads, for performance predictability and ease of use
pub async fn init(
    output_communicator: OutputCommunicator,
    mut input_communicator: InputCommunicator,
    addr: SocketAddr,
) {
    let user_hook = USER_HOOK.clone();

    let mut user_token: Option<Token> = None; // for loggin off in case of unexpected disonnection
    let mut user_name: Option<String> = None;

    let mut chunk_loader = chunk_loader::ChunkLoader::new(output_communicator.clone());

    let chunk_event_sender = CHUNK_HOOK.input.clone();

    'client: loop {
        let Ok(event) = input_communicator.get_event().await else {break 'client};
        let event = match event {
            Event::CTS(ev) => ev,
            Event::Disconnect => break 'client,
            _ => continue,
        };
        match event {
            Register { name } => {
                let (tx, rx) = channel(Some(1));
                user_hook.input.send(UserInputEvent::Register {
                    name: name.clone(),
                    token: tx,
                });
                if let Some(Some(token)) = rx.recv() {
                    user_token = Some(token);
                    user_name = Some(name);
                    output_communicator.send_stc(STC::Token(token)).await;
                } else {
                    output_communicator
                        .send_stc(STC::Error(ClientError::Register))
                        .await
                }
            }
            Login { token } => {
                let (tx, rx) = channel(Some(1));
                user_hook
                    .input
                    .send(UserInputEvent::Login { token, success: tx });
                match rx.recv() {
                    Some(Ok(_)) => {
                        // token must only be set if ok
                        // otherwise logoff of user would happen, when second user with same account logs in
                        user_token = Some(token);
                        let (tx, rx) = channel(Some(1));
                        user_hook
                            .input
                            .send(UserInputEvent::GetUser { token, user: tx });
                        let user = match rx.recv() {
                            Some(Some(user)) => {
                                let name = user.clone().name;
                                info!("[{}] logged in at: {:?}", name, user.pos);
                                user_name = Some(name);
                                Some(user.clone())
                            }
                            _ => None,
                        };
                        if let Some(user) = user {
                            output_communicator.send_stc(STC::LoggedIn(user)).await;
                        }
                    }
                    Some(Err(e)) => {
                        output_communicator
                            .send_stc(STC::Error(ClientError::Login(e)))
                            .await;
                    }
                    _ => (),
                }
            }
            // also triggers chunkload
            Move { coord } => {
                if let Some(token) = user_token {
                    user_hook.input.send(UserInputEvent::ModUser {
                        token,
                        mod_instruction: UserModInstruction::Move(coord),
                    });
                    chunk_loader.update(coord).await;
                } else {
                    warn!(
                        "[{}][{}]: auth violation detected!",
                        user_name.clone().unwrap_or_else(|| String::from("")),
                        addr
                    );
                    output_communicator
                        .send_stc(STC::Error(ClientError::ConnectionReset))
                        .await;
                    break;
                }
            }
            PlaceStructure { structure } => {
                if let Some(_token) = user_token {
                    chunk_event_sender.send(ChunkInputEvent::SuperChunkModified {
                        super_chunk: structure,
                        from_generation: false,
                    })
                } else {
                    warn!(
                        "[{}][{}]: auth violation detected!",
                        user_name.clone().unwrap_or_else(|| String::from("")),
                        addr
                    );
                    output_communicator
                        .send_stc(STC::Error(ClientError::ConnectionReset))
                        .await;
                    break;
                }
            }
        }
    }

    if let Some(name) = user_name {
        log::info!("[{}] disconnected", name);
    }

    // USER DISCONNECTION HANDLING
    if let Some(token) = user_token {
        let (tx, _rx) = channel(Some(1));
        user_hook
            .input
            .send(UserInputEvent::Logoff { token, success: tx });
    }

    output_communicator.disconnect().await;
}
