// @TODO: implement proper tile-storage in storage lib instead of HashMap
use std::collections::HashMap;

use super::Tile;
use crate::hooks::chunk::*;
use crate::hooks::map::*;
use common::prelude::*;
use core::ops::Range;
use std::thread;
use std::time::Duration;

const CYCLE_TIMEOUT: Duration = Duration::from_millis(10);
const OPERATIONS_PER_CYCLE: usize = 128;
const Y_CHUNKS: Range<i64> = 1..4;

pub fn init() {
    thread::spawn(run);
}

fn run() {
    let map_hook = MAP_HOOK.clone();
    let map_input_event_receiver = map_hook.input.get_receiver().unwrap();
    let chunk_hook = CHUNK_HOOK.clone();
    let chunk_output_event_receiver = chunk_hook.output.get_receiver().unwrap();

    let mut tile_storage: HashMap<Coord2D, HashMap<i64, Tile>> = HashMap::default();
    let (chunk_sender, chunk_receiver) = channel(None);

    loop {
        for _ in 0..OPERATIONS_PER_CYCLE {
            if let Ok(ChunkOutputEvent::ChunkUpdate { coord }) =
                chunk_output_event_receiver.try_recv()
            {
                chunk_hook.input.send(ChunkInputEvent::GetChunks {
                    chunks: vec![coord],
                    sender: chunk_sender.clone(),
                })
            } else {
                break;
            }
        }
        for _ in 0..OPERATIONS_PER_CYCLE {
            if let Ok(update_chunk) = chunk_receiver.try_recv() {
                if update_chunk.chunk.is_empty() {
                    continue;
                }

                let coord_2d = [update_chunk.coord[0], update_chunk.coord[2]];

                let tile = Tile::render(&update_chunk.chunk, update_chunk.coord);

                if let Some(map) = tile_storage.get_mut(&coord_2d) {
                    map.insert(update_chunk.coord[1], tile.clone());
                } else {
                    let mut map = HashMap::default();
                    map.insert(update_chunk.coord[1], tile.clone());
                    tile_storage.insert(coord_2d, map);
                }

                map_hook
                    .output
                    .send(MapOutputEvent::TileUpdate { coord: coord_2d });
            } else {
                break;
            }
        }

        if let Some(map_input_event) = map_input_event_receiver.recv_timeout(CYCLE_TIMEOUT) {
            match map_input_event {
                MapInputEvent::GetTiles { tiles, sender } => {
                    for coord in tiles {
                        if let Some(tiles) = tile_storage.get(&coord) {
                            let mut tiles: Vec<(i64, Tile)> = tiles.clone().into_iter().collect();
                            tiles.sort_unstable_by_key(|x| x.0);

                            if !tiles.is_empty() {
                                let (_height, mut tile) = tiles.remove(0);
                                for (_h, t) in tiles {
                                    tile.merge(&t);
                                }
                                let _ = sender.send((tile, coord));
                            }
                        } else {
                            chunk_hook.input.send(ChunkInputEvent::RequestChunks {
                                chunks: Y_CHUNKS.map(|y| [coord[0], y, coord[1]]).collect(),
                                optional_sender: Some(chunk_sender.clone()),
                            });
                        }
                    }
                }
            }
        }
    }
}
