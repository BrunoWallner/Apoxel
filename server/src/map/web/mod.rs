mod web_socket;

use async_std::task;
use std::fs;
use std::io::prelude::*;
use std::net::Ipv4Addr;
use std::net::SocketAddrV4;
use std::net::TcpListener;
use std::net::TcpStream;
use std::path::Path;
use std::thread;

use crate::config;

const STATUS_OK: &str = "HTTP/1.1 200 OK\r\n\r\n";
const STATUS_BAD: &str = "HTTP/1.1 404 NOT FOUND\r\n\r\n";
const PATH: &str = "web/map/";

const WEBSOCKET_PORT: u16 = 8123;

pub fn init() {
    web_socket::init().unwrap();
    thread::spawn(move || {
        // check if web path is available
        if !Path::new(PATH).exists() {
            log::warn!("could not initialize web-map: web directory not found");
        } else {
            let ip = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), config::get().map.port);
            if let Ok(listener) = TcpListener::bind(ip) {
                log::info!("map webserver is listening on: {ip}");
                // Block forever, handling each request that arrives at this IP address
                for stream in listener.incoming().flatten() {
                    task::spawn_blocking(move || {
                        handle_connection(stream);
                    });
                }
            }
        }
    });
}

fn handle_connection(mut stream: TcpStream) {
    // Read the first 1024 bytes of data from the stream
    let mut buffer = [0; 1024];
    let bytes_read = stream.read(&mut buffer).unwrap();

    let request = String::from_utf8_lossy(&buffer[0..bytes_read]).to_string();
    let Some(host_ip) = get_host_ip_from_request(&request) else {return};
    let (content, status) = if let Some(path) = get_path_from_request(&request) {
        // expected output: "{path_of_file}.{file_extension}"
        // nothing else
        let path = match path.as_str() {
            "/" => "index.html",
            p => p.trim_start_matches('/'),
        };

        // safety checks
        if path.starts_with('/') || path.contains("../") {
            log::warn!("dangerous http GET request: **'{}'**\n{}", path, request);
            return;
        }

        let absolute_path = format!("{PATH}{path}");
        if let Ok(inner) = fs::read_to_string(absolute_path) {
            (inner, STATUS_OK)
        } else {
            (String::from(""), STATUS_BAD)
        }
    } else {
        (String::from(""), STATUS_BAD)
    };

    // compile socket
    let content = content.replace(
        "/* @COMPILE: socket */",
        &format!("let socket = new WebSocket(\"ws://{host_ip}:{WEBSOCKET_PORT}\");"),
    );

    // Write response back to the stream,
    // and flush the stream to ensure the response is sent back to the client
    let response = format!("{status}{content}");
    stream.write_all(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}

fn get_path_from_request(request: &str) -> Option<String> {
    let mut lines = request.split('\n');
    let get = lines.find(|line| line.starts_with("GET"))?;
    let mut tokens = get.split(' ');
    tokens.nth(1).map(String::from)
}

fn get_host_ip_from_request(request: &str) -> Option<String> {
    let mut lines = request.split('\n');
    let host = lines.find(|line| line.starts_with("Host"))?;
    // also split port
    let mut tokens = host.split(':');
    Some(String::from(tokens.nth(1)?.trim()))
}
