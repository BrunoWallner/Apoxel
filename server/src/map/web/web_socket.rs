use async_std::task;
use std::io;
use std::net::TcpListener;
use std::net::TcpStream;

use common::prelude::*;

use crate::hooks::map::*;
use crate::hooks::Hook;
use crate::map::Tile;

const PORT: u16 = 8123;
const TILE_SIZE: usize = 32;
const REQUEST_CHUNK_SIZE: usize = 16;
const REQUESTS_PER_CYCLE: usize = 128;

pub fn init() -> Result<(), io::Error> {
    let address = format!("0.0.0.0:{PORT}");
    let listener = TcpListener::bind(address)?;

    task::spawn_blocking(move || {
        for stream in listener.incoming() {
            let Ok(stream) = stream else {continue};
            let Ok(mut web_socket) = tungstenite::accept(stream) else {continue};

            let map_hook = MAP_HOOK.clone();
            let map_event_recevier = map_hook.output.get_receiver().unwrap();
            let (tile_sender, tile_receiver) = channel(None);

            task::spawn_blocking(move || loop {
                let Ok(message) = web_socket.read_message() else { break };
                if let tungstenite::Message::Text(text) = message {
                    let Some(instruction) = parse_text(&text) else {
                        log::warn!("client sent invalid instruction");
                        log::warn!("{}", text);
                        continue;
                    };
                    match_instruction(
                        instruction,
                        &map_event_recevier,
                        &map_hook,
                        &tile_sender,
                        &tile_receiver,
                        &mut web_socket,
                    );
                }
            });
        }
    });

    Ok(())
}

fn match_instruction(
    instruction: Instruction,
    map_event_recevier: &Receiver<MapOutputEvent>,
    map_hook: &Hook<MapInputEvent, MapOutputEvent>,
    tile_sender: &Sender<(Tile, [i64; 2])>,
    tile_receiver: &Receiver<(Tile, [i64; 2])>,
    web_socket: &mut tungstenite::WebSocket<TcpStream>,
) {
    match instruction {
        Instruction::Cycle => {
            let mut tiles_to_get = Vec::new();
            for _ in 0..REQUESTS_PER_CYCLE {
                if let Ok(map_event) = map_event_recevier.try_recv() {
                    match map_event {
                        // @TODO! check if in range of position
                        MapOutputEvent::TileUpdate { coord } => {
                            tiles_to_get.push(coord);
                        }
                    }
                } else {
                    break;
                }
            }
            map_hook.input.send(MapInputEvent::GetTiles {
                tiles: tiles_to_get,
                sender: tile_sender.clone(),
            });
            for _ in 0..REQUESTS_PER_CYCLE {
                if let Ok((tile, coord)) = tile_receiver.try_recv() {
                    // texture and 2 64 bit coords
                    let mut buffer: Vec<u8> = Vec::with_capacity(TILE_SIZE.pow(2) + 2);

                    // coord data as 2 64 bits
                    for c in coord {
                        let byte = (c + 128) as u8;
                        buffer.push(byte);
                    }

                    // rgb image data
                    for i in 0..TILE_SIZE.pow(2) {
                        let c = tile.texture[i];
                        for c in c {
                            buffer.push(c);
                        }
                    }

                    let _ = web_socket.write_message(tungstenite::Message::Binary(buffer));
                } else {
                    break;
                }
            }
        }
        // @FIXME: if world is already generated at specific point it will not be generateable or loadable
        Instruction::Load { coord, radius } => {
            let mut tiles = Vec::new();
            for x in -radius..radius {
                for y in -radius..radius {
                    let x = x + coord[0];
                    let y = y + coord[1];

                    tiles.push([x, y]);
                }
            }
            for tiles in tiles.chunks(REQUEST_CHUNK_SIZE) {
                map_hook.input.send(MapInputEvent::GetTiles {
                    tiles: tiles.to_vec(),
                    sender: tile_sender.clone(),
                });
            }
        }
    }
}

#[derive(Clone, Debug)]
enum Instruction {
    Cycle,
    Load { coord: Coord2D, radius: i64 },
}

fn parse_text(text: &str) -> Option<Instruction> {
    let split: Vec<&str> = text.split(' ').collect();
    match *split.first()? {
        "cycle" => Some(Instruction::Cycle),
        "load" => {
            let x: i64 = split.get(1)?.parse().ok()?;
            let y: i64 = split.get(2)?.parse().ok()?;
            let radius: i64 = split.get(3)?.parse().ok()?;

            Some(Instruction::Load {
                coord: [x, y],
                radius,
            })
        }
        _ => None,
    }
}
