use common::channel::*;
use std::sync::{Arc, Mutex};

lazy_static! {
    static ref BROADCAST: Broadcast<()> = Broadcast::new();
    static ref IS_SAFE_BROADCAST: Broadcast<()> = Broadcast::new();
    static ref MARKER: Arc<Mutex<Vec<bool>>> = Arc::new(Mutex::new(Vec::new()));
    static ref MARKER_INDEX: Arc<Mutex<usize>> = Arc::new(Mutex::new(0));
}

pub struct ShutdownHandle {
    index: usize,
    is_safe_receiver: Receiver<()>,
}
impl ShutdownHandle {
    pub fn new() -> Option<Self> {
        let mut marker = MARKER.lock().ok()?;
        let mut marker_index = MARKER_INDEX.lock().ok()?;
        marker.push(false);

        let index = *marker_index;
        *marker_index += 1;

        let is_safe_receiver = IS_SAFE_BROADCAST.get_receiver()?;

        let shutdown_marker = Self {
            index,
            is_safe_receiver,
        };

        Some(shutdown_marker)
    }

    pub fn mark_safe(&self) {
        let Ok(mut marker) = MARKER.lock() else {return};
        let Some(index) = marker.get_mut(self.index) else {return};
        *index = true;
        if marker.iter().all(|x| *x) {
            IS_SAFE_BROADCAST.send(());
        }
    }

    pub fn ignore(&self) {
        let Ok(mut marker) = MARKER.lock() else {return};
        let Some(index) = marker.get_mut(self.index) else {return};
        *index = true;
    }

    pub fn wait_until_safe(&self) {
        let _ = self.is_safe_receiver.recv();
    }
}

pub fn get() -> Option<(Receiver<()>, ShutdownHandle)> {
    let receiver = BROADCAST.get_receiver()?;
    let marker = ShutdownHandle::new()?;

    Some((receiver, marker))
}

pub fn shut_down() {
    BROADCAST.send(());
}
