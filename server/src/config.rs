use std::fs::File;
use std::io::Read;

use serde::{Deserialize, Serialize};

static mut CONFIG: Option<Config> = None;

pub fn get() -> Config {
    unsafe { CONFIG.clone().unwrap() }
}

pub fn init() -> Result<(), ()> {
    if let Some(config) = Config::get() {
        unsafe { CONFIG = Some(config) };
        Ok(())
    } else {
        Err(())
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config {
    pub connection: Connection,
    pub chunks: Chunks,
    pub map: Map,
}
impl Config {
    // delays startup time
    pub fn get() -> Option<Self> {
        let mut file = File::open("config.toml").ok()?;
        let mut content = vec![];
        file.read_to_end(&mut content).unwrap();

        let toml = toml::from_slice(&content).ok()?;

        Some(toml)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Connection {
    pub port: u16,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Map {
    pub port: u16,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Chunks {
    pub render_distance: u16,
    pub chunks_per_cycle: u32,
    pub seed: u32,
    pub save_path: String,
}
