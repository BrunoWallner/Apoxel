use common::types::octree::{Neighbour, Side};
// Corner reference at: `common/types/octree/path/Corner`
use Neighbour::*;

pub fn push(side: Side, neighbours: &[bool; 26], lights: &mut Vec<f32>) {
    let check: [[Neighbour; 3]; 4] = match side {
        Side::Left => [
            [LMB, LBB, LBM],
            [LMB, LTB, LTM],
            [LMF, LBF, LBM],
            [LMF, LTF, LTM],
        ],
        Side::Right => [
            [RMB, RBB, RBM],
            [RMB, RTB, RTM],
            [RMF, RBF, RBM],
            [RMF, RTF, RTM],
        ],
        Side::Back => [
            [MBB, LBB, LMB],
            [MBB, RBB, RMB],
            [LMB, MTB, LTB],
            [RMB, MTB, RTB],
        ],
        Side::Front => [
            [MBF, LBF, LMF],
            [MBF, RBF, RMF],
            [LMF, MTF, LTF],
            [RMF, MTF, RTF],
        ],
        Side::Top => [
            [MTB, LTB, LTM],
            [MTB, RTM, RTB],
            [LTM, LTF, MTF],
            [MTF, RTM, RTF],
        ],
        Side::Bottom => [
            [MBB, LBB, LBM],
            [MBB, RBM, RBB],
            [LBM, LBF, MBF],
            [MBF, RBM, RBF],
        ],
    };

    for check in check {
        let mut light: f32 = 1.0;
        for n in check {
            if neighbours[n as usize] {
                light -= 0.2;
            }
        }
        lights.push(light);
    }
}
