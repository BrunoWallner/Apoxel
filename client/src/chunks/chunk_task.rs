use super::chunk_material::ChunkMaterial;
use super::mesh::{self, generate};
use super::preprocessor::Preprocessed;
use super::ChunkEntityMap;
use super::ChunkSpawnAnimation;
use bevy::prelude::*;
use common::channel::*;
use common::types::octree::Side;
use common::{chunk::Chunk, Coord};
use std::collections::HashMap;
use std::thread;

const UPDATES_PER_TICK: usize = 32;
const PROCESSES_PER_TICK: u32 = 64;

#[derive(Resource, Component)]
pub struct ChunkMeshTasker {
    pub sender: Sender<(Chunk, Coord)>,
}
impl ChunkMeshTasker {
    pub fn init(chunk_spawner: &ChunkMeshSpawner) -> Self {
        // create channel with unlimited troughput
        // might lead to extensive ram usage when not handled properly
        let (sender, receiver) = channel(None);
        Self::run(receiver, chunk_spawner.sender.clone());
        ChunkMeshTasker { sender }
    }

    pub fn task(&self, chunk: Chunk, coord: Coord) -> Result<(), SendError<(Chunk, Coord)>> {
        self.sender.send((chunk, coord))
    }

    // @TODO: enable multithreaded chunk meshing
    // has to be synced or otherwise they could be received in the wrong order
    // can be done by sending unique index value with mesh
    fn run(chunk_receiver: Receiver<(Chunk, Coord)>, mesh_sender: Sender<(Mesh, Coord)>) {
        // let _child = thread::Builder::new()
        //     .stack_size(1024_000 * 10)
        //     .spawn(move || run(chunk_receiver, mesh_sender))
        //     .unwrap();

        thread::spawn(move || run(chunk_receiver, mesh_sender));

        fn run(chunk_receiver: Receiver<(Chunk, Coord)>, mesh_sender: Sender<(Mesh, Coord)>) {
            let mut handle = PreprocessedHandle::new(mesh_sender);
            let mut updates: Vec<Update> = Vec::new();
            loop {
                let mut processed: u32 = 0;
                while let Ok((chunk, coord)) = chunk_receiver.try_recv() {
                    let mut u = handle.process(chunk, coord);
                    updates.append(&mut u);
                    updates.dedup();

                    processed += 1;
                    if processed > PROCESSES_PER_TICK {
                        break;
                    }
                }

                let ups: Vec<Update> = if updates.len() < UPDATES_PER_TICK {
                    updates.drain(..).collect()
                } else {
                    updates.drain(..UPDATES_PER_TICK).collect()
                };

                for update in ups {
                    let mut _u = handle.update(
                        update.side,
                        update.coord,
                        update.orig_side,
                        update.orig_coord,
                    );
                    // updates.append(&mut u);
                }
            }
        }
    }
}

#[derive(Clone, PartialEq, Eq)]
struct Update {
    pub side: Side,
    pub coord: Coord,
    pub orig_side: Side,
    pub orig_coord: Coord,
}

struct PreprocessedHandle {
    chunks: HashMap<Coord, Preprocessed>,
    mesh_sender: Sender<(Mesh, Coord)>,
}
impl PreprocessedHandle {
    pub fn new(mesh_sender: Sender<(Mesh, Coord)>) -> Self {
        Self {
            chunks: HashMap::default(),
            mesh_sender,
        }
    }

    fn get(&self, coord: &Coord) -> Option<Preprocessed> {
        match self.chunks.get(coord) {
            Some(x) => Some(x.clone()),
            None => None,
        }
    }

    pub fn process(&mut self, chunk: Chunk, coord: Coord) -> Vec<Update> {
        let previous = self.get(&coord);
        let mut updates: Vec<Update> = Vec::new();

        let mut preprocessed = Preprocessed::new();
        preprocessed.build_grid(&chunk);

        // get sides from neighbours
        for side in Side::all() {
            let offset = side.get_offset();
            let neighbour_coord = [
                coord[0] + offset[0] as i64,
                coord[1] + offset[1] as i64,
                coord[2] + offset[2] as i64,
            ];
            if let Some(neighbour) = self.get(&neighbour_coord) {
                let neighbour_side = side.get_opposite();
                let neighbour_side = neighbour.sides[neighbour_side as usize].clone();
                preprocessed.set_internal_side(&side, &neighbour_side);
            }
        }

        preprocessed.update_external_sides();

        // generate mesh and send it
        self.chunks.insert(coord, preprocessed.clone());
        let mesh = generate(&preprocessed, &coord);
        let _ = self.mesh_sender.send((mesh, coord));

        // update all neighbours
        for side in Side::all() {
            let should_update = if let Some(ref previous) = previous {
                let block = previous.sides[side as usize].blocks
                    != preprocessed.sides[side as usize].blocks;
                block
            } else {
                true
            };

            if should_update {
                let offset = side.get_offset();
                let delta_coord = [
                    coord[0] + offset[0] as i64,
                    coord[1] + offset[1] as i64,
                    coord[2] + offset[2] as i64,
                ];
                let delta_side = side.get_opposite();
                // self.update(delta_side, delta_coord, side, coord);
                updates.push(Update {
                    side: delta_side,
                    coord: delta_coord,
                    orig_side: side,
                    orig_coord: coord,
                });
            }
        }
        updates
    }

    fn update(
        &mut self,
        side: Side,
        coord: Coord,
        orig_side: Side,
        orig_coord: Coord,
    ) -> Vec<Update> {
        // log::info!("updated: {:?}", coord);
        let mut updates: Vec<Update> = Vec::new();
        let Some(mut preprocessed) = self.get(&coord) else {
            return updates;
        };

        let Some(orig) = self.get(&orig_coord) else {
            return updates;
        };
        let orig_chunk_side = &orig.sides[orig_side as usize];
        preprocessed.set_internal_side(&side, &orig_chunk_side);

        let previous_sides = preprocessed.sides.clone();

        // generate external sides
        preprocessed.update_external_sides();

        // mesh preprocessed and send it
        self.chunks.insert(coord, preprocessed.clone());
        let mesh = generate(&preprocessed, &coord);
        let _ = self.mesh_sender.send((mesh, coord));

        // update all neighbours
        for neighbour_side in Side::all() {
            let should_update = previous_sides[neighbour_side as usize].blocks
                != preprocessed.sides[neighbour_side as usize].blocks;

            if should_update {
                // println!("recursion");
                let offset = neighbour_side.get_offset();
                let delta_coord = [
                    coord[0] + offset[0] as i64,
                    coord[1] + offset[1] as i64,
                    coord[2] + offset[2] as i64,
                ];
                let delta_side = neighbour_side.get_opposite();
                // self.update(delta_side, delta_coord, neighbour_side, coord);
                updates.push(Update {
                    side: delta_side,
                    coord: delta_coord,
                    orig_side: neighbour_side,
                    orig_coord: coord,
                });
            }
        }

        updates
    }
}

#[derive(Component)]
pub struct ChunkMesh {
    pub mesh: Mesh,
}

#[derive(Resource, Component)]
pub struct ChunkMeshSpawner {
    pub sender: Sender<(Mesh, Coord)>,
    pub receiver: Receiver<(Mesh, Coord)>,
}
impl ChunkMeshSpawner {
    pub fn init() -> Self {
        // create channel with unlimited troughput
        // might lead to extensive ram usage when not handled properly
        let (sender, receiver) = channel(None);
        ChunkMeshSpawner { sender, receiver }
    }

    pub fn poll_mesh(&self) -> Option<(Mesh, Coord)> {
        self.receiver.try_recv().ok()
    }

    pub(super) fn run(
        mut commands: Commands,
        chunk_mesh_spawner: Res<ChunkMeshSpawner>,
        mut chunk_map: ResMut<ChunkEntityMap>,
        mut meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<ChunkMaterial>>,
        mut chunk_meshes: Query<(Entity, &Handle<Mesh>, &ChunkMesh, &mut Visibility)>,
    ) {
        while let Some((mesh, coord)) = chunk_mesh_spawner.poll_mesh() {
            if let Some(entity) = chunk_map.map.get(&coord) {
                commands.entity(*entity).insert(ChunkMesh { mesh });
            } else {
                let entity = commands
                    .spawn(MaterialMeshBundle {
                        mesh: meshes.add(mesh::empty()),
                        transform: Transform::from_xyz(
                            coord[0] as f32,
                            0.0, // already handeled in ChunkSpawnAnimation
                            coord[2] as f32,
                        ),
                        material: materials.add(ChunkMaterial {}),
                        visibility: Visibility::Hidden,
                        ..default()
                    })
                    // .insert(RayCastMesh::<RaycastSet>::default())
                    .insert(ChunkMesh { mesh })
                    .insert(ChunkSpawnAnimation {
                        height: coord[1] as f32,
                        time: 0.0,
                    })
                    .id();
                chunk_map.map.insert(coord, entity);
            }
        }
        // apply mesh
        for (entity, handle, mesh, mut visibility) in chunk_meshes.iter_mut() {
            *meshes.get_mut(handle).unwrap() = mesh.mesh.clone();
            commands.entity(entity).remove::<ChunkMesh>();
            *visibility = Visibility::Visible;
        }
    }
}
