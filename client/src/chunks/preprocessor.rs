use common::blocks::Block;
use common::chunk::BASE_CHUNK_SIZE;
use common::color::Color;
use common::prelude::Chunk;
use common::types::octree::Octree;
use common::types::octree::Side;

const SIDE_SIZE: usize = (BASE_CHUNK_SIZE * BASE_CHUNK_SIZE) as usize;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ChunkSide {
    pub blocks: Box<[Block; SIDE_SIZE]>,
    pub lights: Box<[Option<Color>; SIDE_SIZE]>,
}
impl ChunkSide {
    pub fn new() -> Self {
        Self {
            blocks: Box::new([Block::None; SIDE_SIZE]),
            lights: Box::new([None; SIDE_SIZE]),
        }
    }
    pub fn get_block(&self, x: usize, y: usize) -> Option<&Block> {
        self.blocks.get(Self::flatten_index(x, y))
    }

    pub fn set_block(&mut self, x: usize, y: usize, block: Block) {
        self.blocks[Self::flatten_index(x, y)] = block;
    }

    fn flatten_index(x: usize, y: usize) -> usize {
        y * BASE_CHUNK_SIZE as usize + x
    }
}

// with 2 added sides
const TOTAL_SIZE: usize = BASE_CHUNK_SIZE as usize + 2;
const TOTAL_INDEX_SIZE: usize = BASE_CHUNK_SIZE as usize + 1;
// pow is evil
const GRID_SIZE: usize = TOTAL_SIZE * TOTAL_SIZE * TOTAL_SIZE;

#[derive(Clone, Debug)]
pub struct Preprocessed {
    // contains size of self
    pub sides: Box<[ChunkSide; 6]>,
    // contains sides of others
    pub blocks: Box<[Block; GRID_SIZE]>,
}
impl Preprocessed {
    fn coord_from_side(
        side: &Side,
        mut x: usize,
        mut y: usize,
        start: usize,
        end: usize,
    ) -> (usize, usize, usize) {
        // additional offset to acount for border
        x += 1;
        y += 1;

        match side {
            Side::Left => (start, y, x),
            Side::Right => (end, y, x),
            Side::Back => (x, y, start),
            Side::Front => (x, y, end),
            Side::Bottom => (x, start, y),
            Side::Top => (x, end, y),
        }
    }

    fn flatten_index(x: usize, y: usize, z: usize) -> usize {
        (z * TOTAL_SIZE * TOTAL_SIZE) + (y * TOTAL_SIZE) + x
    }

    pub fn get_block(&self, x: usize, y: usize, z: usize) -> Option<&Block> {
        self.blocks.get(Self::flatten_index(x, y, z))
    }

    pub fn new() -> Self {
        Self {
            sides: Box::new([
                ChunkSide::new(),
                ChunkSide::new(),
                ChunkSide::new(),
                ChunkSide::new(),
                ChunkSide::new(),
                ChunkSide::new(),
            ]),
            blocks: Box::new([Block::None; GRID_SIZE]),
        }
    }

    pub fn set_block(&mut self, x: usize, y: usize, z: usize, block: Block) {
        if let Some(b) = self.blocks.get_mut(Self::flatten_index(x, y, z)) {
            *b = block;
        }
    }

    // get block grid
    pub fn build_grid(&mut self, chunk: &Chunk) {
        let max_depth = Octree::get_depth_from_size(BASE_CHUNK_SIZE);
        let (_, traversed) = chunk.traverse(Some(max_depth));
        for out in traversed {
            if let Some(block) = out.node.to_end().0 {
                let size = BASE_CHUNK_SIZE / Octree::get_size_from_depth(out.depth);
                // with offset of 1 to account for sides
                let position = [
                    (out.position[0] * BASE_CHUNK_SIZE as f32) as usize + 1,
                    (out.position[1] * BASE_CHUNK_SIZE as f32) as usize + 1,
                    (out.position[2] * BASE_CHUNK_SIZE as f32) as usize + 1,
                ];

                for x in position[0]..(position[0] + size as usize) {
                    for y in position[1]..(position[1] + size as usize) {
                        for z in position[2]..(position[2] + size as usize) {
                            self.blocks[Self::flatten_index(x, y, z)] = block
                        }
                    }
                }
            }
        }
    }

    pub fn set_internal_side(&mut self, side: &Side, chunk_side: &ChunkSide) {
        for x in 0..BASE_CHUNK_SIZE as usize {
            for y in 0..BASE_CHUNK_SIZE as usize {
                let (x2, y2, z2) = Self::coord_from_side(side, x, y, 0, TOTAL_INDEX_SIZE);
                let Some(block) = chunk_side.get_block(x, y) else {
                    continue;
                };
                self.set_block(x2, y2, z2, *block);
            }
        }
    }

    // get sides
    pub fn update_external_sides(&mut self) {
        for side in Side::all() {
            for x in 0..BASE_CHUNK_SIZE as usize {
                for y in 0..BASE_CHUNK_SIZE as usize {
                    let (x2, y2, z2) = Self::coord_from_side(&side, x, y, 1, TOTAL_INDEX_SIZE - 1);
                    let Some(block) = self.get_block(x2, y2, z2) else {
                        continue;
                    };
                    let block = *block;
                    self.sides[side as usize].set_block(x, y, block);
                }
            }
        }
    }
}
