// https://github.com/sburris0/bevy_flycam
/*
Copyright 2020 Spencer Burris

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is
hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

use bevy::ecs::event::{Events, ManualEventReader};
use bevy::input::mouse::MouseMotion;
use bevy::prelude::*;
use bevy::window::CursorGrabMode;

/// Keeps track of mouse motion events, pitch, and yaw
#[derive(Resource, Default)]
struct InputState {
    reader_motion: ManualEventReader<MouseMotion>,
    pitch: f32,
    yaw: f32,
}

/// Mouse sensitivity and movement speed
#[derive(Resource)]
pub struct MovementSettings {
    pub sensitivity: f32,
    pub speed: f32,
}

impl Default for MovementSettings {
    fn default() -> Self {
        Self {
            sensitivity: 0.05,
            speed: 1.0,
        }
    }
}

/// A marker component used in queries when you want flycams and not other cameras
#[derive(Component)]
pub struct FlyCamera;

/// Grabs/ungrabs mouse cursor
fn toggle_grab_cursor(window: &mut Window) {
    let grab_mode = match window.cursor.grab_mode {
        CursorGrabMode::None | CursorGrabMode::Locked => CursorGrabMode::Confined,
        CursorGrabMode::Confined => CursorGrabMode::None,
    };
    window.cursor.grab_mode = grab_mode;
    window.cursor.visible = !window.cursor.visible;
}

/// Handles keyboard input and movement
fn player_move(
    keys: Res<Input<KeyCode>>,
    time: Res<Time>,
    windows: Query<&Window>,
    settings: Res<MovementSettings>,
    mut query: Query<&mut Transform, With<FlyCamera>>,
) {
    if let Some(window) = windows.iter().next() {
        for mut transform in query.iter_mut() {
            let mut vel = Vec3::ZERO;
            let local_z = transform.local_z();
            let forward = -Vec3::new(local_z.x, 0., local_z.z);
            let right = Vec3::new(local_z.z, 0., -local_z.x);

            for key in keys.get_pressed() {
                if window.cursor.grab_mode == CursorGrabMode::Confined
                    || window.cursor.grab_mode == CursorGrabMode::Locked
                {
                    match key {
                        KeyCode::W => vel += forward,
                        KeyCode::S => vel -= forward,
                        KeyCode::A => vel -= right,
                        KeyCode::D => vel += right,
                        KeyCode::Space => vel += Vec3::Y,
                        KeyCode::LShift => vel -= Vec3::Y,
                        KeyCode::Q => {
                            server::shut_down();
                            std::process::exit(0);
                        }
                        _ => (),
                    }
                }
            }

            vel = vel.normalize_or_zero();

            transform.translation += vel * time.delta_seconds() * settings.speed;

            // impulse.impulse =
            // vel * (time.delta_seconds_f64() * settings.speed as f64) as f32 * 0.0005
        }
    } else {
        warn!("Primary window not found for `player_move`!");
    }
}

/// Handles looking around if cursor is locked
fn player_look(
    settings: Res<MovementSettings>,
    windows: Query<&Window>,
    mut state: ResMut<InputState>,
    motion: Res<Events<MouseMotion>>,
    mut query: Query<&mut Transform, With<FlyCamera>>,
) {
    if let Some(window) = windows.iter().next() {
        let mut delta_state = state.as_mut();
        for mut transform in query.iter_mut() {
            if window.cursor.grab_mode == CursorGrabMode::Confined
                || window.cursor.grab_mode == CursorGrabMode::Locked
            {
                for ev in delta_state.reader_motion.iter(&motion) {
                    // Using smallest of height or width ensures equal vertical and horizontal sensitivity
                    // let window_scale = window.height().min(window.width());
                    delta_state.pitch -= (settings.sensitivity * ev.delta.y).to_radians();
                    delta_state.yaw -= (settings.sensitivity * ev.delta.x).to_radians();
                }

                delta_state.pitch = delta_state.pitch.clamp(-1.54, 1.54);
                // Order is important to prevent unintended roll
                transform.rotation = Quat::from_axis_angle(Vec3::Y, delta_state.yaw)
                    * Quat::from_axis_angle(Vec3::X, delta_state.pitch);
            }
        }
    } else {
        warn!("Primary window not found for `player_look`!");
    }
}

fn cursor_grab(keys: Res<Input<KeyCode>>, mut windows: Query<&mut Window>) {
    if let Some(mut window) = windows.iter_mut().next() {
        if keys.just_pressed(KeyCode::Escape) {
            toggle_grab_cursor(&mut window);
        }
    } else {
        warn!("Primary window not found for `cursor_grab`!");
    }
}
/// Same as [`PlayerPlugin`] but does not spawn a camera
pub struct CameraPlugin;
impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<InputState>()
            .init_resource::<MovementSettings>()
            .add_system(player_move)
            .add_system(player_look)
            .add_system(cursor_grab);
    }
}
