use bevy::prelude::*;

use super::{Menu, MenuState};

pub(super) fn load(
    mut commands: Commands,
    menu_state: Res<MenuState>,
    menu: Query<Entity, With<Menu>>,
) {
    if *menu_state == MenuState::Connected {
        for m in menu.iter() {
            commands.entity(m).despawn_recursive();
        }
    }
}
