use bevy::prelude::Resource;
use common::event::Event;
use common::io::*;
use common::prelude::*;
use common::Token;

use async_std::io;
use async_std::net::{TcpStream, ToSocketAddrs};
use async_std::task;

#[derive(Resource)]
pub struct Communicator {
    pub token: Token,
    output: OutputCommunicator,
    input: InputCommunicator,
}
impl Communicator {
    pub fn init<H: ToSocketAddrs>(host: H) -> Option<Self> {
        let Ok((output, input)) = task::block_on(async move {
            let socket = TcpStream::connect(host).await?;
            let communicator = communicator::init(socket);

            Result::<(OutputCommunicator, InputCommunicator), io::Error>::Ok(communicator)
        }) else {return None};

        log::info!("init client communicator");

        Some(Self {
            output,
            input,
            token: [0_u8; 16],
        })
    }

    pub fn set_token(&mut self, token: Token) {
        self.token = token;
    }

    pub fn get_event(&mut self) -> Result<STC, RecvError> {
        let event = task::block_on(async move { self.input.get_event().await })?;

        match event {
            Event::STC(event) => Ok(event),
            _ => Err(RecvError),
        }
    }

    pub fn try_get_event(&mut self) -> Result<STC, TryRecvError> {
        let event = self.input.try_get_event()?;

        match event {
            Event::STC(event) => Ok(event),
            _ => Err(TryRecvError::Empty),
        }
    }

    pub fn send_event(&self, event: CTS) {
        task::block_on(async move { self.output.send_cts(event).await });
    }
}
