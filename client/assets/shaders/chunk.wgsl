#import bevy_pbr::mesh_view_bindings
#import bevy_pbr::mesh_bindings

#import "shaders/noise.wgsl"

const RENDER_DISTANCE: f32 = 1.0;
const PI: f32 = 3.14;

struct Vertex {
    @location(0) position: vec3<f32>,
    @location(1) color: vec4<f32>,
    @location(2) light: f32,
    @location(3) normal: vec4<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) color: vec4<f32>,
    @location(1) light: f32,
    @location(3) position: vec4<f32>,
};

@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    var out: VertexOutput;

    var world_position: vec4<f32> = mesh.model * vec4<f32>(vertex.position, 1.0);

    // let player_position: vec4<f32> = view.view[3];
    // let distance: f32 = distance(world_position.xyz, player_position.xyz);
    
    out.color = vertex.color;
    out.clip_position = view.view_proj * world_position;
    out.light = vertex.light;
    out.position = vertex.normal;

    return out;
}

struct FragmentInput {
    @location(0) color: vec4<f32>,
    @location(1) light: f32,
    @location(3) position: vec4<f32>,
};

@fragment
fn fragment(input: FragmentInput) -> @location(0) vec4<f32> {
    let noise_pos = vec3<f32>(
        input.position[0] * 10.0,
        input.position[1] * 10.0,
        input.position[2] * 10.0,
    );
    let noise_pos_2 = vec3<f32>(
        input.position[0] * 0.25,
        input.position[1] * 0.25,
        input.position[2] * 0.25,
    );
    var light_offset = noise(noise_pos) / 4.0;
    light_offset += noise(noise_pos_2) / 3.0;

    var color = vec4<f32>(
        input.color[0],
        input.color[1],
        input.color[2],
        1.0,
    );
    // srgb color correction
    color = pow(color, vec4(2.2, 2.2, 2.2, 2.2));
    color =  color * (input.light - light_offset);
    color[3] = 1.0;
    return color;
     
}