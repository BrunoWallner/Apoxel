use super::REGION_SIZE;
use common::Coord;
use std::ops::Range;
// size of header of one chunk in region in bytes
// two pointers with 64 bits -> 2 * 8 bytes = 16B;
// coords are not saved directly in the header
const HEADER_SECTION_SIZE: usize = 16;

// region header size in bytes
const TOTAL_CHUNKS_IN_REGION: usize = REGION_SIZE.pow(3);
pub const TOTAL_HEADER_SIZE: usize = HEADER_SECTION_SIZE * TOTAL_CHUNKS_IN_REGION;

// 'data' must only be header and NOT entire region file
pub fn get_data_pointer(coord: &Coord, header: &[u8]) -> Option<Range<u64>> {
    assert!(header.len() == TOTAL_HEADER_SIZE);
    // get the two pointers, which point to the part in the data, that contains the chunk data in the current coord
    let start_coord = get_coord_position(coord);
    let end_coord = start_coord + HEADER_SECTION_SIZE;

    let data = &header[start_coord..end_coord];
    let start_pointer = to_u64_be(&data[0..8]);
    let end_pointer = to_u64_be(&data[8..16]);

    match (start_pointer, end_pointer) {
        (s, e) if s >= e => None,
        (s, e) => Some(s..e),
    }
}

// get the position in the header, in which the chunk-data-pointer that corresponds to the coord should be
fn get_coord_position(coord: &Coord) -> usize {
    assert!(coord[0].abs() < REGION_SIZE as i64);
    assert!(coord[1].abs() < REGION_SIZE as i64);
    assert!(coord[2].abs() < REGION_SIZE as i64);

    let start_coord = coord[0].abs()
        + REGION_SIZE as i64 * (coord[1].abs() + REGION_SIZE as i64 * coord[2].abs());

    start_coord as usize * HEADER_SECTION_SIZE
}

// https://stackoverflow.com/questions/36669427/does-rust-have-a-way-to-convert-several-bytes-to-a-number
fn to_u64_be(array: &[u8]) -> u64 {
    ((array[0] as u64) << 56)
        + ((array[1] as u64) << 48)
        + ((array[2] as u64) << 40)
        + ((array[3] as u64) << 32)
        + ((array[4] as u64) << 24)
        + ((array[5] as u64) << 16)
        + ((array[6] as u64) << 8)
        + ((array[7] as u64) << 0)
}

fn from_u64_be(t: u64) -> [u8; 8] {
    let mut array: [u8; 8] = [0x00; 8];
    for i in 1..=8 {
        let byte: u8 = (t >> (64 - (i * 8))) as u8;
        array[i - 1] = byte;
    }
    array
}

pub fn set_chunk(coord: &Coord, mut chunk: Vec<u8>, data: &mut Vec<u8>) -> Result<(), ()> {
    if data.len() < TOTAL_HEADER_SIZE {
        common::log::warn!("invalid data-header");
        return Err(());
    }
    // check if chunk is already contained
    if let Some(pointer) = get_data_pointer(coord, &data[0..TOTAL_HEADER_SIZE]) {
        // chunk already exists
        // delete already existing chunk
        for _ in pointer.clone() {
            data.remove(pointer.start as usize);
        }
        // iter trough all data-entry pointers and offset them if they are further ahead
        let offset = pointer.end - pointer.start;
        for entry in 0..TOTAL_CHUNKS_IN_REGION {
            let start_entry_pointer = entry * HEADER_SECTION_SIZE;
            let end_entry_pointer = start_entry_pointer + HEADER_SECTION_SIZE;
            let entry = &data[start_entry_pointer..end_entry_pointer];
            let start_data_pointer = to_u64_be(&entry[0..8]);
            let end_data_pointer = to_u64_be(&entry[8..16]);
            if start_data_pointer >= pointer.start {
                set_coord_pointers(
                    start_entry_pointer,
                    data,
                    start_data_pointer - offset,
                    end_data_pointer - offset,
                );
            }
        }
    }
    // get data-pointers
    // append chunk-data to end
    let start_data_pointer = data.len() as u64;
    let end_data_pointer = start_data_pointer + chunk.len() as u64;

    // set coord pointers
    let index = get_coord_position(coord);
    set_coord_pointers(index, data, start_data_pointer, end_data_pointer);

    // append chunk-data
    data.append(&mut chunk);

    Ok(())
}

fn set_coord_pointers(index: usize, data: &mut Vec<u8>, start: u64, end: u64) {
    let pointers = [from_u64_be(start), from_u64_be(end)];
    let pointers: Vec<u8> = pointers.into_iter().flatten().collect(); // requires nightly unstable feature
    for i in index..(index + HEADER_SECTION_SIZE as usize) {
        let relative_position = i - index;
        data[i] = pointers[relative_position];
    }
}

mod test {
    #[test]
    fn u64_be_shift() {
        use super::from_u64_be;
        use super::to_u64_be;

        for number in 0..1024 {
            let array = from_u64_be(number);
            let n = to_u64_be(&array);
            assert_eq!(number, n);
        }
    }
}
