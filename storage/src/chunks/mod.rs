mod header;

use std::collections::HashMap;
use std::collections::HashSet;

use common::chunk::Chunk;
use common::data::serde::{self, Deserialize, Serialize};
use common::Coord;
use common::Token;

use crate::file_exists;
use std::fs::File;
use std::io::{self, Read, Write};
use std::io::{Seek, SeekFrom};

const REGION_SIZE: usize = 32;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(crate = "self::serde")]
pub struct StoredChunk {
    pub chunk: Chunk,
    pub needed_by: HashSet<Token>,
    pub fully_loaded: bool,
    pub modified: bool,
}
impl StoredChunk {
    pub fn new(chunk: Chunk) -> Self {
        Self {
            chunk,
            needed_by: HashSet::default(),
            fully_loaded: false,
            modified: false,
        }
    }
    pub fn is_needed(&self) -> bool {
        !self.needed_by.is_empty()
    }
    pub fn mark_needed_by(&mut self, token: Token) {
        self.needed_by.insert(token);
    }
    pub fn mark_unneeded_by(&mut self, token: &Token) {
        self.needed_by.remove(token);
    }
}

pub struct ChunkStorage {
    uncompressed: HashMap<Coord, StoredChunk>,
    compressed: HashMap<Coord, Vec<u8>>,
    save_path: String,
}

impl ChunkStorage {
    pub fn new(save_path: &str) -> Self {
        let mut save_path = String::from(save_path);
        if save_path.chars().last() != Some('/') {
            save_path.push('/');
        }
        crate::create_directory(&save_path).unwrap();
        Self {
            uncompressed: HashMap::default(),
            compressed: HashMap::default(),
            save_path,
        }
    }

    pub fn get<'a>(&'a mut self, coord: &Coord) -> Option<&'a StoredChunk> {
        match self.load(coord) {
            Ok(()) => (),
            Err(e) => common::log::warn!("failed to load world data: {}", e),
        }
        self.uncompressed.get(coord)
    }

    pub fn get_mut<'a>(&'a mut self, coord: &Coord) -> Option<&'a mut StoredChunk> {
        match self.load(coord) {
            Ok(()) => (),
            Err(e) => common::log::warn!("failed to load world data: {}", e),
        }
        self.uncompressed.get_mut(coord)
    }

    pub fn insert(&mut self, coord: Coord, stored_chunk: StoredChunk) {
        self.uncompressed.insert(coord, stored_chunk);
    }

    pub fn unload(&mut self, coord: &Coord) -> Result<(), ()> {
        let mut chunk = self.uncompressed.remove(coord).ok_or(())?;
        if chunk.modified {
            chunk.modified = false;
            let compressed = common::data::compress(&chunk).ok_or(())?;
            self.compressed.insert(*coord, compressed);
        }
        self.uncompressed.shrink_to_fit();

        Ok(())
    }

    pub fn unload_all(&mut self) -> Result<(), ()> {
        for key in self.uncompressed.clone().into_keys() {
            self.unload(&key)?;
        }
        Ok(())
    }

    pub fn save(&mut self) -> io::Result<()> {
        // file set_len to create empty header
        for (coord, chunk) in self.compressed.clone().into_iter() {
            let (region_coord, chunk_coord) =
                common::chunk::get_chunk_coords(REGION_SIZE as u32, &coord);
            let chunk_coord = [
                chunk_coord[0] as i64,
                chunk_coord[1] as i64,
                chunk_coord[2] as i64,
            ];

            let path = get_path(self.save_path.clone(), &region_coord);
            let data = if file_exists(&path) {
                let mut file = File::open(&path)?;
                // read file header
                let len = file.metadata()?.len();
                let mut buffer: Vec<u8> = Vec::with_capacity(len as usize);
                file.read_to_end(&mut buffer)?;
                if header::set_chunk(&chunk_coord, chunk, &mut buffer).is_err() {
                    common::log::warn!("failed to save chunk");
                }

                buffer
            } else {
                let mut buffer: Vec<u8> = vec![0_u8; header::TOTAL_HEADER_SIZE];
                if header::set_chunk(&chunk_coord, chunk, &mut buffer).is_err() {
                    common::log::warn!("failed to save chunk");
                }
                buffer
            };

            let mut file = File::create(path)?;
            file.write_all(&data)?;
        }
        self.compressed.clear();
        self.compressed.shrink_to_fit();

        Ok(())
    }

    // loads compressed or stored chunks into uncompressed
    fn load(&mut self, coord: &Coord) -> io::Result<()> {
        if self.uncompressed.contains_key(coord) {
            return Ok(());
        }
        if let Some(compressed) = self.compressed.remove(coord) {
            if let Some(uncompressed) = common::data::uncompress(&compressed) {
                if let Some(stored_chunk) = common::data::deserialize::<StoredChunk>(&uncompressed)
                {
                    self.uncompressed.insert(*coord, stored_chunk);
                }
            }
        } else {
            let (region_coord, chunk_coord) =
                common::chunk::get_chunk_coords(REGION_SIZE as u32, coord);
            let chunk_coord = [
                chunk_coord[0] as i64,
                chunk_coord[1] as i64,
                chunk_coord[2] as i64,
            ];
            let path = get_path(self.save_path.clone(), &region_coord);
            // needs unstable feature
            if file_exists(&path) {
                let mut file = File::open(&path)?;
                // read file header
                let len = file.metadata()?.len();
                if len >= header::TOTAL_HEADER_SIZE as u64 {
                    let mut header: [u8; header::TOTAL_HEADER_SIZE] =
                        [0x00; header::TOTAL_HEADER_SIZE];
                    file.read_exact(&mut header)?;
                    // check if chunk is contained and if so, get pointers to the data to it
                    if let Some(pointer) = header::get_data_pointer(&chunk_coord, &header) {
                        // chunk is contained -> read only the part of the file that contains the chunk-data
                        file.seek(SeekFrom::Start(pointer.start))?;
                        let mut data = vec![0_u8; (pointer.end - pointer.start) as usize];
                        file.read_exact(&mut data)?;

                        // uncompress and deserialize
                        if let Some(uncompressed) = common::data::uncompress(&data) {
                            if let Some(stored_chunk) =
                                common::data::deserialize::<StoredChunk>(&uncompressed)
                            {
                                self.uncompressed.insert(*coord, stored_chunk);
                            } else {
                                common::log::warn!("invalid file data in: {}", path);
                            }
                        } else {
                            common::log::warn!("invalid file data in: {}", path);
                        }
                    }
                } else {
                    common::log::warn!("invalid file header in: {}", path);
                }
            }
        }

        Ok(())
    }
}

fn get_path(save_path: String, coord: &Coord) -> String {
    let mut coord_path: String = String::new();
    for (i, c) in coord.iter().enumerate() {
        if i < coord.len() && i > 0 {
            coord_path.push('.');
        }
        coord_path.push_str(&c.to_string())
    }
    let path = save_path + &coord_path + ".region";
    path
}
