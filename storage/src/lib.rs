pub mod token;
// pub mod voxel;

pub mod chunks;
pub use chunks::*;

use std::path::Path;

use std::fs;
use std::io::Result;

pub fn file_exists<P: AsRef<Path>>(path: P) -> bool {
    let metadata = fs::metadata(path);
    match metadata {
        Ok(m) => m.is_file(),
        Err(_) => false,
    }
}

pub fn directory_exists<P: AsRef<Path>>(path: P) -> bool {
    let metadata = fs::metadata(path);
    match metadata {
        Ok(m) => m.is_dir(),
        Err(_) => false,
    }
}

pub fn create_directory<P: AsRef<Path> + Clone>(path: P) -> Result<()> {
    if !directory_exists(path.clone()) {
        fs::create_dir_all(path)?;
    }
    Ok(())
}
