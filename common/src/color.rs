use core::ops::{Div, Sub};
use serde::{Deserialize, Serialize};
use std::cmp::max;

#[derive(Copy, Clone, Debug, Deserialize, Serialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}
impl Color {
    pub const fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }
    pub const fn black() -> Self {
        Self { r: 0, g: 0, b: 0 }
    }
    pub const fn white() -> Self {
        Self {
            r: 255,
            g: 255,
            b: 255,
        }
    }
    pub const fn splat(splat: u8) -> Self {
        Self {
            r: splat,
            g: splat,
            b: splat,
        }
    }

    pub fn set_min(&mut self, min: u8) {
        self.r = max(self.r, min);
        self.g = max(self.g, min);
        self.b = max(self.b, min);
    }

    pub fn get_max(&self) -> u8 {
        let Some(max) = [self.r, self.g, self.b].into_iter().max() else {return 0};
        max
    }

    pub fn call<F: FnOnce(&mut u8) + Copy>(&mut self, f: F) {
        f(&mut self.r);
        f(&mut self.g);
        f(&mut self.b);
    }

    pub fn get_diff(&self, other: &Self) -> f32 {
        (self.r as f32 - other.r as f32).abs()
            + (self.g as f32 - other.g as f32).abs()
            + (self.b as f32 - other.b as f32).abs()
    }
}
impl Div<u8> for Color {
    type Output = Color;

    fn div(self, rhs: u8) -> Self::Output {
        Color {
            r: self.r / rhs,
            g: self.g / rhs,
            b: self.b / rhs,
        }
    }
}

impl Div<f32> for Color {
    type Output = Color;

    fn div(self, rhs: f32) -> Self::Output {
        Color {
            r: (self.r as f32 / rhs) as u8,
            g: (self.g as f32 / rhs) as u8,
            b: (self.b as f32 / rhs) as u8,
        }
    }
}

impl Sub<u8> for Color {
    type Output = Color;

    fn sub(self, rhs: u8) -> Self::Output {
        Color {
            r: self.r.checked_sub(rhs).unwrap_or(0),
            g: self.g.checked_sub(rhs).unwrap_or(0),
            b: self.b.checked_sub(rhs).unwrap_or(0),
        }
    }
}
