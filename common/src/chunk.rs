use super::blocks::Block;
use super::types::octree::Node;
use super::types::Octree;
use super::Coord;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

pub type Chunk = Octree<Block>;

pub const BASE_CHUNK_SIZE: u32 = 128;

pub fn get_chunk_coords(size: u32, coord: &[i64; 3]) -> ([i64; 3], [u32; 3]) {
    // Must work
    let chunk = [
        (coord[0] as f64 / size as f64).floor() as i64,
        (coord[1] as f64 / size as f64).floor() as i64,
        (coord[2] as f64 / size as f64).floor() as i64,
    ];
    let index = [
        (coord[0] - (chunk[0] * size as i64)).abs() as u32,
        (coord[1] - (chunk[1] * size as i64)).abs() as u32,
        (coord[2] - (chunk[2] * size as i64)).abs() as u32,
    ];

    (chunk, index)
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SuperChunk {
    pub chunks: BTreeMap<[i64; 3], Chunk>,
    pub coord: [i64; 3],
}
impl SuperChunk {
    pub fn new(main_chunk: Chunk, coord: Coord) -> Self {
        let mut chunks = BTreeMap::default();
        chunks.insert([0, 0, 0], main_chunk.clone());

        Self { chunks, coord }
    }

    pub fn merge(&mut self, other: &Self, offset: [f32; 3], max_depth: Option<u32>) {
        // from types/octree/octree/merge()
        for (coord, chunk) in other.chunks.iter() {
            let (_depth, out) = chunk.traverse(max_depth);
            for mut out in out {
                let size = Octree::get_size_from_depth(out.depth);
                out.multiply_position(size as f32);
                let pos = out.position;
                let pos = [
                    pos[0] as i64
                        + (((offset[0] - 0.5) * size as f32) as i64)
                        + (coord[0] * size as i64),
                    pos[1] as i64
                        + (((offset[1] - 0.5) * size as f32) as i64)
                        + (coord[1] * size as i64),
                    pos[2] as i64
                        + (((offset[2] - 0.5) * size as f32) as i64)
                        + (coord[2] * size as i64),
                ];
                if let Some(block) = out.node.to_end().0 {
                    self.place(size, pos, block);
                }
                match out.node {
                    Node::Empty => (),
                    n => self.place_node(size, pos, n),
                }
            }
        }
    }

    pub fn place_chunk(
        &mut self,
        chunk: &Chunk,
        offset: [f32; 3],
        max_depth: Option<u32>,
        override_size: Option<u32>,
    ) {
        let (_depth, out) = chunk.traverse(max_depth);
        for mut out in out {
            let orig_size = Octree::get_size_from_depth(out.depth);
            let size = match override_size {
                Some(s) => s,
                None => orig_size,
            };
            out.multiply_position(orig_size as f32);
            let pos = out.position;
            let pos = [
                pos[0] as i64 + (((offset[0] - 0.5) * size as f32) as i64),
                pos[1] as i64 + (((offset[1] - 0.5) * size as f32) as i64),
                pos[2] as i64 + (((offset[2] - 0.5) * size as f32) as i64),
            ];
            if let Some(block) = out.node.to_end().0 {
                self.place(size, pos, block);
            }
            match out.node {
                Node::Empty => (),
                n => self.place_node(size, pos, n),
            }
        }
    }

    pub fn get_main_chunk(&self) -> &Chunk {
        // note on unwrap:
        // guaranteed to not panic if initialized with Self::new()
        self.chunks.get(&[0, 0, 0]).unwrap()
    }
    pub fn remove_main_chunk(&mut self) -> Chunk {
        // guaranteed to not panic if initialized with Self::new()
        self.chunks.remove(&[0, 0, 0]).unwrap()
    }
    pub fn get(&self, size: u32, coord: [i64; 3]) -> (Option<Block>, u32) {
        let (chunk, index) = get_chunk_coords(size, &coord);

        if let Some(c) = self.chunks.get(&chunk) {
            return c.get_xyz(size, index[0], index[1], index[2]);
        } else {
            return (None, 0);
        }
    }

    pub fn place(&mut self, size: u32, coord: [i64; 3], block: Block) {
        if block != Block::None {
            let (chunk, index) = get_chunk_coords(size, &coord);

            if let Some(c) = self.chunks.get_mut(&chunk) {
                c.set_xyz(size, index[0], index[1], index[2], block);
            } else {
                let mut c = Octree::default();
                c.set_xyz(size, index[0], index[1], index[2], block);
                self.chunks.insert(chunk, c);
            }
        }
    }

    pub fn place_node(&mut self, size: u32, coord: [i64; 3], node: Node<Block>) {
        let (chunk, index) = get_chunk_coords(size, &coord);

        if let Some(c) = self.chunks.get_mut(&chunk) {
            c.set_node_xyz(size, index[0], index[1], index[2], node);
        } else {
            let mut c = Octree::default();
            c.set_node_xyz(size, index[0], index[1], index[2], node);
            self.chunks.insert(chunk, c);
        }
    }

    pub fn pack(&mut self) {
        for (_coord, chunk) in self.chunks.iter_mut() {
            chunk.pack();
        }
    }
}
