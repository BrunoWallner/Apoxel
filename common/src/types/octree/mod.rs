pub mod node;
pub mod octree;
pub mod path;
pub mod side;

pub use octree::Octree;
pub use path::Corner;
pub use side::Neighbour;
pub use side::Side;

pub use node::Node;

#[derive(Clone, Debug, PartialEq)]
pub struct TraverseOutput<T: Clone> {
    pub position: [f32; 3],
    pub depth: u32,
    pub node: Node<T>,
}
impl<T: Clone> TraverseOutput<T> {
    pub fn multiply_position(&mut self, multiplier: f32) {
        self.position[0] *= multiplier;
        self.position[1] *= multiplier;
        self.position[2] *= multiplier;
    }

    pub fn offset_position(&mut self, offset: [f32; 3]) {
        self.position[0] += offset[0];
        self.position[1] += offset[1];
        self.position[2] += offset[2];
    }
}
