use super::path::Corner;
use super::TraverseOutput;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum Node<T: Clone> {
    Group(Box<[Node<T>; 8]>),
    // Packed(Box<Node<T>>),
    Filled(T),
    Empty,
}
impl<T: Clone + PartialEq> Node<T> {
    pub fn get_empty_group() -> Box<[Node<T>; 8]> {
        Box::new([
            Node::Empty,
            Node::Empty,
            Node::Empty,
            Node::Empty,
            Node::Empty,
            Node::Empty,
            Node::Empty,
            Node::Empty,
        ])
    }

    pub fn get(&self, path: &[Corner]) -> (&Self, u32) {
        let mut t = self;

        let mut layer: u32 = 0;
        for p in path.iter() {
            match t {
                Self::Empty => {
                    break;
                }
                // when self is filled to an end return it by breaking
                Self::Filled(_) => {
                    break;
                }
                Self::Group(g) => {
                    t = &g[*p as usize];
                } // Self::Packed(node) => {
                  //     t = node;
                  // }
            }
            layer += 1;
        }
        (t, layer)
    }

    pub fn get_mut(&mut self, path: &[Corner]) -> (&mut Self, u32) {
        let mut t = self;

        let mut layer: u32 = 0;
        for p in path.iter() {
            match t {
                Self::Empty => {
                    break;
                }
                // when self is filled to an end return it by breaking
                Self::Filled(_) => {
                    break;
                }
                Self::Group(g) => {
                    t = &mut g[*p as usize];
                } // Self::Packed(node) => {
                  //     t = node;
                  // }
            }
            layer += 1;
        }
        (t, layer)
    }

    pub fn set(&mut self, path: &[Corner], node: Node<T>) {
        // setup last node to be a group
        let mut last_node: &mut Node<T> = self;
        for p in path[..].iter() {
            match last_node {
                Node::Group(group) => {
                    last_node = &mut group[*p as usize];
                }
                _ => {
                    last_node.subdivide();
                    let Node::Group(group) = last_node else {
                        unreachable!() // due to subdivision
                    };
                    last_node = &mut group[*p as usize];
                }
            }
        }

        *last_node = node;
    }

    #[inline]
    fn subdivide(&mut self) {
        match self {
            Node::Empty => {
                *self = Node::Group(Node::get_empty_group());
            }
            Node::Filled(fill) => {
                let mut group: Box<[Node<T>; 8]> = Node::get_empty_group();
                for corner in Corner::all() {
                    group[corner as usize] = Node::Filled(fill.clone());
                }
                *self = Node::Group(group);
            }
            // Node::Packed(pack) => {
            //     // unpack back to group
            //     let group = Node::get_filled_group(pack);
            //     *self = Node::Group(group);
            // }
            Node::Group(_group) => (),
        }
    }

    // pub fn pack(&mut self) {
    //     match self {
    //         Self::Empty | Self::Filled(_) => (),
    //         Self::Group(group) => {
    //             // actual cleaning
    //             // check if group consists of equal nodes
    //             if group.windows(2).all(|w| w[0] == w[1]) {
    //                 let sub_node = group[0].clone();
    //                 // *self = Node::Packed(Box::new(sub_node));
    //                 *self = Node::Filled()
    //                 self.pack();
    //             } else {
    //                 for corner in Corner::all() {
    //                     group[corner as usize].pack();
    //                 }
    //             }
    //         }
    //         Self::Packed(pack) => {
    //             pack.pack();
    //         }
    //     }
    // }

    pub fn pack(&mut self) {
        () // must be done in reverse
    }

    pub fn traverse(
        &self,
        current_depth: &mut u32,
        corners: &[Corner],
        max_depth: Option<u32>,
    ) -> Vec<TraverseOutput<T>> {
        fn is_max_depth(current_depth: u32, max_depth: Option<u32>) -> bool {
            if let Some(max_depth) = max_depth {
                if current_depth > max_depth {
                    return true;
                }
            }

            false
        }

        let mut output: Vec<TraverseOutput<T>> = Vec::new();
        if is_max_depth(*current_depth, max_depth) {
            return output;
        }
        match self {
            Self::Empty => (),
            Self::Filled(_) => output.push(TraverseOutput {
                position: [0.0, 0.0, 0.0],
                depth: *current_depth,
                node: self.clone(),
            }),
            Self::Group(group) => {
                // check if max depth is reached and do not traverse any further if so
                if is_max_depth(*current_depth, max_depth) {
                    output.push(TraverseOutput {
                        position: [0.0, 0.0, 0.0],
                        depth: *current_depth,
                        node: self.clone(),
                    })
                } else {
                    // traverse further
                    *current_depth += 1;
                    let mut current_depth_clone = vec![current_depth.clone(); 8];
                    for corner in corners {
                        let other = group[*corner as usize].traverse(
                            &mut current_depth_clone[*corner as usize],
                            corners,
                            max_depth,
                        );
                        let offset = corner.get_offset(0.5);
                        for mut other in other {
                            other.multiply_position(0.5);
                            other.offset_position(offset);
                            output.push(other);
                        }
                    }
                    *current_depth = *current_depth_clone.iter().max().unwrap();
                }
            } /*
              Self::Packed(pack) => {
                  // check if max depth is reached and do not traverse any further if so
                  if is_max_depth(*current_depth, max_depth) {
                      output.push(TraverseOutput {
                          position: [0.0, 0.0, 0.0],
                          depth: *current_depth,
                          node: self.clone(),
                      })
                  } else {
                      // traverse further
                      *current_depth += 1;
                      let other = pack.traverse(current_depth, corners, max_depth);
                      for mut other in other {
                          other.multiply_position(0.5);

                          for corner in corners {
                              let mut other = other.clone();
                              let offset = corner.get_offset(0.5);
                              other.offset_position(offset);
                              output.push(other);
                          }
                      }
                  }
              }
              */
        }

        output
    }

    pub fn sample_down(&mut self, depth: u32, current_depth: u32) {
        if current_depth >= depth {
            *self = match self.to_end() {
                (Some(t), _) => Node::Filled(t),
                (None, _) => Node::Empty,
            };
        } else {
            match self {
                Node::Empty => (),
                Node::Filled(_) => (),
                Node::Group(g) => {
                    for corner in Corner::all() {
                        g[corner as usize].sample_down(depth, current_depth + 1);
                    }
                } // Node::Packed(p) => {
                  //     p.sample_down(depth, current_depth + 1);
                  // }
            }
        }
    }

    pub fn make_dense(&mut self, layer: u32) {
        if layer == 0 {
            if let Some(end) = self.to_end().0 {
                *self = Node::Filled(end);
            } else {
                *self = Node::Empty;
            }
            return;
        }

        self.subdivide();
        let Node::Group(g) = self else { unreachable!() }; // due to subdivision

        for corner in Corner::all() {
            g[corner as usize].make_dense(layer - 1);
        }
    }

    pub fn to_end(&self) -> (Option<T>, u32) {
        let mut out: Option<T> = None;
        let mut node = self;
        let mut layer: u32 = 0;
        loop {
            match node {
                Node::Empty => {
                    break;
                }
                Node::Filled(t) => {
                    out = Some(t.clone());
                    break;
                }
                Node::Group(group) => {
                    layer += 1;
                    // let frequent = most_frequent(&**group, 2);
                    // node = match frequent[0].1 {
                    //     Node::Empty => frequent[1].1,
                    //     n => n,
                    // }
                    node = down(&**group);
                } // Node::Packed(pack) => {
                  //     layer += 1;
                  //     node = &pack;
                  // }
            }
        }

        (out, layer)
    }
}

fn down<T>(a: &[T]) -> &T {
    &a[0]
}

// https://stackoverflow.com/questions/64262297/rust-how-to-find-n-th-most-frequent-element-in-a-collection
// use std::cmp::{Eq, Ord, Reverse};
// use std::collections::{BinaryHeap, HashMap};
// use std::hash::Hash;

// pub fn most_frequent<T>(array: &[T], k: usize) -> Vec<(usize, &T)>
// where
//     T: Hash + Eq + Ord,
// {
//     let mut map = HashMap::new();
//     for x in array {
//         *map.entry(x).or_default() += 1;
//     }

//     let mut heap = BinaryHeap::with_capacity(k + 1);
//     for (x, count) in map.into_iter() {
//         heap.push(Reverse((count, x)));
//         if heap.len() > k {
//             heap.pop();
//         }
//     }
//     heap.into_sorted_vec().into_iter().map(|r| r.0).collect()
// }
