const SIDE_OFFSET: [[i8; 3]; 6] = [
    // left
    [-1, 0, 0],
    // right
    [1, 0, 0],
    // back
    [0, 0, -1],
    // front
    [0, 0, 1],
    // top
    [0, 1, 0],
    // bottom
    [0, -1, 0],
];

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum Side {
    Left = 0,
    Right = 1,
    Back = 2,
    Front = 3,
    Top = 4,
    Bottom = 5,
}
impl Side {
    pub const fn all() -> [Self; 6] {
        use Side::*;
        [Left, Right, Back, Front, Top, Bottom]
    }

    pub const fn get_offset(&self) -> [i8; 3] {
        SIDE_OFFSET[*self as usize]
    }

    pub fn get_opposite(&self) -> Self {
        use Side::*;
        match self {
            Left => Right,
            Right => Left,
            Back => Front,
            Front => Back,
            Top => Bottom,
            Bottom => Top,
        }
    }
}

const NEIGHBOUR_OFFSET: [[i8; 3]; 26] = [
    // left
    [-1, -1, -1],
    [-1, -1, 0],
    [-1, -1, 1],
    [-1, 0, -1],
    [-1, 0, 0],
    [-1, 0, 1],
    [-1, 1, -1],
    [-1, 1, 0],
    [-1, 1, 1],
    // middle
    [0, -1, -1],
    [0, -1, 0],
    [0, -1, 1],
    [0, 0, -1],
    // [0, 0, 0], -> must be omitted
    [0, 0, 1],
    [0, 1, -1],
    [0, 1, 0],
    [0, 1, 1],
    // right
    [1, -1, -1],
    [1, -1, 0],
    [1, -1, 1],
    [1, 0, -1],
    [1, 0, 0],
    [1, 0, 1],
    [1, 1, -1],
    [1, 1, 0],
    [1, 1, 1],
];

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum Neighbour {
    /* LEFT SIDE */
    // left bottom back
    LBB = 0,
    LBM = 1,
    LBF = 2,

    LMB = 3,
    LMM = 4,
    LMF = 5,

    LTB = 6,
    LTM = 7,
    LTF = 8,

    /* MIDDLE SIDE */
    MBB = 9,
    MBM = 10,
    MBF = 11,

    MMB = 12,
    // MMM -> must be omitted, bc this is no neighbour, but itself
    MMF = 13,

    MTB = 14,
    MTM = 15,
    MTF = 16,

    /* RIGHT SIDE */
    RBB = 17,
    RBM = 18,
    RBF = 19,

    RMB = 20,
    RMM = 21,
    RMF = 22,

    RTB = 23,
    RTM = 24,
    RTF = 25,
}

impl Neighbour {
    pub fn from_side(side: &Side) -> Self {
        use Neighbour::*;
        use Side::*;
        match side {
            Left => LMM,
            Right => RMM,
            Back => MMB,
            Front => MMF,
            Top => MTM,
            Bottom => MBM,
        }
    }

    pub fn get_offset(&self) -> [i8; 3] {
        NEIGHBOUR_OFFSET[*self as usize]
    }

    pub fn all() -> [Neighbour; 26] {
        use Neighbour::*;
        [
            LBB, LBM, LBF, LMB, LMM, LMF, LTB, LTM, LTF, MBB, MBM, MBF, MMB,
            // mmm omitted
            MMF, MTB, MTM, MTF, RBB, RBM, RBF, RMB, RMM, RMF, RTB, RTM, RTF,
        ]
    }

    pub fn get_opposite(&self) -> Self {
        use Neighbour::*;
        match self {
            LBB => RTF,
            LBM => RTM,
            LBF => RTB,
            LMB => RMF,
            LMM => RMM,
            LMF => RMB,
            LTB => RBF,
            LTM => RBM,
            LTF => RBB,

            MBB => MTF,
            MBM => MTM,
            MBF => MTB,
            MMB => MMF,
            MMF => MMB,
            MTB => MBF,
            MTM => MBM,
            MTF => MBB,

            RBB => LTF,
            RBM => LTM,
            RBF => LTB,
            RMB => LMF,
            RMM => LMM,
            RMF => LMB,
            RTB => LBF,
            RTM => LBM,
            RTF => LBB,
        }
    }
}
