use super::chunk::Chunk;
use super::chunk::SuperChunk;
use crate::error::ClientError;
use crate::User;
use crate::{Coord, PlayerCoord, Token};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum CTS {
    Register { name: String },
    Login { token: Token },
    Move { coord: PlayerCoord },
    PlaceStructure { structure: SuperChunk },
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum STC {
    Error(ClientError),
    Token(Token),
    LoggedIn(User),
    ChunkUnloads(Vec<Coord>),
    ChunkUpdates(Vec<(Chunk, Coord)>),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Event {
    Disconnect,
    Invalid,
    CTS(CTS),
    STC(STC),
}

pub mod prelude {
    pub use super::Event::*;
    pub use super::CTS::{self, *};
    pub use super::STC::{self, *};
    pub use crate::error::{ClientError, LoginError};
}
