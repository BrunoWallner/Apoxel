pub mod blocks;
pub mod channel;
pub mod chunk;
pub mod color;
pub mod data;
pub mod error;
pub mod event;
pub mod io;
pub mod types;
pub mod voxel;

use rand::Rng;
use serde::{Deserialize, Serialize};

const TCP_EVENT_BYTES: usize = 255;

pub type Token = [u8; 16];
pub type Coord = [i64; 3];
pub type Coord2D = [i64; 2];
pub type PlayerCoord = [f64; 3];

pub fn calculate_chunk_distance(p1: &Coord, p2: &Coord) -> i64 {
    let distance =
        (((p1[0] - p2[0]).pow(2) + (p1[1] - p2[1]).pow(2) + (p1[2] - p2[2]).pow(2)) as f64).sqrt();

    distance as i64
}

pub fn calculate_distance(p1: &PlayerCoord, p2: &PlayerCoord) -> f64 {
    let distance = (((p1[0] - p2[0]).powi(2) + (p1[1] - p2[1]).powi(2) + (p1[2] - p2[2]).powi(2))
        as f64)
        .sqrt();

    distance
}

pub fn coord_to_player_coord(coord: Coord) -> PlayerCoord {
    [coord[0] as f64, coord[1] as f64, coord[2] as f64]
}

pub fn player_coord_to_coord(player_coord: PlayerCoord) -> Coord {
    [
        player_coord[0] as i64,
        player_coord[1] as i64,
        player_coord[2] as i64,
    ]
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct User {
    pub name: String,
    pub pos: PlayerCoord,

    pub health: u8,

    pub online: bool,
}
impl User {
    pub fn new(name: String) -> (Token, Self) {
        let token = User::gen_token();
        (
            token,
            Self {
                name,
                pos: [0.0, 0.0, 0.0],
                health: 100,

                online: false,
            },
        )
    }

    // currently not secure
    fn gen_token() -> Token {
        let mut token = [0; 16];
        let mut rng = rand::thread_rng();
        for t in token.iter_mut() {
            *t = rng.gen();
        }
        token
    }
}

pub fn cpu_count() -> usize {
    num_cpus::get()
}

pub use log;

pub mod prelude {
    pub use super::blocks::Block;
    pub use super::channel::*;
    pub use super::chunk::*;
    pub use super::color::*;
    pub use super::error::*;
    pub use super::event::prelude::*;
    pub use super::io::*;
    pub use super::types::*;
    pub use super::{Coord, PlayerCoord, Token, *};
    pub use data::get_mem_size;
}
