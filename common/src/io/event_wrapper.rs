use crate::event::Event;
use crate::TCP_EVENT_BYTES;
use async_std::io::{self, ReadExt, WriteExt};

// reader
use flate2::read::ZlibDecoder;
use std::io::Read;

// writer
use flate2::write::ZlibEncoder;
use flate2::Compression;
use std::io::Write;

pub struct EventWrapper<T: ReadExt + WriteExt + Unpin> {
    rw: T,
}
impl<T: ReadExt + WriteExt + Unpin> EventWrapper<T> {
    pub fn new(socket: T) -> Self {
        Self { rw: socket }
    }

    // when len is 0 stream is closed
    // have to manually return error to remove 100% cpu cycle
    pub async fn read(&mut self, buffer: &mut [u8]) -> io::Result<()> {
        // @WARN: previously checked if amount of read bytes were 0 and
        // threw an BrokenPipe Error to not loop indefinetly
        self.rw.read_exact(buffer).await
    }

    pub async fn get_event(&mut self) -> io::Result<(Event, bool)> {
        let mut buffer: Vec<u8> = Vec::new();
        'get_bytes: loop {
            let mut buf = Box::new([0u8; TCP_EVENT_BYTES]);
            self.read(&mut (*buf)).await?;
            let completed = buf[0] == 1;

            buffer.append(&mut buf[2..buf[1] as usize].to_vec());

            if completed {
                break 'get_bytes;
            }
        }

        let mut decompressor = ZlibDecoder::new(&buffer[..]);
        let mut buffer: Vec<u8> = Vec::new();
        decompressor.read_to_end(&mut buffer)?;

        let important = buffer[0] == 1;

        let event: Event = bincode::deserialize(&buffer[1..]).unwrap_or(Event::Invalid);
        Ok((event, important))
    }

    pub async fn write(&mut self, buffer: &[u8]) -> io::Result<()> {
        self.rw.write_all(buffer).await?;
        Ok(())
    }

    pub async fn send_event(&mut self, event: &Event, important: bool) -> io::Result<()> {
        let mut encoded: Vec<u8> = bincode::serialize(event).unwrap();

        if important {
            encoded.insert(0, 1)
        } else {
            encoded.insert(0, 0);
        }

        // compress
        let mut compressor = ZlibEncoder::new(Vec::new(), Compression::default());
        compressor.write_all(&encoded)?;
        let compressed = compressor.finish()?;

        let bytes = byte_vector(&compressed);
        for bytes in bytes {
            self.write(&bytes).await?;
        }
        self.rw.flush().await?;

        Ok(())
    }
}

fn byte_vector(bytes: &[u8]) -> Vec<Vec<u8>> {
    let buffer_size = TCP_EVENT_BYTES - 2; // because of first byte that carries extra info

    let mut buffer: Vec<Vec<u8>> = Vec::new();

    let mut chunks = bytes.chunks(buffer_size).peekable();

    loop {
        if let Some(chunk) = chunks.next() {
            let len = chunk.len() as u8;
            let mut buf: Vec<u8> = Vec::new();

            let last = !chunks.peek().is_some();
            if last {
                buf.push(1);
            } else {
                buf.push(0);
            }

            buf.push(len + 2);

            buf.append(&mut chunk.to_vec());
            buf.append(&mut vec![0x00; buffer_size - chunk.len()]);
            buffer.push(buf);

            if last {
                break;
            }
        }
    }

    buffer
}
