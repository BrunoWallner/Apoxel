use crate::channel::*;
use crate::event::{Event, CTS, STC};

use async_std::net::TcpStream;

pub fn init(stream: TcpStream) -> (OutputCommunicator, InputCommunicator) {
    let (input_sender, input_receiver) = async_channel(None);
    let (output_sender, output_receiver) = async_channel(None);

    let input = InputCommunicator {
        receiver: input_receiver,
    };
    let output = OutputCommunicator {
        sender: output_sender,
    };

    init_local(stream, output_receiver, input_sender);

    (output, input)
}

#[derive(Debug)]
pub struct InputCommunicator {
    receiver: AsyncReceiver<Event>,
}
impl InputCommunicator {
    pub async fn get_event(&mut self) -> Result<Event, RecvError> {
        self.receiver.recv().await
    }

    pub fn try_get_event(&mut self) -> Result<Event, TryRecvError> {
        self.receiver.try_recv()
    }
}

#[derive(Debug, Clone)]
pub struct OutputCommunicator {
    sender: AsyncSender<Event>,
}
impl OutputCommunicator {
    pub async fn send_stc(&self, stc: STC) {
        let _ = self.sender.send(Event::STC(stc)).await;
    }

    pub async fn send_cts(&self, cts: CTS) {
        let _ = self.sender.send(Event::CTS(cts)).await;
    }

    pub async fn disconnect(&self) {
        let _ = self.sender.send(Event::Disconnect).await;
    }
}

use super::event_wrapper::EventWrapper;
use async_std::task;

fn init_local(stream: TcpStream, mut receiver: AsyncReceiver<Event>, sender: AsyncSender<Event>) {
    task::spawn(async move {
        let mut reader = EventWrapper::new(stream.clone());
        let mut writer = EventWrapper::new(stream);

        // init non-blocking reader
        task::spawn(async move {
            'running: loop {
                if let Ok((event, _important)) = reader.get_event().await {
                    if sender.send(event).await.is_err() {
                        break 'running;
                    };
                } else {
                    break 'running;
                }
            }
            drop(reader);
        });

        'running: loop {
            loop {
                match receiver.try_recv() {
                    Ok(event) => {
                        if writer.send_event(&event, false).await.is_err() {
                            break 'running;
                        }
                    }
                    Err(TryRecvError::Empty) => break,
                    Err(TryRecvError::Closed) => break 'running,
                }
            }
        }
        let _ = writer.send_event(&Event::Disconnect, false).await;
        drop(writer);
    });
}
