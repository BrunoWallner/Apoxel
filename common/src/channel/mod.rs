pub mod r#async;
pub mod sync;

pub use r#async::*;
pub use sync::*;

#[derive(Clone, Debug)]
pub enum TryRecvError {
    Closed,
    Empty,
}

impl From<crossbeam_channel::TryRecvError> for TryRecvError {
    fn from(value: crossbeam_channel::TryRecvError) -> Self {
        match value {
            crossbeam_channel::TryRecvError::Empty => TryRecvError::Empty,
            crossbeam_channel::TryRecvError::Disconnected => TryRecvError::Closed,
        }
    }
}

impl From<async_std::channel::TryRecvError> for TryRecvError {
    fn from(value: async_std::channel::TryRecvError) -> Self {
        match value {
            async_std::channel::TryRecvError::Closed => TryRecvError::Closed,
            async_std::channel::TryRecvError::Empty => TryRecvError::Empty,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct RecvError;

impl From<crossbeam_channel::RecvError> for RecvError {
    fn from(_value: crossbeam_channel::RecvError) -> Self {
        RecvError
    }
}

impl From<async_std::channel::RecvError> for RecvError {
    fn from(_value: async_std::channel::RecvError) -> Self {
        RecvError
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SendError<T>(pub T);

impl<T> From<crossbeam_channel::SendError<T>> for SendError<T> {
    fn from(value: crossbeam_channel::SendError<T>) -> Self {
        SendError(value.0)
    }
}
