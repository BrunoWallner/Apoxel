use crossbeam_channel::{bounded, unbounded, Receiver as CrossReceiver, Sender as CrossSender};

use super::{SendError, TryRecvError};
use std::clone::Clone;
use std::sync::{Arc, Mutex};
use std::time::Duration;

pub fn channel<T>(bound: Option<usize>) -> (Sender<T>, Receiver<T>) {
    let (sender, receiver) = if let Some(bound) = bound {
        bounded(bound)
    } else {
        unbounded()
    };
    (Sender { sender }, Receiver { receiver })
}

#[derive(Debug, Clone)]
pub struct Sender<T> {
    sender: CrossSender<T>,
}
impl<T> Sender<T> {
    pub fn send(&self, t: T) -> Result<(), SendError<T>> {
        match self.sender.send(t) {
            Ok(()) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }
}

#[derive(Debug)]
pub struct Receiver<T> {
    receiver: CrossReceiver<T>,
}
impl<T> Receiver<T> {
    pub fn recv(&self) -> Option<T> {
        match self.receiver.recv() {
            Ok(t) => Some(t),
            Err(_) => None,
        }
    }

    pub fn try_recv(&self) -> Result<T, TryRecvError> {
        match self.receiver.try_recv() {
            Ok(t) => Ok(t),
            Err(e) => Err(e.into()),
        }
    }

    pub fn recv_timeout(&self, timeout: Duration) -> Option<T> {
        match self.receiver.recv_timeout(timeout) {
            Ok(t) => Some(t),
            Err(_) => None,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Broadcast<T> {
    senders: Arc<Mutex<Vec<Sender<T>>>>,
}

impl<T: Clone> Broadcast<T> {
    pub fn new() -> Self {
        Self {
            senders: Arc::new(Mutex::new(vec![])),
        }
    }

    pub fn get_receiver(&self) -> Option<Receiver<T>> {
        let (s, r) = channel(None);
        // append sender to Broadcast
        if let Ok(mut senders) = self.senders.lock() {
            senders.push(s);
            Some(r)
        } else {
            None
        }
    }

    pub fn send(&self, t: T) {
        let mut senders = self.senders.lock().unwrap();
        let mut to_delete: Vec<usize> = Vec::new();
        for (index, sender) in senders.iter().enumerate() {
            if sender.send(t.clone()).is_err() {
                to_delete.push(index);
            }
        }
        for (i, d) in to_delete.iter().enumerate() {
            senders.remove(d - i);
        }
    }
}
