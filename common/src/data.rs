use flate2::write::ZlibEncoder;
use flate2::Compression;
use serde::{Deserialize, Serialize};
use std::io::Write;

pub use serde;

/// first uncompressed second compressed
pub fn get_mem_size<T: Serialize>(t: &T) -> (usize, usize) {
    let encoded: Vec<u8> = bincode::serialize(t).unwrap();
    let uncompressed = encoded.len();

    let mut compressor = ZlibEncoder::new(Vec::new(), Compression::default());
    compressor.write_all(&encoded).unwrap();
    let compressed = compressor.finish().unwrap().len();

    (uncompressed, compressed)
}

pub fn compress<T: Serialize>(t: &T) -> Option<Vec<u8>> {
    match bincode::serialize(t) {
        Ok(encoded) => {
            let mut compressor = ZlibEncoder::new(Vec::new(), Compression::default());
            compressor.write_all(&encoded).ok()?;
            let compressed = compressor.finish().ok()?;
            Some(compressed)
        }
        Err(_) => None,
    }
}

use flate2::read::ZlibDecoder;
use std::io::Read;

pub fn uncompress(t: &[u8]) -> Option<Vec<u8>> {
    let mut decompressor = ZlibDecoder::new(t);
    let mut buffer: Vec<u8> = Vec::new();
    decompressor.read_to_end(&mut buffer).ok()?;

    Some(buffer)
}

pub fn deserialize<'a, T: Deserialize<'a>>(t: &'a [u8]) -> Option<T> {
    match bincode::deserialize(t) {
        Ok(t) => Some(t),
        Err(_) => None,
    }
}

pub fn serialize<T: Serialize>(t: &T) -> Option<Vec<u8>> {
    match bincode::serialize(t) {
        Ok(t) => Some(t),
        Err(_) => None,
    }
}
