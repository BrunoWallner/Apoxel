use crate::prelude::octree::*;
use crate::prelude::Block;
use crate::prelude::Chunk;
use crate::prelude::Color;
use dot_vox::load_bytes;
use glam::IVec3;
use glam::Vec3Swizzles;

pub fn parse(bytes: &[u8]) -> Option<Chunk> {
    let mut chunk: Octree<Block> = Octree::default();

    let vox_data = load_bytes(bytes).ok()?;
    let palette = vox_data.palette;

    log::info!("importing vox ...");
    let mut global_size: [IVec3; 2] = [IVec3::ZERO; 2];
    vox_data.scenes.iter().for_each(|scene| {
        let dot_vox::SceneNode::Transform { frames, .. } = scene else {
            return;
        };
        let Some(transform) = &frames[0].attributes.get("_t") else {
            return;
        };
        let c = transform.split_whitespace().collect::<Vec<_>>();
        let x: i32 = c[0].parse().unwrap();
        let y: i32 = c[1].parse().unwrap();
        let z: i32 = c[2].parse().unwrap();
        let v = IVec3::new(x, y, z);
        global_size[0] = global_size[0].min(v);
        global_size[1] = global_size[1].max(v);
    });

    for scene in &vox_data.scenes {
        let dot_vox::SceneNode::Transform { frames, child, .. } = scene else {
            continue;
        };
        let Some(transform) = &frames[0].attributes.get("_t") else {
            continue;
        };
        let c = transform.split_whitespace().collect::<Vec<_>>();
        let x: i32 = c[0].parse().unwrap();
        let y: i32 = c[1].parse().unwrap();
        let z: i32 = c[2].parse().unwrap();
        let offset = IVec3::new(x, y, z) + global_size[0].abs(); // always positive

        let dot_vox::SceneNode::Shape { models, .. } = &vox_data.scenes[*child as usize] else {
            continue;
        };
        for model in models {
            let model = &vox_data.models[model.model_id as usize];
            let chunk_size = 1024 * 2;

            for voxel in model.voxels.iter() {
                let color = palette.get(voxel.i as usize)?;
                let color = Color::new(color.r, color.g, color.b);
                let block = Block::Color(color);
                let c = IVec3::new(voxel.x as i32, voxel.y as i32, voxel.z as i32);

                let c = c + offset;
                let c = c.xzy(); // swap y and z

                chunk.set_xyz(
                    chunk_size, // rotate axis, opposing standards
                    c.x as u32, c.y as u32, c.z as u32, block,
                );
            }
        }
    }
    log::info!("imported vox");

    Some(chunk)
}
