// #define_import_path chunks_edit

// const UPDATE_ITEM_SIZE: u32 = 4u;

// fn remove_node(mem_offset: u32) {
//     var parent = chunks::load(mem_offset + 1u);
//     let octant = (chunks::load(mem_offset) >> 1u) & 7u;
//     // unmark from parent childmask
//     var d = chunks::load(parent);
//     d &= ~(1u << 4u + octant);
//     chunks::store(parent, d);

//     // if parent index == 0u the node is the root node
//     while parent != 0u {
//         // let parent_child_mask = (data[parent] >> 4u) & 0xFFu;
//         let parent_child_mask = (chunks::load(parent) >> 4u) & 0xFFu;
//         // recursively remove parents if it does not contain any child
//         if parent_child_mask == 0u {
//             let octant = (chunks::load(parent) >> 1u) & 7u;
//             parent = chunks::load(parent + 1u);
//             var d = chunks::load(parent);
//             d &= ~(1u << 4u + octant);
//             chunks::store(parent, d);
//         } else {
//             break;
//         }
//     }
// }

// fn set_light(mem_offset: u32, light: vec3<f32>, a: f32) {
//     var alpha = a;

//     var node = chunks::load(mem_offset);
//     let node_type = node & 1u;
//     if node_type != 0u {return;} // return if not leaf
//     let block_kind = (node >> 4u) & 0xFu;
//     if block_kind == 1u {return;} // if light return
//     // check if not already lit
//     let already_lit = (node >> 8u) & 1u;
//     if already_lit == 0u {
//         alpha *= 10.0;
//         node |= 1u << 8u; // set to already lit
//         chunks::store(mem_offset, node);
//     }

//     for (var c = 0u; c < 3u; c += 1u) {
//         var l = chunks::load(mem_offset + 3u + c);
//         var l_f32 = bitcast<f32>(l);
//         l_f32 = mix(l_f32, light[c], alpha);
//         l = bitcast<u32>(l_f32);
//         chunks::store(mem_offset + 3u + c, l);
//     }
// }

// fn place_light(mem_offset: u32, light: vec3<f32>) {
//     var node = chunks::load(mem_offset);
//     let node_type = node & 1u;
//     if node_type != 0u {return;} // return if not leaf

//     // set to light block
//     node &= ~(0xFu << 4u);
//     node |= 1u << 4u;
//     chunks::store(mem_offset, node);

//     let lr = bitcast<u32>(light.r);
//     let lg = bitcast<u32>(light.g);
//     let lb = bitcast<u32>(light.b);
//     chunks::store(mem_offset + 3u, lr);
//     chunks::store(mem_offset + 4u, lg);
//     chunks::store(mem_offset + 5u, lb);
// }

// fn push_light_update(position: vec3<f32>) {
//     if update_exists(position, 0.001) {return;}
//     var mem_offset = 0u;
//     while mem_offset < chunks::vars.update_size {
//         var header = chunks::update[mem_offset];
//         let there = header & 1u;

//         if there != 1u {
//             header &= 0u; // clear
//             header |= 1u; // mark as there
//             chunks::update[mem_offset] = header;
//             chunks::update[mem_offset + 1u] = bitcast<u32>(position.x);
//             chunks::update[mem_offset + 2u] = bitcast<u32>(position.y);
//             chunks::update[mem_offset + 3u] = bitcast<u32>(position.z);
//             return;
//         } else {
//             mem_offset += UPDATE_ITEM_SIZE; 
//         }
//     }
// }

// fn update_exists(position: vec3<f32>, threshold: f32) -> bool {
//     var mem_offset = 0u;
//     while mem_offset < chunks::vars.update_size {
//         var header = chunks::update[mem_offset];
//         let there = header & 1u;
//         if there == 1u {
//             let p = vec3<f32>(
//                 bitcast<f32>(chunks::update[mem_offset + 1u]),
//                 bitcast<f32>(chunks::update[mem_offset + 2u]),
//                 bitcast<f32>(chunks::update[mem_offset + 3u]),
//             );
//             if distance(position, p) < threshold {
//                 return true;
//             } else {
//                 mem_offset += UPDATE_ITEM_SIZE;
//             }
//         } else {
//             mem_offset += UPDATE_ITEM_SIZE;
//         }
//     }
//     return false;
// }

// struct UpdatePull {
//     valid: bool,
//     position: vec3<f32>,
// }

// fn pull_update() -> UpdatePull {
//     var pull = UpdatePull(false, vec3<f32>(0.0));
//     var mem_offset = 0u;
//     while mem_offset < chunks::vars.update_size {
//         var header = chunks::update[mem_offset];
//         let there = header & 1u;
//         if there == 1u {
//             header &= 0u; // clear
//             chunks::update[mem_offset] = header;

//             let p = vec3<f32>(
//                 bitcast<f32>(chunks::update[mem_offset + 1u]),
//                 bitcast<f32>(chunks::update[mem_offset + 2u]),
//                 bitcast<f32>(chunks::update[mem_offset + 3u]),
//             );
//             pull.valid = true;
//             pull.position = p;
//             break;
//         }
//         mem_offset += UPDATE_ITEM_SIZE;
//     }

//     return pull;
// }

