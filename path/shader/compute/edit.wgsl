// @group(1)
// @binding(0)
// var<storage, read> camera: Camera;
// struct Camera {
//     position: vec3<f32>,
//     yaw: f32,
//     pitch: f32,
//     fov: f32,
// }

// @group(1)
// @binding(1)
// var<storage, read_write> vars: Vars;
// struct Vars {
//     random_seed: u32,
//     to_remove: u32,
// }

// @compute
// @workgroup_size(1, 1, 1)
// fn main(
//     @builtin(global_invocation_id) global_id: vec3<u32>,
// ) {
//     // var relative_render_position = vec2<f32>(0.5, 0.5);
//     if vars.to_remove == 0u {return;}
//     let yaw = mat3x3<f32>(
//         vec3<f32>(cos(camera.yaw), 0.0, sin(camera.yaw)),
//         vec3<f32>(0.0, 1.0, 0.0),
//         vec3<f32>(-sin(camera.yaw), 0.0, cos(camera.yaw)),
//     );
//     let pitch = mat3x3<f32>(
//         vec3<f32>(1.0, 0.0, 0.0),
//         vec3<f32>(0.0, cos(camera.pitch), -sin(camera.pitch)),
//         vec3<f32>(0.0, sin(camera.pitch), cos(camera.pitch)),
//     );
//     let rotation = yaw * pitch;

//     let origin: vec3<f32> = camera.position;
//     let ray_front = vec2<f32>(0.0, 0.0);
//     let direction = normalize(vec3<f32>(ray_front, pow(camera.fov, 4.0)));
//     let ray = ray::Ray(origin, rotation * direction);

//     // chunks::remove_node(chunk_hit.mem_offset);

//     let chunk_hit = chunks::hit(ray, camera.position, 1000.0);
//     // let offset = vec3<i32>(global_id) - vec3<i32>(3);
//     if chunk_hit.hit && vars.to_remove == 1u {
//         var pos = chunk_hit.node_position;
//         let size = chunk_hit.node_size;
//         let r = 4;
//         for (var x = -r; x < r; x += 1) {
//             for (var y = -r; y < r; y += 1) {
//                 for (var z = -r; z < r; z += 1) {
//                     let offset = vec3<i32>(x, y, z);
//                     let p = chunks::point(pos + vec3<f32>(offset) * size, 100u);
//                     if p.hit {
//                         // chunks_edit::remove_node(p.mem_offset);
//                     }

//                 }
//             }
//         }
//     }
//     if chunk_hit.hit && vars.to_remove == 2u {
//         let r = vars.random_seed;
//         let color = normalize(vec3<f32>(noise::rng(r), noise::rng(r + 1u), noise::rng(r + 2u)));
//         // chunks_edit::place_light(chunk_hit.mem_offset, color * 30.0);
//     }
// }
