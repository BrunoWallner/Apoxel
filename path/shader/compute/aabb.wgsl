#define_import_path aabb

#import ray

struct Aabb {
    min: vec3<f32>,
    max: vec3<f32>,
}

struct AabbHit {
    hit: bool,
    distance: f32,
};

fn hit(aabb: Aabb, ray: ray::Ray) -> AabbHit {
    var hit = AabbHit(false, 0.0);

    var inv_dir = vec3<f32>(1.0) / ray.direction;
    var t1 = (aabb.min - ray.origin) * inv_dir;
    var t2 = (aabb.max - ray.origin) * inv_dir;
    var tmin = min(t1, t2);
    var tmax = max(t1, t2);

    var min_tmax = min(tmax.x, min(tmax.y, tmax.z));
    var max_tmin = max(tmin.x, max(tmin.y, tmin.z));

    if min_tmax >= max_tmin && max_tmin >= 0.0 {
        hit.distance = max_tmin;
        // hit.relative_position = ray.origin + ray.direction * hit.distance - aabb.min;
        hit.hit = true;
    }

    return hit;
}

fn in_bound(aabb: Aabb, point: vec3<f32>) -> bool {
    let in_bound = all(point >= aabb.min) && all(point <= aabb.max);
    return in_bound;
}

fn exit_distance(aabb: Aabb, ray: ray::Ray) -> AabbHit {
    var hit = AabbHit(false, 0.0);

    let tMin: vec3<f32> = (aabb.min - ray.origin) / ray.direction;
    let tMax: vec3<f32> = (aabb.max - ray.origin) / ray.direction;

    let t1: vec3<f32> = min(tMin, tMax);
    let t2: vec3<f32> = max(tMin, tMax);

    let tNear: f32 = max(max(t1.x, t1.y), t1.z);
    let tFar: f32 = min(min(t2.x, t2.y), t2.z);

    if tNear > tFar || tFar < 0.0 {
        // // No intersection, or the AABB is behind the ray
        return hit;
    }

    if tNear > 0.0 {
        return hit;
    }

    // Calculate the distance from the ray origin to the nearest face of the AABB
    hit.hit = true;
    hit.distance = tFar;
    return hit;
}

// fn exit_plane(aabb: Aabb, ray: ray::Ray) -> vec3<f32> {
//     // Calculate tMin and tMax
//     let tMin: vec3<f32> = (aabb.min - ray.origin) / ray.direction;
//     let tMax: vec3<f32> = (aabb.max - ray.origin) / ray.direction;

//     let t1: vec3<f32> = min(tMin, tMax);
//     let t2: vec3<f32> = max(tMin, tMax);

//     // Find the farthest intersection point
//     let tFar: f32 = min(min(t2.x, t2.y), t2.z);

//     // Tolerance to avoid floating-point comparison issues
//     let epsilon: f32 = 1e-6;

//     // Determine which axis is responsible for tFar
//     if abs(tFar - t2.x) < epsilon {
//         // Exiting on X plane
//         return vec3<f32>(sign(ray.direction.x), 0.0, 0.0);
//     } else if abs(tFar - t2.y) < epsilon {
//         // Exiting on Y plane
//         return vec3<f32>(0.0, sign(ray.direction.y), 0.0);
//     } else {
//         // Exiting on Z plane
//         return vec3<f32>(0.0, 0.0, sign(ray.direction.z));
//     }
// }

fn exit_plane(aabb: Aabb, ray: ray::Ray) -> vec3<f32> {
    var tmin = (aabb.min.x - ray.origin.x) / ray.direction.x;
    var tmax = (aabb.max.x - ray.origin.x) / ray.direction.x;

    if tmin > tmax {
        let temp = tmin;
        tmin = tmax;
        tmax = temp;
    }

    var tymin = (aabb.min.y - ray.origin.y) / ray.direction.y;
    var tymax = (aabb.max.y - ray.origin.y) / ray.direction.y;

    if tymin > tymax {
        let temp = tymin;
        tymin = tymax;
        tymax = temp;
    }

    if (tmin > tymax) || (tymin > tmax) {
        // return ExitPlane.None;
        return vec3<f32>(0.0);
    }

    if tymin > tmin {
        tmin = tymin;
    }
    if tymax < tmax {
        tmax = tymax;
    }

    var tzmin = (aabb.min.z - ray.origin.z) / ray.direction.z;
    var tzmax = (aabb.max.z - ray.origin.z) / ray.direction.z;

    if tzmin > tzmax {
        let temp = tzmin;
        tzmin = tzmax;
        tzmax = temp;
    }

    if (tmin > tzmax) || (tzmin > tmax) {
        // return ExitPlane.None;
        return vec3<f32>(0.0);
    }

    if tzmin > tmin {
        tmin = tzmin;
    }
    if tzmax < tmax {
        tmax = tzmax;
    }

    // Determine which plane the ray exits
    if tmax == (aabb.max.x - ray.origin.x) / ray.direction.x {
        // return ExitPlane.XMax;
        return vec3<f32>(1.0, 0.0, 0.0);
    } else if tmax == (aabb.min.x - ray.origin.x) / ray.direction.x {
        // return ExitPlane.XMin;
        return vec3<f32>(-1.0, 0.0, 0.0);
    } else if tmax == (aabb.max.y - ray.origin.y) / ray.direction.y {
        // return ExitPlane.YMax;
        return vec3<f32>(0.0, 1.0, 0.0);
    } else if tmax == (aabb.min.y - ray.origin.y) / ray.direction.y {
        // return ExitPlane.YMin;
        return vec3<f32>(0.0, -1.0, 0.0);
    } else if tmax == (aabb.max.z - ray.origin.z) / ray.direction.z {
        // return ExitPlane.ZMax;
        return vec3<f32>(0.0, 0.0, 1.0);
    } else if tmax == (aabb.min.z - ray.origin.z) / ray.direction.z {
        // return ExitPlane.ZMin;
        return vec3<f32>(0.0, 0.0, -1.0);
    }

    return vec3<f32>(0.0);
}
