struct StorageTexture {
    width: u32,
    height: u32,
    data: array<vec4<u32>>,
}

@group(1)
@binding(1)
var<storage, read_write> position_storage: StorageTexture;

@group(1)
@binding(2)
var<storage, read_write> color_storage: StorageTexture;

@group(1)
@binding(3)
var output_texture: texture_storage_2d<rgba32float, write>;

@compute
@workgroup_size(16, 16)
fn finalize(
    @builtin(global_invocation_id) global_id: vec3<u32>,
) {
    let uv = vec2<u32>(global_id.x, global_id.y);
    let index = uv.x + uv.y * color_storage.width;
    let data = color_storage.data[index];
    let color  = vec3<f32>(
        bitcast<f32>(data.r),
        bitcast<f32>(data.g),
        bitcast<f32>(data.b),
    );
    if all(color == vec3<f32>(0.0)) {
        textureStore(output_texture, uv, vec4<f32>(0.0));
        return;
    }
    textureStore(output_texture, uv, vec4<f32>(color, 1.0));

    // // let hash = hashmap::hash_vec(position);
    // let hash = index;
    // // hashmap::insert(hash, bitcast<u32>(1.0));
    // let value = hashmap::retrieve(hash);
    // // let value = bitcast<f32>(color_storage.data[index].r);


    // let color_store = vec4<f32>(bitcast<f32>(value));
    // // let color_store = vec4<f32>(f32(hash) / pow(2.0, 32.0));

    // // let color_store = vec4<f32>(position, 1.0);
    // textureStore(output_texture, uv, color_store);

    // let value = hashmap::retrieve(index);

    // let color_store = vec4<f32>(bitcast<f32>(value));
    // let color_store = vec4<f32>(position, 1.0);
    // let color = f32(uv.x) / f32(color_storage.width);
    // let color_store = vec4<f32>(color);
    // textureStore(output_texture, uv, color_store);
}

@compute
@workgroup_size(16, 16)
fn lighting(
    @builtin(global_invocation_id) global_id: vec3<u32>,
) {
    // let uv = vec2<u32>(global_id.x, global_id.y);
    // let index = uv.x + uv.y * color_storage.width;
    // let data = position_storage.data[index];
    // let position = vec3<f32>(
    //     bitcast<f32>(data.r),
    //     bitcast<f32>(data.g),
    //     bitcast<f32>(data.b),
    // );
    // // let hash = hashmap::hash_vec(position);
    // let hash = index;
    // let color = bitcast<f32>(color_storage.data[index].r);

    // hashmap::insert(hash, bitcast<u32>(1.0));
    // let color = f32(uv.x) / f32(color_storage.width);
    // let color = f32(uv.x) / f32(color_storage.width);
    // hashmap::insert(index, bitcast<u32>(color));
}

