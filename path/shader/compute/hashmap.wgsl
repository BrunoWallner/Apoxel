#define_import_path hashmap

const NODE_SIZE: u32 = 4u;
const BUCKETS: u32 = 128;
const MUTEXES: u32 = BUCKETS + 1u;
const ITERATIONS: u32 = 15000u;

const HEAP_OFFSET: u32 = BUCKETS * NODE_SIZE;

/*
Node:
 - u32: occupied,
 - u32: key,
 - u32: value,
 - u32: next,
*/

@group(1)
@binding(0)
var<storage, read_write> map: HashMap;

struct HashMap {
    // // stack_end: atomic<u32>,
    // heap_end: atomic<u32>,
    // mutexes: array<atomic<u32>, MUTEXES>,
    // // contains buckets and linked list
    // data: array<u32>,
    buckets: array<atomic<u32>, BUCKETS>,
    data: array<u32>,
}

fn get_index(
    key: u32,
) -> u32 {
    let index = key % BUCKETS;
    return index * NODE_SIZE;
}

fn retrieve(key: u32) -> u32 {
    let start_index  = get_index(key);
    var index = start_index;

    // // check if mem is initialized
    // let stack_end = atomicLoad(&map.stack_end);
    // if index >= stack_end {
    //     // progressively init mem
    //     for (var i = 0u; i < 4; i += 1u) {
    //         map.data[stack_end + i] = 0u;
    //     }
    //     atomicAdd(&map.stack_end, 4u);
    //     return 0u;
    // }

    // lock_index(start_index + 1);
    lock_index(0u);

    var return_value: u32 = 0;
    for (var i = 0u; i < ITERATIONS; i += 1u) {
    // loop {
        // if not occupied
        if map.data[index] == 0 {
            break;
        }
        // if key is the same
        if map.data[index + 1u] == key {
            return_value = map.data[index + 2];
            break;
        }
        // not yet found, traverse linked list
        let next = map.data[index + 3];
        if next == 0 {
            break;
        }
        index = next;
    }

    // unlock_index(start_index + 1);
    unlock_index(0u);
    return return_value;
}

fn insert(key: u32, value: u32) {
    let start_index  = get_index(key);
    var index = start_index;
    // lock_index(start_index + 1);
    lock_index(0u); // lock heap_offset

    // loop {
    for (var i = 0u; i < ITERATIONS; i += 1u) {
        // if not occupied, insert straight away
        if map.data[index] == 0u {
            map.data[index] = 1u;
            map.data[index + 1u] = key;
            map.data[index + 2u] = value;
            map.data[index + 3u] = 0u;
            break;
        }
        // node is already occupied
        if map.data[index + 1] == key {
            // just update the value, bc same key
            map.data[index + 2] = value;
            break;
        }

        let next = map.data[index + 3];
        if next != 0u {
            // traverse the linked list further
            index = next;
        } else {
            // end of linked list reached
            // insert it further down the linked list
            let next_index = atomicLoad(&map.heap_end) + HEAP_OFFSET;
            // map.heap_offset += NODE_SIZE;
            atomicAdd(&map.heap_end, NODE_SIZE);
            map.data[index + 3] = next_index;

            map.data[next_index] = 1u;
            map.data[next_index + 1u] = key;
            map.data[next_index + 2u] = value;
            map.data[next_index + 3u] = 0u;

            index = next_index;
            break;
        }
    }

    unlock_index(0u);
    // unlock_index(start_index + 1);
}


// fn lock_index(index: u32) {
//     let mutex = &map.mutexes[index];

//     loop {
//         let result = atomicCompareExchangeWeak(mutex, 0u, 1u);
//         if result.exchanged {
//             return;
//         }
//     }
// }

// fn unlock_index(index: u32) {
//     let mutex = &map.mutexes[index];
//     atomicStore(mutex, 0u);
// }

fn lock_index(index: u32) {
    let mutex = &map.mutexes[index];
    var backoff_time = 1u;

    loop {
        let result = atomicCompareExchangeWeak(mutex, 0u, 1u);
        if result.exchanged {
            // Memory fence to ensure visibility of writes
            workgroupBarrier();
            storageBarrier();
            return;
        }

        // Exponential backoff to reduce contention
        for (var i = 0u; i < backoff_time; i = i + 1u) {
            // Busy-wait for a bit (backoff delay)
        }
        backoff_time = backoff_time * 2u; // Increase the backoff delay
    }
}

fn unlock_index(index: u32) {
    let mutex = &map.mutexes[index];
    // Memory fence to ensure writes are visible before unlocking
    workgroupBarrier();
    storageBarrier();
    atomicStore(mutex, 0u);
}

fn hash_vec(vec: vec3<f32>) -> u32 {
    let x = bitcast<u32>(vec.x);
    let y = bitcast<u32>(vec.y);
    let z = bitcast<u32>(vec.z);
    
    var hash: u32 = 2166136261u; // FNV offset basis
    let prime: u32 = 16777619u; // FNV prime

    hash = (hash ^ x) * prime;
    hash = (hash ^ y) * prime;
    hash = (hash ^ z) * prime;

    return hash;
}
