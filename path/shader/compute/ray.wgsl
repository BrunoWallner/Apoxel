#define_import_path ray

struct Ray {
    origin: vec3<f32>,
    direction: vec3<f32>,
};

struct Material {
    color: vec3<f32>,
    brightness: f32,
    metal: f32,
}

struct Hit {
    hit: bool,
    distance: f32,
    normal: vec3<f32>,
    position: vec3<f32>,
    material: Material,
}

struct ChunkHit {
    hit: bool,
    mem_offset: u32,
    distance: f32,
    normal: vec3<f32>,
    position: vec3<f32>,
    color: vec3<f32>,
    light: vec3<f32>,
    node_size: f32,
    node_position: vec3<f32>,
    node_header: u32,
}

fn new_material() -> Material {
    return Material(
        vec3<f32>(0.0),
        0.0,
        0.0,
    );
}

fn new_hit() -> Hit {
    return Hit(
        false,
        0.0,
        vec3<f32>(0.0),
        vec3<f32>(0.0),
        new_material(),
    );
}

fn new_chunkhit() -> ChunkHit {
    return ChunkHit(
        false,
        0u,
        0.0,
        vec3<f32>(0.0),
        vec3<f32>(0.0),
        vec3<f32>(0.0),
        vec3<f32>(0.0),
        0.0,
        vec3<f32>(0.0),
        0u,
    );
}
