// STRUCTS
struct VertexInput {
    @location(0) position: vec2<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) position: vec2<f32>,
};

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.clip_position = vec4<f32>(model.position, 0.0, 1.0);
    // out.position = (model.position + 1.0) / 2.0;
    out.position = model.position;
    return out;
}

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;

struct Vars {
    surface_width: u32,
    surface_height: u32,
}

@group(1) @binding(0)
var<storage, read> vars: Vars;


@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    let aspect_ratio = f32(vars.surface_width) / f32(vars.surface_height);
 
    var uv = in.position;
    // let a = length(uv);
    // let m = sqrt(abs(a)) * sign(a);
    // uv *= m;

    uv = (uv + 1.0) / 2.0;
    let size = textureDimensions(t_diffuse);
    var pixel_position = vec2<u32>(vec2<f32>(size) * uv);
    
    var color = sample(pixel_position);
    // color = pow(color, vec4<f32>(1.0 / 2.2));

    let target_size: f32 = f32(vars.surface_width * vars.surface_height);
    let target_distance = normalize(vec2<f32>(target_size, target_size * aspect_ratio));
    let from_center = abs(vec2<f32>(0.5) - uv);
    if all(from_center < target_distance * 0.01) {
        color = vec4<f32>(1.0) - color;
    }
    // if distance(from_center * vec2<f32>(aspect_ratio, 1.0), vec2<f32>(0.0)) < s {
    //     // color = vec4<f32>(0.0);
    //     color = vec4<f32>(1.0) - color;
    // }


    return color;
}

fn sample(uv: vec2<u32>) -> vec4<f32> {
    return textureLoad(t_diffuse, uv, 0);
}

fn furthest_from_zero(a: f32, b: f32) -> f32 {
    let abs_a = abs(a);
    let abs_b = abs(b);
    
    if abs_a > abs_b {
        return a;
    } else {
        return b;
    }
}
