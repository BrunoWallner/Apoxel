use std::fs;
use std::process;

use naga_oil::compose::{
    ComposableModuleDescriptor, Composer, ComposerError, NagaModuleDescriptor,
};

use wgpu::naga;

pub fn shader_from_folder(folder_path: &str, entry: &str, modules: &[&str]) -> naga::Module {
    if !folder_path.ends_with('/') {
        eprintln!("folder path does not end with '/'");
        process::exit(1);
    }
    match import(folder_path, entry, modules) {
        Ok(m) => m,
        Err((error, composer)) => {
            eprintln!("{}", error.emit_to_string(&composer));
            process::exit(1);
        }
    }
}

fn import(folder_path: &str, entry: &str, modules: &[&str]) -> Result<naga::Module, (ComposerError, Composer)> {
    let mut composer = Composer::default();

    // for entry in fs::read_dir(folder).unwrap() {
    for module in modules {
        // let entry = entry.unwrap();

        // if let Some(extension) = entry.path().extension() {
        // if extension == "wgsl" {
        // let module_name = entry.file_name().into_string().unwrap();
        // let module_name = module_name.split('.').collect::<Vec<_>>()[0];
        let module_name = module;

        // let module_path = entry.path().into_os_string().into_string().unwrap();
        let module_path = String::from(folder_path) + module_name + ".wgsl";
        let Ok(module_source) = fs::read_to_string(&module_path) else {
            // return Err((ComposerError::from(()), composer));
            panic!("module: {} not found", module_path);
        };

        let module_descriptor = ComposableModuleDescriptor {
            source: &module_source,
            file_path: &module_path,
            // language: naga_oil::compose::ShaderLanguage::Wgsl,
            // as_name: Some(String::from(module_name)),
            ..Default::default()
        };
        match composer.add_composable_module(module_descriptor) {
            Ok(_) => (),
            Err(e) => return Err((e, composer)), // Err(e) => {
                                                 //     println!("got error: {}", e);
                                                 //     println!("on: {}", module_name);
                                                 // }
        }

        // println!("adding: {}", module_name);
        // }
        // }
    }

    let source_path = folder_path.to_string() + entry + ".wgsl";
    let source = fs::read_to_string(&source_path).unwrap();
    let module_desc = NagaModuleDescriptor {
        source: &source,
        file_path: &source_path,
        ..Default::default()
    };
    // match composer.make_naga_module(module_desc) {
    //     Ok(module) => {
    //         Ok(module)
    //     }
    //     Err(e) => {
    //         eprintln!("{}", e.emit_to_string(&composer));
    //         process::exit(1);
    //     }
    // }
    match composer.make_naga_module(module_desc) {
        Ok(m) => Ok(m),
        Err(e) => Err((e, composer)),
    }
}
