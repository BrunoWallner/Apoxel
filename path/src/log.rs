
use common::channel::*;
use log::LevelFilter;
use log::SetLoggerError;

#[derive(Clone, Debug)]
pub struct LogEvent {
    pub level: log::Level,
    pub message: String,
    pub target: String,
}

#[derive(Clone, Debug)]
pub struct Logger {
    pub sender: Sender<LogEvent>,
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        let target = record.metadata().target();
        let _ = self.sender.send(LogEvent {
            level: record.metadata().level(),
            message: format!("{}", record.args()),
            target: String::from(target),
        });
    }

    fn flush(&self) {}
}

#[allow(clippy::new_ret_no_self)]
impl Logger {
    pub fn new() -> Result<Receiver<LogEvent>, SetLoggerError> {
        let (sender, receiver) = channel(None);
        let logger = Logger { sender };

        log::set_boxed_logger(Box::new(logger))?;
        log::set_max_level(LevelFilter::Info);

        Ok(receiver)
    }
}
