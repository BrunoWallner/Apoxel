use egui_plot::{Line, Plot, PlotPoints};

mod import;

use common::{
    channel::{Receiver, Sender},
    chunk::Chunk,
    log::Level,
};
use egui::{Align2, Color32, Context, Resize, ScrollArea, Ui};

use crate::{
    app,
    log::{LogEvent, Logger},
};

const MAX_LOG: usize = 1000;

pub struct Gui {
    logger: Receiver<LogEvent>,
    logs: Vec<LogEvent>,
    picked_path: Option<String>,
    chunk_tx: Sender<Chunk>,

    import: import::Import,

    pub lod_distance: f32,
    pub shadows: bool,
    pub ao_resolution: u32,
    pub ao_strength: f32,
    timings: Vec<f32>,
}
impl app::GuiTrait for Gui {
    fn update(&mut self, dt: f32) {
        while let Ok(log) = self.logger.try_recv() {
            let blacklist = ["wgpu_core", "calloop", "wgpu_hal"];
            for b in blacklist {
                if log.target.contains(b) {
                    return;
                }
            }

            self.logs.push(log);
            if self.logs.len() > MAX_LOG {
                self.logs.remove(0);
            }
        }

        self.timings.push(1000.0 / dt);
        while self.timings.len() > 200 {
            self.timings.remove(0);
        }
    }
    fn render(&mut self, ctx: &Context) {
        // let mut graphics_window_width = 0.0;
        egui::Window::new("Graphics settings")
            .anchor(Align2::LEFT_TOP, [0.0, 0.0])
            // .default_height(0.2)
            .show(ctx, |ui| {
                self.fps_plot(ui);
                egui::CollapsingHeader::new("Graphics settings collapsible").show(ui, |ui| {
                    // ScrollArea::vertical().show(ui, |ui| {
                    // ui.vertical(|mut ui| {
                    // ui.horizontal(|ui| {
                    egui::Grid::new("Graphic sliders").show(ui, |ui| {
                        ui.label("LOD");
                        ui.add(egui::Slider::new(&mut self.lod_distance, 1.0..=20.0));
                        ui.end_row();

                        ui.label("AO resolution");
                        ui.add(egui::Slider::new(&mut self.ao_resolution, 0..=10));
                        ui.end_row();

                        ui.label("AO strength");
                        ui.add(egui::Slider::new(&mut self.ao_strength, 0.0..=1.0));
                        ui.end_row();
                    });
                    if ui
                        .add(egui::RadioButton::new(self.shadows, "shadows"))
                        .clicked()
                    {
                        self.shadows = !self.shadows;
                    }
                    // graphics_window_width = ui.max_rect().width();
                });
                // });
            });

        import::importer(self, ctx);

        egui::Window::new("log")
            .anchor(Align2::RIGHT_TOP, [0.0, 0.0])
            .resizable(true)
            // .resize(|r| r.fixed_size([1000.0; 2]))
            .show(ctx, |ui| {
                ScrollArea::both().stick_to_bottom(true).show(ui, |ui| {
                    for log in self.logs.iter() {
                        if log.level == Level::Debug || log.level == Level::Trace {
                            continue;
                        }
                        let color = match log.level {
                            Level::Info => Color32::from_rgb(0, 255, 255),
                            Level::Warn => Color32::from_rgb(255, 255, 0),
                            Level::Error => Color32::from_rgb(255, 10, 0),
                            _ => Color32::from_rgb(255, 255, 255),
                        };
                        ui.vertical(|ui| {
                            ui.label(
                                egui::RichText::new(format!(
                                    "[{:?}][{}] {}",
                                    log.level,
                                    log.target,
                                    log.message.clone()
                                ))
                                .color(color),
                            );
                            ui.separator();
                        });
                    }
                });
            });
    }
}
impl Gui {
    pub fn new(chunk_tx: Sender<Chunk>) -> Self {
        Self {
            picked_path: None,
            chunk_tx,
            logger: Logger::new().unwrap(),
            logs: Vec::new(),
            lod_distance: 10.0,
            shadows: true,
            ao_resolution: 3,
            ao_strength: 0.2,
            timings: Vec::new(),
            import: import::Import::default(),
        }
    }

    fn fps_plot(&self, ui: &mut Ui) {
        let points: PlotPoints = self
            .timings
            .iter()
            .enumerate()
            .map(|(x, y)| [x as f64 / 10.0, *y as f64])
            .collect();
        egui::CollapsingHeader::new("FPS header").show(ui, |ui| {
            Resize::default().show(ui, |ui| {
                ui.label(format!("fps: {:?}", self.timings.last()));
                Plot::new("FPS")
                    // .center_x_axis(true)
                    // .center_y_axis(true)
                    .allow_drag(false)
                    .allow_zoom(false)
                    .allow_scroll(false)
                    .show_axes([false, false])
                    .show(ui, |ui| ui.line(Line::new(points)));
            });
        });
    }
}
