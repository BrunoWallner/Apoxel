use common::{blocks::Block, chunk::Chunk, color::Color};
// use noise::{NoiseFn, Perlin};
use fastanvil::{complete, Chunk as ChunkTrait, Region};

// pub fn import(coord: [i32; 2]) -> Chunk {
//     let mut chunk = Chunk::default();

//     let size = 1024;
//     for x in 0..size {
//         for z in 0..size {
//             let color = Color {r: 100, g: 100, b: 100};
//             chunk.set_xyz(size, x, 2, z, Block::Color(color));
//         }
//     }

//     chunk
// }

// pub fn import(path: &str, offset: [i32; 2], scale: f32, y_range: [i32; 2], size: u32) -> Chunk {
//     log::info!("importing from: {}", path);
//     let mut chunk = Chunk::default();

//     let size = 8;
//     for x in 0..size {
//         for z in 0..size {
//             let y = (((x as f32 / size as f32 * 6.3).sin() + 1.0) * (size / 2) as f32) as u32;
//             // let y = 3;
//             chunk.set_xyz(
//                 size,
//                 x,
//                 y,
//                 z,
//                 Block::Color(Color {
//                     r: (x * (255 / size)) as u8,
//                     g: 50,
//                     b: (z * (255 / size)) as u8,
//                 }),
//             );
//         }
//     }

//     chunk
// }

pub fn import(path: &str, offset: [i32; 2], scale: f32, y_range: [i32; 2], size: u32) -> Chunk {
    log::info!("importing from: {}", path);
    let mut chunk = Chunk::default();
    let mut loaded_chunks = 0;
    let size = size as i32;
    let region_size = ((size as f32) * scale) as i32;
    for region_x in offset[0]..offset[0] + region_size {
        for region_z in offset[1]..offset[1] + region_size {
            let Ok(file) = std::fs::File::open(format!("{}/r.{}.{}.mca", path, region_x, region_z))
            else {
                log::error!("could not find region at: {} {}", region_x, region_z);
                continue;
            };
            log::info!("processing region: {} {}", region_x, region_z);
            let Ok(mut region) = Region::from_stream(file) else {
                continue;
            };
            let region_x = region_x - offset[0];
            let region_z = region_z - offset[1];
            for chunk_x in 0..32 {
                for chunk_z in 0..32 {
                    // println!("region: {:?}, chunk: {} {}", region_coord, chunk_x, chunk_z);
                    // if region_coord[0] <= 1 || region_coord[1] <= 1 {
                    //     continue;
                    // }
                    let Ok(Some(data)) = region.read_chunk(chunk_x, chunk_z) else {
                        continue;
                    };
                    loaded_chunks += 1;
                    let c = complete::Chunk::from_bytes(&data).unwrap();
                    for x in 0..(16.0 / scale) as u32 {
                        for z in 0..(16.0 / scale) as u32 {
                            // let h = c.surface_height(x, z, fastanvil::HeightMode::Trust);
                            for y in (y_range[0] as f32 / scale) as u32
                                ..(y_range[1] as f32 / scale) as u32
                            {
                                let x = (x as f32 * scale) as usize;
                                let y = (y as f32 * scale) as isize;
                                let z = (z as f32 * scale) as usize;
                                let Some(block) = c.block(x, y, z) else {
                                    continue;
                                };
                                // println!("{}", block.name());
                                if block.name() == "minecraft:air" {
                                    continue;
                                };
                                let Some(color) = block_id_to_color(block.name()) else {
                                    continue;
                                };
                                // let color = Color::splat(70);
                                let x = ((region_x as usize * 32 * 16 + chunk_x * 16 + x) as f32
                                    / scale) as i32
                                    / 2;
                                let z = ((region_z as usize * 32 * 16 + chunk_z * 16 + z) as f32
                                    / scale) as i32
                                    / 2;
                                let y = (y as f32 / scale) as u32;

                                let size = 256 * size;
                                if x > 0 && x <= size && z > 0 && z <= size {
                                    // println!("good: {} {} {}", x, y, z);

                                    chunk.set_xyz(
                                        size as u32,
                                        x as u32,
                                        y,
                                        z as u32,
                                        Block::Color(color),
                                    );
                                } else {
                                    // println!("bad: {} {} {}", x, y, z);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // if let Some(unknown) = MAP.get() {
    //     let u = unknown.lock().unwrap();
    //     let mut collected: Vec<(String, u32)> =
    //         u.iter().map(|(id, count)| (id.clone(), *count)).collect();
    //     collected.sort_by_key(|(_id, count)| *count as i32 * -1);
    //     let p: Vec<String> = collected.iter().map(|(id, _count)| id.clone()).collect();
    //     // println!("unknown: {:?}", collected);
    //     // println!("unknown: {:#?}", p);
    // }

    log::info!("loaded {} chunks", loaded_chunks);

    chunk
}

// fn get_region_coord(name: &str) -> Option<[i32; 2]> {
//     let split = name.split('.').collect::<Vec<_>>();
//     let x = split.get(1)?;
//     let y = split.get(2)?;
//     let x = x.parse::<i32>().ok()?;
//     let y = y.parse::<i32>().ok()?;
//     Some([x, y])
// }

// use std::collections::HashMap;
// use std::sync::{Arc, Mutex, OnceLock};

// static MAP: OnceLock<Arc<Mutex<HashMap<String, u32>>>> = OnceLock::new();

// chatGPT generated, should probaly be done with Hashmap
// but too lazy
#[rustfmt::skip]
fn block_id_to_color(block_id: &str) -> Option<Color> {
    match block_id {
        "minecraft:air" => Some(Color { r: 255, g: 255, b: 255 }), // White
        "minecraft:stone" => Some(Color { r: 125, g: 125, b: 125 }), // Gray
        "minecraft:grass_block" => Some(Color { r: 0, g: 128, b: 0 }), // Green
        "minecraft:dirt" => Some(Color { r: 139, g: 69, b: 19 }), // Brown
        "minecraft:water" => Some(Color { r: 50, g: 80, b: 255 }), // Blue
        "minecraft:lava" => Some(Color { r: 255, g: 140, b: 0 }), // Orange
        // Add more blocks with their corresponding colors
        "minecraft:stone_bricks" => Some(Color { r: 125, g: 125, b: 125 }), // Gray
        "minecraft:brick_block" => Some(Color { r: 155, g: 100, b: 75 }), // Brick red
        "minecraft:bookshelf" => Some(Color { r: 182, g: 155, b: 76 }), // Bookshelf brown
        "minecraft:planks" => Some(Color { r: 157, g: 128, b: 79 }), // Oak wood brown
        "minecraft:glass" => Some(Color { r: 191, g: 255, b: 255 }), // Light blue
        "minecraft:leaves" => Some(Color { r: 27, g: 126, b: 27 }), // Dark green
        "minecraft:torch" => Some(Color { r: 255, g: 232, b: 120 }), // Yellow
        "minecraft:flower_pot" => Some(Color { r: 150, g: 80, b: 50 }), // Flower pot brown
        "minecraft:bed" => Some(Color { r: 250, g: 150, b: 150 }), // Bed red
        "minecraft:chest" => Some(Color { r: 130, g: 90, b: 60 }), // Chest brown
        "minecraft:ender_chest" => Some(Color { r: 40, g: 20, b: 30 }), // Ender chest dark purple
        "minecraft:barrel" => Some(Color { r: 120, g: 90, b: 70 }), // Barrel brown
        "minecraft:grindstone" => Some(Color { r: 100, g: 100, b: 100 }), // Grindstone gray
        "minecraft:anvil" => Some(Color { r: 150, g: 150, b: 150 }), // Anvil gray
        "minecraft:bell" => Some(Color { r: 180, g: 180, b: 180 }), // Bell gray
        "minecraft:lectern" => Some(Color { r: 200, g: 150, b: 100 }), // Lectern brown
        "minecraft:soul_torch" => Some(Color { r: 80, g: 0, b: 120 }), // Soul torch purple
        "minecraft:chain" => Some(Color { r: 100, g: 100, b: 100 }), // Chain gray
        "minecraft:lantern" => Some(Color { r: 200, g: 200, b: 100 }), // Lantern yellow
        "minecraft:soul_lantern" => Some(Color { r: 80, g: 0, b: 120 }), // Soul lantern purple
        "minecraft:sweet_berry_bush" => Some(Color { r: 170, g: 100, b: 50 }), // Sweet berry bush brown
        "minecraft:campfire" => Some(Color { r: 150, g: 70, b: 20 }), // Campfire brown
        "minecraft:smithing_table" => Some(Color { r: 100, g: 70, b: 50 }), // Smithing table brown
        "minecraft:furnace" => Some(Color { r: 100, g: 100, b: 100 }), // Furnace gray
        "minecraft:blast_furnace" => Some(Color { r: 100, g: 100, b: 100 }), // Blast furnace gray
        "minecraft:smoker" => Some(Color { r: 100, g: 100, b: 100 }), // Smoker gray
        "minecraft:brewing_stand" => Some(Color { r: 100, g: 100, b: 100 }), // Brewing stand gray
        "minecraft:cauldron" => Some(Color { r: 80, g: 80, b: 80 }), // Cauldron dark gray
        "minecraft:composter" => Some(Color { r: 50, g: 80, b: 30 }), // Composter green
        "minecraft:cartography_table" => Some(Color { r: 150, g: 120, b: 100 }), // Cartography table brown
        "minecraft:loom" => Some(Color { r: 150, g: 120, b: 100 }), // Loom brown
        "minecraft:stonecutter" => Some(Color { r: 100, g: 100, b: 100 }), // Stonecutter gray
        "minecraft:jukebox" => Some(Color { r: 140, g: 110, b: 90 }), // Jukebox brown
        "minecraft:note_block" => Some(Color { r: 140, g: 110, b: 90 }), // Note block brown
        "minecraft:scaffolding" => Some(Color { r: 180, g: 160, b: 140 }), // Scaffolding brown
        "minecraft:bamboo" => Some(Color { r: 80, g: 150, b: 50 }), // Bamboo green
        "minecraft:bamboo_sapling" => Some(Color { r: 80, g: 150, b: 50 }), // Bamboo sapling green

        "minecraft:andesite" => Some(Color { r: 131, g: 131, b: 131 }), // Andesite gray
        "minecraft:diorite" => Some(Color { r: 222, g: 222, b: 222 }), // Diorite white
        "minecraft:granite" => Some(Color { r: 156, g: 95, b: 82 }), // Granite pink-brown
        "minecraft:gravel" => Some(Color { r: 134, g: 126, b: 116 }), // Gravel gray-brown
        "minecraft:coal_ore" => Some(Color { r: 51, g: 51, b: 51 }), // Coal ore black
        "minecraft:sand" => Some(Color { r: 219, g: 212, b: 158 }), // Sand yellow
        "minecraft:copper_ore" => Some(Color { r: 158, g: 90, b: 61 }), // Copper ore orange-brown
        "minecraft:oak_leaves" => Some(Color { r: 58, g: 94, b: 36 }), // Oak leaves dark green
        "minecraft:spruce_leaves" => Some(Color { r: 82, g: 106, b: 50 }), // Spruce leaves green
        "minecraft:sandstone" => Some(Color { r: 218, g: 210, b: 158 }), // Sandstone yellow
        "minecraft:terracotta" => Some(Color { r: 150, g: 94, b: 68 }), // Terracotta brown
        "minecraft:brown_terracotta" => Some(Color { r: 104, g: 67, b: 50 }), // Brown terracotta brown
        "minecraft:kelp_plant" => Some(Color { r: 29, g: 126, b: 29 }), // Dark green
        "minecraft:orange_terracotta" => Some(Color { r: 191, g: 102, b: 31 }), // Orange
        "minecraft:iron_ore" => Some(Color { r: 136, g: 130, b: 127 }), // Iron ore gray
        "minecraft:birch_leaves" => Some(Color { r: 154, g: 199, b: 49 }), // Birch leaves bright green
        "minecraft:clay" => Some(Color { r: 173, g: 164, b: 152 }), // Clay gray
        "minecraft:dripstone_block" => Some(Color { r: 140, g: 109, b: 81 }), // Dripstone block brown
        "minecraft:snow_block" => Some(Color { r: 255, g: 255, b: 255 }), // Snow white
        "minecraft:red_terracotta" => Some(Color { r: 153, g: 87, b: 83 }), // Red
        "minecraft:vine" => Some(Color { r: 69, g: 125, b: 30 }), // Vine green
        "minecraft:grass" => Some(Color { r: 109, g: 137, b: 49 }), // Grass green
        "minecraft:podzol" => Some(Color { r: 108, g: 84, b: 50 }), // Podzol brown
        "minecraft:jungle_leaves" => Some(Color { r: 84, g: 109, b: 32 }), // Jungle leaves dark green
        "minecraft:white_terracotta" => Some(Color { r: 210, g: 178, b: 161 }), // White terracotta light gray
        "minecraft:spruce_log" => Some(Color { r: 95, g: 76, b: 49 }), // Spruce brown
        "minecraft:tall_seagrass" => Some(Color { r: 86, g: 138, b: 29 }), // Seagrass green
        "minecraft:light_gray_terracotta" => Some(Color { r: 135, g: 107, b: 98 }), // Light gray terracotta light brown
        "minecraft:cave_air" => None, // Cave air transparent
        "minecraft:seagrass" => Some(Color { r: 50, g: 93, b: 24 }), // Seagrass green

        "minecraft:snow" => Some(Color { r: 255, g: 255, b: 255 }), // Snow white
        "minecraft:packed_ice" => Some(Color { r: 174, g: 255, b: 255 }), // Packed ice light blue
        "minecraft:moss_block" => Some(Color { r: 122, g: 166, b: 125 }), // Moss block green
        "minecraft:oak_log" => Some(Color { r: 92, g: 60, b: 30 }), // Oak brown
        "minecraft:dark_oak_leaves" => Some(Color { r: 64, g: 81, b: 34 }), // Dark oak leaves dark green
        "minecraft:coarse_dirt" => Some(Color { r: 134, g: 96, b: 67 }), // Coarse dirt brown
        "minecraft:lapis_ore" => Some(Color { r: 57, g: 78, b: 98 }), // Lapis ore blue
        "minecraft:pointed_dripstone" => Some(Color { r: 140, g: 109, b: 81 }), // Pointed dripstone brown
        "minecraft:kelp" => Some(Color { r: 39, g: 126, b: 27 }), // Dark green
        "minecraft:jungle_log" => Some(Color { r: 95, g: 76, b: 49 }), // Jungle brown
        "minecraft:red_sand" => Some(Color { r: 171, g: 91, b: 52 }), // Red sand red-brown
        "minecraft:fern" => Some(Color { r: 102, g: 126, b: 40 }), // Fern dark green
        "minecraft:birch_log" => Some(Color { r: 197, g: 195, b: 173 }), // Birch light brown
        "minecraft:dark_oak_log" => Some(Color { r: 60, g: 39, b: 19 }), // Dark oak brown
        "minecraft:mossy_cobblestone" => Some(Color { r: 90, g: 108, b: 76 }), // Mossy cobblestone green-brown
        "minecraft:acacia_leaves" => Some(Color { r: 66, g: 86, b: 34 }), // Acacia leaves dark green
        "minecraft:deepslate_bricks" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate gray
        "minecraft:tall_grass" => Some(Color { r: 123, g: 152, b: 49 }), // Tall grass green
        "minecraft:powder_snow" => Some(Color { r: 255, g: 255, b: 255 }), // Powder snow white
        "minecraft:smooth_basalt" => Some(Color { r: 72, g: 72, b: 72 }), // Basalt gray
        "minecraft:gold_ore" => Some(Color { r: 215, g: 174, b: 21 }), // Gold ore yellow
        "minecraft:bubble_column" => Some(Color { r: 26, g: 64, b: 150 }), // Water blue
        "minecraft:calcite" => Some(Color { r: 232, g: 232, b: 232 }), // Calcite white
        "minecraft:cobblestone" => Some(Color { r: 124, g: 124, b: 124 }), // Cobblestone gray
        "minecraft:glow_lichen" => Some(Color { r: 36, g: 90, b: 36 }), // Glow lichen green
        "minecraft:rooted_dirt" => Some(Color { r: 108, g: 84, b: 50 }), // Rooted dirt brown
        "minecraft:polished_blackstone_bricks" => Some(Color { r: 40, g: 40, b: 40 }), // Polished blackstone bricks dark gray
        "minecraft:infested_stone" => Some(Color { r: 98, g: 67, b: 58 }), // Infested stone red-brown
        "minecraft:large_fern" => Some(Color { r: 102, g: 126, b: 40 }), // Large fern dark green
        "minecraft:deepslate_tiles" => Some(Color { r: 80, g: 80, b: 80 }), // Deepslate gray

        "minecraft:oak_planks" => Some(Color { r: 157, g: 126, b: 78 }), // Oak planks light brown
        "minecraft:spruce_planks" => Some(Color { r: 105, g: 82, b: 50 }), // Spruce planks brown
        "minecraft:amethyst_block" => Some(Color { r: 130, g: 108, b: 173 }), // Amethyst block purple
        "minecraft:polished_deepslate" => Some(Color { r: 71, g: 71, b: 71 }), // Polished deepslate gray
        "minecraft:cyan_terracotta" => Some(Color { r: 86, g: 91, b: 91 }), // Cyan terracotta cyan
        "minecraft:dirt_path" => Some(Color { r: 135, g: 98, b: 67 }), // Dirt path brown
        "minecraft:green_concrete_powder" => Some(Color { r: 73, g: 91, b: 36 }), // Green concrete powder dark green
        "minecraft:green_terracotta" => Some(Color { r: 76, g: 83, b: 42 }), // Green terracotta dark green
        "minecraft:stripped_warped_stem" => Some(Color { r: 66, g: 84, b: 62 }), // Stripped warped stem dark green
        "minecraft:cave_vines_plant" => Some(Color { r: 51, g: 91, b: 35 }), // Dark green
        "minecraft:acacia_planks" => Some(Color { r: 161, g: 79, b: 28 }), // Acacia planks orange-brown
        "minecraft:redstone_wire" => Some(Color { r: 255, g: 0, b: 0 }), // Redstone wire red
        "minecraft:magma_block" => Some(Color { r: 104, g: 18, b: 18 }), // Magma block red-brown
        "minecraft:cobbled_deepslate" => Some(Color { r: 56, g: 56, b: 56 }), // Cobbled deepslate dark gray
        "minecraft:deepslate" => Some(Color { r: 80, g: 80, b: 80 }), // Deepslate gray
        "minecraft:yellow_terracotta" => Some(Color { r: 186, g: 133, b: 36 }), // Yellow terracotta yellow-brown
        "minecraft:ice" => Some(Color { r: 160, g: 193, b: 255 }), // Ice light blue
        "minecraft:oak_fence" => Some(Color { r: 157, g: 126, b: 78 }), // Oak fence light brown
        "minecraft:spruce_slab" => Some(Color { r: 105, g: 82, b: 50 }), // Spruce slab brown
        "minecraft:dead_bush" => Some(Color { r: 113, g: 107, b: 60 }), // Dead bush brown
        "minecraft:green_concrete" => Some(Color { r: 73, g: 91, b: 36 }), // Green concrete dark green
        "minecraft:deepslate_brick_slab" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate brick slab gray
        "minecraft:emerald_ore" => Some(Color { r: 52, g: 97, b: 80 }), // Emerald ore green-gray
        "minecraft:polished_basalt" => Some(Color { r: 74, g: 74, b: 74 }), // Polished basalt dark gray
        "minecraft:white_concrete" => Some(Color { r: 210, g: 210, b: 210 }), // White concrete light gray
        "minecraft:polished_andesite" => Some(Color { r: 150, g: 150, b: 150 }), // Polished andesite light gray
        "minecraft:brown_mushroom" => Some(Color { r: 151, g: 109, b: 89 }), // Brown mushroom brown
        "minecraft:black_concrete" => Some(Color { r: 20, g: 20, b: 20 }), // Black concrete black
        "minecraft:rail" => Some(Color { r: 125, g: 125, b: 125 }), // Rail gray
        "minecraft:deepslate_brick_stairs" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate brick stairs gray

        "minecraft:polished_deepslate_slab" => Some(Color { r: 71, g: 71, b: 71 }), // Polished deepslate slab gray
        "minecraft:azalea_leaves" => Some(Color { r: 76, g: 112, b: 43 }), // Azalea leaves dark green
        "minecraft:spruce_trapdoor" => Some(Color { r: 105, g: 82, b: 50 }), // Spruce trapdoor brown
        "minecraft:powered_rail" => Some(Color { r: 125, g: 125, b: 125 }), // Powered rail gray
        "minecraft:tripwire" => Some(Color { r: 92, g: 73, b: 56 }), // Tripwire brown
        "minecraft:smooth_sandstone" => Some(Color { r: 216, g: 207, b: 163 }), // Smooth sandstone beige
        "minecraft:polished_blackstone_brick_stairs" => Some(Color { r: 39, g: 39, b: 39 }), // Polished blackstone brick stairs dark gray
        "minecraft:stripped_spruce_log" => Some(Color { r: 105, g: 82, b: 50 }), // Stripped spruce log brown
        "minecraft:iron_bars" => Some(Color { r: 123, g: 123, b: 123 }), // Iron bars gray
        "minecraft:netherrack" => Some(Color { r: 116, g: 47, b: 47 }), // Netherrack red-brown
        "minecraft:moss_carpet" => Some(Color { r: 122, g: 166, b: 125 }), // Moss carpet green
        "minecraft:dandelion" => Some(Color { r: 255, g: 255, b: 85 }), // Dandelion yellow
        "minecraft:spruce_stairs" => Some(Color { r: 105, g: 82, b: 50 }), // Spruce stairs brown
        "minecraft:pink_stained_glass" => Some(Color { r: 255, g: 181, b: 181 }), // Pink stained glass light pink
        "minecraft:quartz_block" => Some(Color { r: 235, g: 234, b: 230 }), // Quartz block white
        "minecraft:polished_blackstone_slab" => Some(Color { r: 39, g: 39, b: 39 }), // Polished blackstone slab dark gray
        "minecraft:sculk" => Some(Color { r: 109, g: 99, b: 104 }), // Sculk purple-gray
        "minecraft:light_gray_stained_glass" => Some(Color { r: 192, g: 192, b: 192 }), // Light gray stained glass light gray
        "minecraft:acacia_log" => Some(Color { r: 161, g: 79, b: 28 }), // Acacia log orange-brown
        "minecraft:prismarine_bricks" => Some(Color { r: 104, g: 122, b: 123 }), // Prismarine bricks blue-gray
        "minecraft:red_concrete" => Some(Color { r: 142, g: 32, b: 32 }), // Red concrete red
        "minecraft:cracked_stone_bricks" => Some(Color { r: 124, g: 124, b: 124 }), // Cracked stone bricks gray
        "minecraft:deepslate_tile_slab" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate tile slab gray
        "minecraft:blue_terracotta" => Some(Color { r: 73, g: 73, b: 122 }), // Blue terracotta blue
        "minecraft:tinted_glass" => Some(Color { r: 50, g: 50, b: 50 }), // Tinted glass dark gray
        "minecraft:dark_oak_planks" => Some(Color { r: 62, g: 40, b: 17 }), // Dark oak planks dark brown
        "minecraft:gray_concrete" => Some(Color { r: 84, g: 84, b: 84 }), // Gray concrete gray
        "minecraft:hopper" => Some(Color { r: 111, g: 111, b: 111 }), // Hopper gray
        "minecraft:tuff" => Some(Color { r: 109, g: 111, b: 111 }), // Tuff gray
        "minecraft:red_mushroom_block" => Some(Color { r: 181, g: 41, b: 35 }), // Red mushroom block red

        "minecraft:cave_vines" => Some(Color { r: 95, g: 158, b: 67 }), // Cave vines bright green
        "minecraft:polished_blackstone_wall" => Some(Color { r: 43, g: 43, b: 43 }), // Polished blackstone wall dark gray
        "minecraft:spruce_fence" => Some(Color { r: 105, g: 82, b: 50 }), // Spruce fence brown
        "minecraft:dark_prismarine" => Some(Color { r: 24, g: 53, b: 58 }), // Dark prismarine dark blue-green
        "minecraft:cyan_concrete" => Some(Color { r: 21, g: 119, b: 136 }), // Cyan concrete cyan
        "minecraft:mud_bricks" => Some(Color { r: 92, g: 74, b: 63 }), // Mud bricks brown
        "minecraft:light_gray_wool" => Some(Color { r: 170, g: 170, b: 170 }), // Light gray wool light gray
        "minecraft:iron_block" => Some(Color { r: 229, g: 229, b: 229 }), // Iron block light gray
        "minecraft:birch_fence" => Some(Color { r: 198, g: 180, b: 135 }), // Birch fence light brown
        "minecraft:orange_concrete" => Some(Color { r: 223, g: 109, b: 16 }), // Orange concrete orange
        "minecraft:prismarine" => Some(Color { r: 72, g: 154, b: 138 }), // Prismarine turquoise
        "minecraft:obsidian" => Some(Color { r: 15, g: 0, b: 48 }), // Obsidian dark purple
        "minecraft:light_blue_wool" => Some(Color { r: 58, g: 100, b: 185 }), // Light blue wool light blue
        "minecraft:spruce_wood" => Some(Color { r: 105, g: 82, b: 50 }), // Spruce wood brown
        "minecraft:oak_stairs" => Some(Color { r: 106, g: 84, b: 55 }), // Oak stairs brown
        "minecraft:ladder" => Some(Color { r: 150, g: 123, b: 83 }), // Ladder brown
        "minecraft:wall_torch" => Some(Color { r: 170, g: 166, b: 114 }), // Wall torch yellow
        "minecraft:smooth_quartz" => Some(Color { r: 219, g: 219, b: 202 }), // Smooth quartz light gray
        "minecraft:black_wool" => Some(Color { r: 20, g: 20, b: 20 }), // Black wool black
        "minecraft:spruce_wall_sign" => Some(Color { r: 105, g: 82, b: 50 }), // Spruce wall sign brown
        "minecraft:packed_mud" => Some(Color { r: 102, g: 83, b: 70 }), // Packed mud brown
        "minecraft:birch_planks" => Some(Color { r: 200, g: 182, b: 140 }), // Birch planks light brown
        "minecraft:polished_deepslate_wall" => Some(Color { r: 71, g: 71, b: 71 }), // Polished deepslate wall gray
        "minecraft:light_gray_concrete" => Some(Color { r: 145, g: 145, b: 145 }), // Light gray concrete light gray
        "minecraft:mangrove_leaves" => Some(Color { r: 78, g: 112, b: 29 }), // Mangrove leaves dark green
        "minecraft:orange_wool" => Some(Color { r: 214, g: 93, b: 14 }), // Orange wool orange
        "minecraft:polished_deepslate_stairs" => Some(Color { r: 71, g: 71, b: 71 }), // Polished deepslate stairs gray
        "minecraft:stone_brick_stairs" => Some(Color { r: 122, g: 122, b: 122 }), // Stone brick stairs light gray
        "minecraft:cobweb" => Some(Color { r: 252, g: 252, b: 252 }), // Cobweb white
        "minecraft:cobbled_deepslate_slab" => Some(Color { r: 54, g: 54, b: 54 }), // Cobbled deepslate slab gray
        "minecraft:polished_blackstone_button" => Some(Color { r: 34, g: 34, b: 34 }), // Polished blackstone button black
        "minecraft:pink_wool" => Some(Color { r: 233, g: 139, b: 157 }), // Pink wool pink
        "minecraft:yellow_concrete" => Some(Color { r: 189, g: 153, b: 12 }), // Yellow concrete yellow
        "minecraft:smooth_red_sandstone_slab" => Some(Color { r: 178, g: 85, b: 37 }), // Smooth red sandstone slab orange-brown
        "minecraft:flowering_azalea_leaves" => Some(Color { r: 76, g: 112, b: 43 }), // Flowering azalea leaves dark green
        "minecraft:warped_wart_block" => Some(Color { r: 49, g: 75, b: 77 }), // Warped wart block turquoise
        "minecraft:poppy" => Some(Color { r: 199, g: 8, b: 8 }), // Poppy red
        "minecraft:light_blue_terracotta" => Some(Color { r: 112, g: 112, b: 180 }), // Light blue terracotta light blue
        "minecraft:brown_mushroom_block" => Some(Color { r: 134, g: 96, b: 67 }), // Brown mushroom block brown
        "minecraft:quartz_pillar" => Some(Color { r: 242, g: 238, b: 229 }), // Quartz pillar white

        "minecraft:cracked_deepslate_bricks" => Some(Color { r: 54, g: 54, b: 54 }), // Cracked deepslate bricks gray
        "minecraft:gray_stained_glass" => Some(Color { r: 51, g: 51, b: 51 }), // Gray stained glass dark gray
        "minecraft:allium" => Some(Color { r: 134, g: 27, b: 160 }), // Allium purple
        "minecraft:sugar_cane" => Some(Color { r: 46, g: 128, b: 0 }), // Sugar cane dark green
        "minecraft:sunflower" => Some(Color { r: 254, g: 219, b: 58 }), // Sunflower yellow
        "minecraft:stone_stairs" => Some(Color { r: 125, g: 125, b: 125 }), // Stone stairs light gray
        "minecraft:comparator" => Some(Color { r: 131, g: 130, b: 122 }), // Comparator gray
        "minecraft:nether_wart_block" => Some(Color { r: 144, g: 39, b: 32 }), // Nether wart block red
        "minecraft:cocoa" => Some(Color { r: 85, g: 64, b: 45 }), // Cocoa brown
        "minecraft:white_wool" => Some(Color { r: 221, g: 221, b: 221 }), // White wool white
        "minecraft:stone_brick_wall" => Some(Color { r: 122, g: 122, b: 122 }), // Stone brick wall light gray
        "minecraft:gray_stained_glass_pane" => Some(Color { r: 51, g: 51, b: 51 }), // Gray stained glass pane dark gray
        "minecraft:observer" => Some(Color { r: 124, g: 125, b: 126 }), // Observer light gray
        "minecraft:oxidized_cut_copper" => Some(Color { r: 90, g: 90, b: 90 }), // Oxidized cut copper gray
        "minecraft:stripped_mangrove_wood" => Some(Color { r: 105, g: 82, b: 50 }), // Stripped mangrove wood brown
        "minecraft:azure_bluet" => Some(Color { r: 82, g: 139, b: 165 }), // Azure bluet light blue
        "minecraft:sea_lantern" => Some(Color { r: 100, g: 215, b: 152 }), // Sea lantern light blue-green
        "minecraft:honeycomb_block" => Some(Color { r: 192, g: 152, b: 25 }), // Honeycomb block yellow
        "minecraft:stone_slab" => Some(Color { r: 125, g: 125, b: 125 }), // Stone slab light gray
        "minecraft:dark_prismarine_stairs" => Some(Color { r: 24, g: 53, b: 58 }), // Dark prismarine stairs dark blue-green
        "minecraft:andesite_wall" => Some(Color { r: 128, g: 128, b: 128 }), // Andesite wall gray
        "minecraft:deepslate_brick_wall" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate brick wall gray
        "minecraft:blackstone" => Some(Color { r: 24, g: 24, b: 24 }), // Blackstone black
        "minecraft:tnt" => Some(Color { r: 224, g: 53, b: 41 }), // TNT red
        "minecraft:oxidized_copper" => Some(Color { r: 102, g: 136, b: 136 }), // Oxidized copper turquoise
        "minecraft:chiseled_polished_blackstone" => Some(Color { r: 43, g: 43, b: 43 }), // Chiseled polished blackstone dark gray
        "minecraft:soul_sand" => Some(Color { r: 82, g: 66, b: 55 }), // Soul sand brown
        "minecraft:mangrove_wood" => Some(Color { r: 105, g: 82, b: 50 }), // Mangrove wood brown
        "minecraft:nether_brick_fence" => Some(Color { r: 42, g: 25, b: 25 }), // Nether brick fence dark red
        "minecraft:dark_oak_slab" => Some(Color { r: 61, g: 47, b: 32 }), // Dark oak

        "minecraft:basalt" => Some(Color { r: 82, g: 82, b: 82 }), // Basalt dark gray
        "minecraft:smooth_stone" => Some(Color { r: 128, g: 128, b: 128 }), // Smooth stone light gray
        "minecraft:yellow_wool" => Some(Color { r: 245, g: 215, b: 66 }), // Yellow wool yellow
        "minecraft:oxeye_daisy" => Some(Color { r: 244, g: 236, b: 66 }), // Oxeye daisy yellow
        "minecraft:cracked_polished_blackstone_bricks" => Some(Color { r: 43, g: 43, b: 43 }), // Cracked polished blackstone bricks dark gray
        "minecraft:cherry_leaves" => Some(Color { r: 52, g: 128, b: 29 }), // Cherry leaves dark green
        "minecraft:azalea" => Some(Color { r: 52, g: 128, b: 29 }), // Azalea dark green
        "minecraft:smooth_quartz_slab" => Some(Color { r: 237, g: 235, b: 228 }), // Smooth quartz slab white
        "minecraft:dark_oak_stairs" => Some(Color { r: 61, g: 47, b: 32 }), // Dark oak stairs dark brown
        "minecraft:cyan_wool" => Some(Color { r: 21, g: 119, b: 136 }), // Cyan wool cyan
        "minecraft:polished_diorite" => Some(Color { r: 195, g: 195, b: 195 }), // Polished diorite light gray
        "minecraft:black_stained_glass_pane" => Some(Color { r: 25, g: 25, b: 25 }), // Black stained glass pane black
        "minecraft:dispenser" => Some(Color { r: 107, g: 107, b: 107 }), // Dispenser gray
        "minecraft:stripped_spruce_wood" => Some(Color { r: 162, g: 123, b: 64 }), // Stripped spruce wood brown
        "minecraft:peony" => Some(Color { r: 185, g: 148, b: 207 }), // Peony light purple
        "minecraft:flowering_azalea" => Some(Color { r: 52, g: 128, b: 29 }), // Flowering azalea dark green
        "minecraft:stripped_oak_log" => Some(Color { r: 162, g: 123, b: 64 }), // Stripped oak log brown
        "minecraft:polished_blackstone" => Some(Color { r: 43, g: 43, b: 43 }), // Polished blackstone dark gray
        "minecraft:budding_amethyst" => Some(Color { r: 164, g: 132, b: 255 }), // Budding amethyst light purple
        "minecraft:big_dripleaf" => Some(Color { r: 0, g: 117, b: 18 }), // Big dripleaf dark green
        "minecraft:cobblestone_wall" => Some(Color { r: 125, g: 125, b: 125 }), // Cobblestone wall light gray
        "minecraft:prismarine_brick_slab" => Some(Color { r: 77, g: 137, b: 123 }), // Prismarine brick slab cyan
        "minecraft:light_gray_concrete_powder" => Some(Color { r: 144, g: 144, b: 144 }), // Light gray concrete powder light gray
        "minecraft:glowstone" => Some(Color { r: 204, g: 200, b: 101 }), // Glowstone light yellow
        "minecraft:iron_trapdoor" => Some(Color { r: 213, g: 213, b: 213 }), // Iron trapdoor light gray
        "minecraft:polished_andesite_stairs" => Some(Color { r: 140, g: 140, b: 140 }), // Polished andesite stairs light gray
        "minecraft:sandstone_wall" => Some(Color { r: 218, g: 210, b: 158 }), // Sandstone wall light brown
        "minecraft:ochre_froglight" => Some(Color { r: 249, g: 189, b: 100 }), // Ochre froglight yellow-orange
        "minecraft:cobbled_deepslate_wall" => Some(Color { r: 54, g: 54, b: 54 }),

        "minecraft:polished_granite" => Some(Color { r: 149, g: 105, b: 89 }), // Polished granite light brown
        "minecraft:blue_concrete" => Some(Color { r: 45, g: 47, b: 143 }), // Blue concrete blue
        "minecraft:polished_blackstone_brick_slab" => Some(Color { r: 43, g: 43, b: 43 }), // Polished blackstone brick slab dark gray
        "minecraft:stripped_birch_wood" => Some(Color { r: 212, g: 195, b: 150 }), // Stripped birch wood light brown
        "minecraft:mud_brick_slab" => Some(Color { r: 76, g: 55, b: 33 }), // Mud brick slab dark brown
        "minecraft:glass_pane" => Some(Color { r: 204, g: 204, b: 204 }), // Glass pane light gray
        "minecraft:mud" => Some(Color { r: 76, g: 55, b: 33 }), // Mud dark brown
        "minecraft:gray_concrete_powder" => Some(Color { r: 81, g: 83, b: 85 }), // Gray concrete powder gray
        "minecraft:lilac" => Some(Color { r: 204, g: 160, b: 195 }), // Lilac light purple
        "minecraft:dark_oak_fence" => Some(Color { r: 61, g: 47, b: 32 }), // Dark oak fence dark brown
        "minecraft:medium_amethyst_bud" => Some(Color { r: 164, g: 132, b: 255 }), // Medium amethyst bud light purple
        "minecraft:black_terracotta" => Some(Color { r: 37, g: 23, b: 16 }), // Black terracotta black
        "minecraft:deepslate_tile_stairs" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate tile stairs gray
        "minecraft:acacia_slab" => Some(Color { r: 162, g: 123, b: 64 }), // Acacia slab brown
        "minecraft:purple_wool" => Some(Color { r: 105, g: 51, b: 160 }), // Purple wool purple
        "minecraft:chiseled_stone_bricks" => Some(Color { r: 123, g: 123, b: 123 }), // Chiseled stone bricks light gray
        "minecraft:repeater" => Some(Color { r: 128, g: 128, b: 128 }), // Repeater light gray
        "minecraft:spruce_fence_gate" => Some(Color { r: 111, g: 83, b: 43 }), // Spruce fence gate brown
        "minecraft:red_wool" => Some(Color { r: 161, g: 39, b: 35 }), // Red wool red
        "minecraft:oak_slab" => Some(Color { r: 162, g: 123, b: 64 }), // Oak slab brown
        "minecraft:smooth_stone_slab" => Some(Color { r: 128, g: 128, b: 128 }), // Smooth stone slab light gray
        "minecraft:cobblestone_slab" => Some(Color { r: 125, g: 125, b: 125 }), // Cobblestone slab light gray
        "minecraft:waxed_copper_block" => Some(Color { r: 117, g: 101, b: 77 }), // Waxed copper block brown
        "minecraft:oak_trapdoor" => Some(Color { r: 162, g: 123, b: 64 }), // Oak trapdoor brown
        "minecraft:oxidized_cut_copper_stairs" => Some(Color { r: 102, g: 136, b: 136 }), // Oxidized cut copper stairs turquoise
        "minecraft:wheat" => Some(Color { r: 227, g: 201, b: 105 }), // Wheat yellow
        "minecraft:blue_ice" => Some(Color { r: 102, g: 136, b: 136 }), // Blue ice turquoise
        "minecraft:verdant_froglight" => Some(Color { r: 53, g: 130, b: 55 }), // Verdant froglight dark green
        "minecraft:cobblestone_stairs" => Some(Color { r: 125, g: 125, b: 125 }), // Cobblestone stairs light gray
        "minecraft:black_stained_glass" => Some(Color { r: 25, g: 25, b: 25 }), // Black stained glass black
        "minecraft:jungle_slab" => Some(Color { r: 126, g: 94, b: 53 }), // Jungle slab brown
        "minecraft:deepslate_redstone_ore" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate redstone ore gray
        "minecraft:quartz_slab" => Some(Color { r: 232, g: 232, b: 232 }), // Quartz slab white
        "minecraft:purple_stained_glass" => Some(Color { r: 102, g: 26, b: 153 }), // Purple stained glass purple
        "minecraft:shulker_box" => Some(Color { r: 156, g: 127, b: 183 }), // Shulker box light purple
        "minecraft:carrots" => Some(Color { r: 255, g: 133, b: 46 }), // Carrots orange
        "minecraft:polished_blackstone_brick_wall" => Some(Color { r: 43, g: 43, b: 43 }), // Polished blackstone brick wall dark gray
        "minecraft:warped_stem" => Some(Color { r: 85, g: 141, b: 118 }), // Warped stem cyan
        "minecraft:dead_brain_coral_block" => Some(Color { r: 177, g: 129, b: 134 }), // Dead brain coral block pink
        "minecraft:white_stained_glass" => Some(Color { r: 221, g: 221, b: 221 }), // White stained glass white

        "minecraft:piston" => Some(Color { r: 130, g: 98, b: 52 }), // Piston brown
        "minecraft:end_stone" => Some(Color { r: 221, g: 221, b: 165 }), // End stone light yellow
        "minecraft:cut_sandstone" => Some(Color { r: 216, g: 205, b: 158 }), // Cut sandstone light brown
        "minecraft:white_concrete_powder" => Some(Color { r: 234, g: 236, b: 237 }), // White concrete powder white
        "minecraft:shroomlight" => Some(Color { r: 226, g: 188, b: 123 }), // Shroomlight light brown
        "minecraft:stone_brick_slab" => Some(Color { r: 123, g: 123, b: 123 }), // Stone brick slab light gray
        "minecraft:warped_hyphae" => Some(Color { r: 85, g: 141, b: 118 }), // Warped hyphae cyan
        "minecraft:acacia_trapdoor" => Some(Color { r: 162, g: 123, b: 64 }), // Acacia trapdoor brown
        "minecraft:gray_carpet" => Some(Color { r: 64, g: 64, b: 64 }), // Gray carpet dark gray
        "minecraft:smooth_red_sandstone" => Some(Color { r: 169, g: 88, b: 64 }), // Smooth red sandstone orange
        "minecraft:green_wool" => Some(Color { r: 76, g: 83, b: 42 }), // Green wool green
        "minecraft:blue_stained_glass" => Some(Color { r: 51, g: 76, b: 178 }), // Blue stained glass blue
        "minecraft:magenta_stained_glass" => Some(Color { r: 170, g: 43, b: 164 }), // Magenta stained glass magenta
        "minecraft:purple_glazed_terracotta" => Some(Color { r: 80, g: 30, b: 79 }), // Purple glazed terracotta dark purple
        "minecraft:sticky_piston" => Some(Color { r: 130, g: 98, b: 52 }), // Sticky piston brown
        "minecraft:oak_door" => Some(Color { r: 162, g: 123, b: 64 }), // Oak door brown
        "minecraft:lime_stained_glass" => Some(Color { r: 50, g: 205, b: 50 }), // Lime stained glass lime
        "minecraft:cyan_stained_glass" => Some(Color { r: 21, g: 174, b: 170 }), // Cyan stained glass cyan
        "minecraft:white_tulip" => Some(Color { r: 219, g: 219, b: 219 }), // White tulip white
        "minecraft:quartz_stairs" => Some(Color { r: 232, g: 232, b: 232 }), // Quartz stairs white
        "minecraft:red_stained_glass" => Some(Color { r: 153, g: 51, b: 51 }), // Red stained glass red
        "minecraft:dropper" => Some(Color { r: 128, g: 128, b: 128 }), // Dropper light gray
        "minecraft:dark_oak_trapdoor" => Some(Color { r: 61, g: 47, b: 32 }), // Dark oak trapdoor dark brown
        "minecraft:prismarine_slab" => Some(Color { r: 95, g: 136, b: 120 }), // Prismarine slab green
        "minecraft:crimson_planks" => Some(Color { r: 153, g: 40, b: 36 }), // Crimson planks red
        "minecraft:stripped_dark_oak_log" => Some(Color { r: 56, g: 44, b: 29 }), // Stripped dark oak log dark brown
        "minecraft:soul_soil" => Some(Color { r: 76, g: 64, b: 46 }), // Soul soil brown
        "minecraft:chiseled_deepslate" => Some(Color { r: 54, g: 54, b: 54 }), // Chiseled deepslate gray
        "minecraft:mangrove_log" => Some(Color { r: 97, g: 76, b: 48 }), // Mangrove log brown
        "minecraft:andesite_slab" => Some(Color { r: 131, g: 131, b: 131 }), // Andesite slab light gray
        "minecraft:mud_brick_stairs" => Some(Color { r: 76, g: 55, b: 33 }), // Mud brick stairs dark brown
        "minecraft:cyan_stained_glass_pane" => Some(Color { r: 21, g: 174, b: 170 }), // Cyan stained glass pane cyan
        "minecraft:red_sandstone" => Some(Color { r: 186, g: 99, b: 44 }), // Red sandstone orange
        "minecraft:nether_portal" => Some(Color { r: 9, g: 8, b: 11 }), // Nether portal black
        "minecraft:crying_obsidian" => Some(Color { r: 91, g: 20, b: 161 }), // Crying obsidian purple
        "minecraft:light_gray_stained_glass_pane" => Some(Color { r: 184, g: 184, b: 184 }), // Light gray stained glass pane light gray
        "minecraft:mossy_cobblestone_slab" => Some(Color { r: 105, g: 118, b: 89 }), // Mossy cobblestone slab grayish green
        "minecraft:blackstone_slab" => Some(Color { r: 43, g: 43, b: 43 }), // Blackstone slab dark gray
        "minecraft:deepslate_tile_wall" => Some(Color { r: 54, g: 54, b: 54 }), // Deepslate tile wall gray
        "minecraft:lever" => Some(Color { r: 153, g: 126, b: 79 }), // Lever brown

        "minecraft:lightning_rod" => Some(Color { r: 169, g: 169, b: 169 }), // Lightning rod light gray
        "minecraft:lily_of_the_valley" => Some(Color { r: 69, g: 132, b: 51 }), // Lily of the valley green
        "minecraft:warped_wall_sign" => Some(Color { r: 85, g: 141, b: 118 }), // Warped wall sign cyan
        "minecraft:acacia_stairs" => Some(Color { r: 162, g: 123, b: 64 }), // Acacia stairs brown
        "minecraft:jungle_stairs" => Some(Color { r: 102, g: 76, b: 51 }), // Jungle stairs brown
        "minecraft:raw_copper_block" => Some(Color { r: 150, g: 96, b: 70 }), // Raw copper block orange
        "minecraft:oak_sapling" => Some(Color { r: 72, g: 118, b: 16 }), // Oak sapling green
        "minecraft:purpur_stairs" => Some(Color { r: 197, g: 131, b: 197 }), // Purpur stairs magenta
        "minecraft:crimson_fence_gate" => Some(Color { r: 153, g: 40, b: 36 }), // Crimson fence gate red
        "minecraft:oxidized_cut_copper_slab" => Some(Color { r: 127, g: 97, b: 70 }), // Oxidized cut copper slab brown
        "minecraft:orange_stained_glass" => Some(Color { r: 216, g: 127, b: 51 }), // Orange stained glass orange
        "minecraft:polished_granite_slab" => Some(Color { r: 144, g: 118, b: 108 }), // Polished granite slab brown
        "minecraft:brown_carpet" => Some(Color { r: 139, g: 101, b: 66 }), // Brown carpet brown
        "minecraft:blue_bed" => Some(Color { r: 58, g: 71, b: 148 }), // Blue bed blue
        "minecraft:wither_rose" => Some(Color { r: 30, g: 30, b: 30 }), // Wither rose black
        "minecraft:end_stone_brick_stairs" => Some(Color { r: 221, g: 221, b: 165 }), // End stone brick stairs light yellow
        "minecraft:small_dripleaf" => Some(Color { r: 97, g: 122, b: 61 }), // Small dripleaf green
        "minecraft:gray_glazed_terracotta" => Some(Color { r: 96, g: 96, b: 96 }), // Gray glazed terracotta gray
        "minecraft:oak_button" => Some(Color { r: 162, g: 123, b: 64 }), // Oak button brown
        "minecraft:birch_wall_sign" => Some(Color { r: 212, g: 212, b: 212 }), // Birch wall sign white
        "minecraft:waxed_exposed_cut_copper" => Some(Color { r: 151, g: 106, b: 78 }), // Waxed exposed cut copper brown
        "minecraft:nether_brick_stairs" => Some(Color { r: 45, g: 25, b: 26 }), // Nether brick stairs dark red
        "minecraft:stripped_oak_wood" => Some(Color { r: 162, g: 123, b: 64 }), // Stripped oak wood brown
        "minecraft:crafting_table" => Some(Color { r: 123, g: 77, b: 46 }), // Crafting table brown
        "minecraft:lodestone" => Some(Color { r: 101, g: 94, b: 94 }), // Lodestone gray
        "minecraft:beetroots" => Some(Color { r: 140, g: 23, b: 23 }), // Beetroots red
        "minecraft:mangrove_planks" => Some(Color { r: 118, g: 94, b: 63 }), // Mangrove planks brown
        "minecraft:nether_bricks" => Some(Color { r: 45, g: 25, b: 26 }), // Nether bricks dark red
        "minecraft:waxed_cut_copper" => Some(Color { r: 151, g: 106, b: 78 }), // Waxed cut copper brown
        "minecraft:cobbled_deepslate_stairs" => Some(Color { r: 54, g: 54, b: 54 }), // Cobbled deepslate stairs gray
        "minecraft:crimson_trapdoor" => Some(Color { r: 153, g: 40, b: 36 }), // Crimson trapdoor red
        "minecraft:diorite_stairs" => Some(Color { r: 198, g: 198, b: 198 }), // Diorite stairs light gray
        "minecraft:bone_block" => Some(Color { r: 230, g: 230, b: 230 }), // Bone block white
        "minecraft:yellow_bed" => Some(Color { r: 229, g: 148, b: 42 }), // Yellow bed yellow
        "minecraft:polished_diorite_stairs" => Some(Color { r: 198, g: 198, b: 198 }), // Polished diorite stairs light gray
        "minecraft:pink_tulip" => Some(Color { r: 240, g: 134, b: 193 }), // Pink tulip pink
        "minecraft:stripped_warped_hyphae" => Some(Color { r: 85, g: 141, b: 118 }), // Stripped warped hyphae cyan
        "minecraft:red_sandstone_wall" => Some(Color { r: 186, g: 99, b: 44 }), // Red sandstone wall orange
        "minecraft:dark_prismarine_slab" => Some(Color { r: 38, g: 56, b: 60 }), // Dark prismarine slab dark blue-green
        "minecraft:oak_wood" => Some(Color { r: 162, g: 123, b: 64 }), // Oak wood brown
        
        "minecraft:bubble_coral_fan" => Some(Color { r: 85, g: 64, b: 74 }), // Bubble coral fan gray
        "minecraft:blue_stained_glass_pane" => Some(Color { r: 51, g: 76, b: 178 }), // Blue stained glass pane blue
        "minecraft:bubble_coral_block" => Some(Color { r: 119, g: 119, b: 102 }), // Bubble coral block gray
        "minecraft:rose_bush" => Some(Color { r: 255, g: 85, b: 85 }), // Rose bush red
        "minecraft:redstone_block" => Some(Color { r: 150, g: 33, b: 28 }), // Redstone block red
        "minecraft:purple_carpet" => Some(Color { r: 127, g: 63, b: 178 }), // Purple carpet purple
        "minecraft:cyan_concrete_powder" => Some(Color { r: 21, g: 119, b: 136 }), // Cyan concrete powder cyan
        "minecraft:waxed_exposed_copper" => Some(Color { r: 127, g: 97, b: 70 }), // Waxed exposed copper brown
        "minecraft:bamboo_slab" => Some(Color { r: 70, g: 120, b: 28 }), // Bamboo slab green
        "minecraft:light_blue_stained_glass" => Some(Color { r: 102, g: 153, b: 216 }), // Light blue stained glass light blue
        "minecraft:diorite_slab" => Some(Color { r: 198, g: 198, b: 198 }), // Diorite slab light gray
        "minecraft:stripped_jungle_log" => Some(Color { r: 162, g: 132, b: 76 }), // Stripped jungle log brown
        "minecraft:cactus" => Some(Color { r: 57, g: 141, b: 40 }), // Cactus green
        "minecraft:sculk_vein" => Some(Color { r: 51, g: 51, b: 51 }), // Sculk vein gray
        "minecraft:birch_trapdoor" => Some(Color { r: 212, g: 212, b: 212 }), // Birch trapdoor white
        "minecraft:end_rod" => Some(Color { r: 255, g: 255, b: 178 }), // End rod light yellow
        "minecraft:sculk_sensor" => Some(Color { r: 51, g: 51, b: 51 }), // Sculk sensor gray
        "minecraft:green_carpet" => Some(Color { r: 76, g: 127, b: 51 }), // Green carpet green
        "minecraft:piston_head" => Some(Color { r: 150, g: 96, b: 70 }), // Piston head orange
        "minecraft:light_gray_carpet" => Some(Color { r: 171, g: 171, b: 171 }), // Light gray carpet light gray
        "minecraft:acacia_sapling" => Some(Color { r: 162, g: 123, b: 64 }), // Acacia sapling brown
        "minecraft:waxed_oxidized_copper" => Some(Color { r: 127, g: 97, b: 70 }), // Waxed oxidized copper brown
        "minecraft:deepslate_diamond_ore" => Some(Color { r: 88, g: 88, b: 88 }), // Deepslate diamond ore gray
        "minecraft:brown_concrete_powder" => Some(Color { r: 96, g: 59, b: 31 }), // Brown concrete powder brown
        "minecraft:muddy_mangrove_roots" => Some(Color { r: 118, g: 94, b: 63 }), // Muddy mangrove roots brown
        "minecraft:hay_block" => Some(Color { r: 214, g: 182, b: 81 }), // Hay block yellow
        "minecraft:green_stained_glass" => Some(Color { r: 89, g: 125, b: 39 }), // Green stained glass green
        "minecraft:gray_wool" => Some(Color { r: 79, g: 79, b: 79 }), // Gray wool gray
        "minecraft:chiseled_bookshelf" => Some(Color { r: 162, g: 123, b: 64 }), // Chiseled bookshelf brown
        "minecraft:iron_door" => Some(Color { r: 166, g: 166, b: 166 }), // Iron door gray
        "minecraft:stripped_acacia_wood" => Some(Color { r: 162, g: 123, b: 64 }), // Stripped acacia wood brown
        "minecraft:light_blue_stained_glass_pane" => Some(Color { r: 102, g: 153, b: 216 }), // Light blue stained glass pane light blue
        "minecraft:polished_diorite_slab" => Some(Color { r: 198, g: 198, b: 198 }), // Polished diorite slab light gray
        "minecraft:polished_andesite_slab" => Some(Color { r: 139, g: 139, b: 139 }), // Polished andesite slab light gray
        "minecraft:red_tulip" => Some(Color { r: 235, g: 137, b: 137 }), // Red tulip red
        "minecraft:cracked_deepslate_tiles" => Some(Color { r: 63, g: 63, b: 63 }), // Cracked deepslate tiles gray
        "minecraft:candle" => Some(Color { r: 235, g: 208, b: 165 }), // Candle orange
        "minecraft:brick_wall" => Some(Color { r: 152, g: 94, b: 75 }), // Brick wall brown
        "minecraft:coal_block" => Some(Color { r: 26, g: 26, b: 26 }), // Coal block black
        "minecraft:cherry_fence" => Some(Color { r: 152, g: 94, b: 75 }), // Cherry fence brown
        "minecraft:redstone_lamp" => Some(Color { r: 150, g: 33, b: 28 }), // Redstone lamp red
        "minecraft:waxed_weathered_cut_copper_slab" => Some(Color { r: 127, g: 97, b: 70 }), // Waxed weathered cut copper slab brown
        "minecraft:pink_petals" => Some(Color { r: 238, g: 184, b: 207 }), // Pink petals pink
        "minecraft:spawner" => Some(Color { r: 47, g: 47, b: 47 }), // Spawner gray
        "minecraft:diamond_block" => Some(Color { r: 55, g: 206, b: 204 }), // Diamond block cyan
        "minecraft:mangrove_slab" => Some(Color { r: 162, g: 132, b: 76 }), // Mangrove slab brown
        "minecraft:pink_stained_glass_pane" => Some(Color { r: 255, g: 153, b: 160 }), // Pink stained glass pane pink
        "minecraft:crimson_stairs" => Some(Color { r: 136, g: 25, b: 23 }), // Crimson stairs red

        "minecraft:bricks" => Some(Color { r: 142, g: 32, b: 32 }), // Bricks red
        "minecraft:black_concrete_powder" => Some(Color { r: 21, g: 21, b: 26 }), // Black concrete powder black
        "minecraft:chiseled_quartz_block" => Some(Color { r: 223, g: 223, b: 223 }), // Chiseled quartz block white
        "minecraft:stripped_dark_oak_wood" => Some(Color { r: 79, g: 64, b: 34 }), // Stripped dark oak wood brown
        "minecraft:gray_terracotta" => Some(Color { r: 59, g: 64, b: 68 }), // Gray terracotta gray
        "minecraft:white_stained_glass_pane" => Some(Color { r: 255, g: 255, b: 255 }), // White stained glass pane white
        "minecraft:andesite_stairs" => Some(Color { r: 126, g: 126, b: 126 }), // Andesite stairs gray
        "minecraft:mossy_stone_bricks" => Some(Color { r: 103, g: 119, b: 103 }), // Mossy stone bricks green
        "minecraft:nether_brick_slab" => Some(Color { r: 45, g: 22, b: 27 }), // Nether brick slab dark red
        "minecraft:jungle_planks" => Some(Color { r: 153, g: 112, b: 77 }), // Jungle planks brown
        "minecraft:oak_wall_sign" => Some(Color { r: 91, g: 64, b: 38 }), // Oak wall sign brown
        "minecraft:bedrock" => Some(Color { r: 83, g: 83, b: 83 }), // Bedrock dark gray
        "minecraft:blackstone_stairs" => Some(Color { r: 38, g: 38, b: 38 }), // Blackstone stairs black
        "minecraft:stone_button" => Some(Color { r: 123, g: 123, b: 123 }), // Stone button gray
        "minecraft:pink_terracotta" => Some(Color { r: 194, g: 123, b: 133 }), // Pink terracotta pink
        "minecraft:purple_terracotta" => Some(Color { r: 117, g: 69, b: 100 }), // Purple terracotta purple
        "minecraft:birch_stairs" => Some(Color { r: 198, g: 192, b: 172 }), // Birch stairs light brown
        "minecraft:stripped_jungle_wood" => Some(Color { r: 179, g: 128, b: 79 }), // Stripped jungle wood brown
        "minecraft:sandstone_stairs" => Some(Color { r: 218, g: 211, b: 160 }), // Sandstone stairs yellow
        "minecraft:smooth_quartz_stairs" => Some(Color { r: 233, g: 233, b: 233 }), // Smooth quartz stairs white
        "minecraft:brick_stairs" => Some(Color { r: 142, g: 32, b: 32 }), // Brick stairs red
        "minecraft:redstone_torch" => Some(Color { r: 214, g: 44, b: 8 }), // Redstone torch red
        "minecraft:jungle_wood" => Some(Color { r: 153, g: 112, b: 77 }), // Jungle wood brown
        "minecraft:dark_oak_wood" => Some(Color { r: 79, g: 64, b: 34 }), // Dark oak wood brown
        "minecraft:magenta_terracotta" => Some(Color { r: 149, g: 87, b: 108 }), // Magenta terracotta magenta
        "minecraft:blackstone_wall" => Some(Color { r: 38, g: 38, b: 38 }), // Blackstone wall black
        "minecraft:chiseled_sandstone" => Some(Color { r: 218, g: 211, b: 160 }), // Chiseled sandstone yellow
        "minecraft:brick_slab" => Some(Color { r: 142, g: 32, b: 32 }), // Brick slab red
        "minecraft:yellow_stained_glass_pane" => Some(Color { r: 255, g: 255, b: 85 }), // Yellow stained glass pane yellow
        "minecraft:red_nether_brick_stairs" => Some(Color { r: 92, g: 22, b: 20 }), // Red nether brick stairs dark red
        "minecraft:light_blue_concrete" => Some(Color { r: 78, g: 133, b: 162 }), // Light blue concrete light blue
        "minecraft:stone_pressure_plate" => Some(Color { r: 128, g: 128, b: 128 }), // Stone pressure plate gray
        "minecraft:sandstone_slab" => Some(Color { r: 218, g: 211, b: 160 }), // Sandstone slab yellow
        "minecraft:spruce_door" => Some(Color { r: 114, g: 85, b: 47 }), // Spruce door brown
        "minecraft:acacia_wood" => Some(Color { r: 185, g: 142, b: 97 }), // Acacia wood brown
        "minecraft:birch_slab" => Some(Color { r: 198, g: 192, b: 172 }), // Birch slab light brown
        "minecraft:polished_blackstone_stairs" => Some(Color { r: 45, g: 45, b: 45 }), // Polished blackstone stairs black
        "minecraft:dark_oak_door" => Some(Color { r: 79, g: 64, b: 34 }), // Dark oak door brown
        "minecraft:black_carpet" => Some(Color { r: 21, g: 21, b: 26 }), // Black carpet black
        "minecraft:red_carpet" => Some(Color { r: 143, g: 34, b: 34 }), // Red carpet red
        "minecraft:birch_door" => Some(Color { r: 198, g: 192, b: 172 }), // Birch door light brown
        "minecraft:red_bed" => Some(Color { r: 178, g: 22, b: 27 }), // Red bed dark red
        "minecraft:jungle_door" => Some(Color { r: 153, g: 112, b: 77 }), // Jungle door brown

        "minecraft:red_nether_brick_slab" => Some(Color { r: 92, g: 22, b: 20 }), // Red nether brick slab dark red
        "minecraft:brown_wool" => Some(Color { r: 102, g: 76, b: 51 }), // Brown wool brown
        "minecraft:end_stone_bricks" => Some(Color { r: 221, g: 221, b: 163 }), // End stone bricks yellow
        "minecraft:red_nether_bricks" => Some(Color { r: 70, g: 14, b: 15 }), // Red nether bricks dark red
        "minecraft:oak_pressure_plate" => Some(Color { r: 128, g: 96, b: 58 }), // Oak pressure plate brown
        "minecraft:white_carpet" => Some(Color { r: 238, g: 238, b: 238 }), // White carpet white
        "minecraft:granite_slab" => Some(Color { r: 134, g: 81, b: 63 }), // Granite slab brown
        "minecraft:redstone_wall_torch" => Some(Color { r: 214, g: 44, b: 8 }), // Redstone wall torch red
        "minecraft:daylight_detector" => Some(Color { r: 214, g: 189, b: 124 }), // Daylight detector light brown
        "minecraft:crimson_nylium" => Some(Color { r: 141, g: 24, b: 31 }), // Crimson nylium dark red
        "minecraft:crimson_hyphae" => Some(Color { r: 144, g: 46, b: 46 }), // Crimson hyphae dark red
        "minecraft:diorite_wall" => Some(Color { r: 208, g: 208, b: 208 }), // Diorite wall white
        "minecraft:cornflower" => Some(Color { r: 70, g: 118, b: 179 }), // Cornflower blue
        "minecraft:granite_stairs" => Some(Color { r: 134, g: 81, b: 63 }), // Granite stairs brown
        "minecraft:blue_orchid" => Some(Color { r: 54, g: 144, b: 227 }), // Blue orchid blue
        "minecraft:weeping_vines_plant" => Some(Color { r: 71, g: 121, b: 69 }), // Weeping vines plant green
        "minecraft:potted_bamboo" => Some(Color { r: 65, g: 107, b: 35 }), // Potted bamboo green
        "minecraft:netherite_block" => Some(Color { r: 41, g: 41, b: 41 }), // Netherite block dark gray
        "minecraft:blue_wool" => Some(Color { r: 51, g: 76, b: 178 }), // Blue wool blue
        "minecraft:red_mushroom" => Some(Color { r: 181, g: 67, b: 68 }), // Red mushroom red
        "minecraft:smooth_sandstone_stairs" => Some(Color { r: 218, g: 211, b: 160 }), // Smooth sandstone stairs yellow
        "minecraft:jungle_fence" => Some(Color { r: 153, g: 112, b: 77 }), // Jungle fence brown
        "minecraft:lime_wool" => Some(Color { r: 112, g: 185, b: 26 }), // Lime wool lime
        "minecraft:spruce_button" => Some(Color { r: 91, g: 64, b: 38 }), // Spruce button brown
        "minecraft:dead_tube_coral_block" => Some(Color { r: 112, g: 112, b: 112 }), // Dead tube coral block light gray
        "minecraft:nether_brick_wall" => Some(Color { r: 45, g: 22, b: 27 }), // Nether brick wall dark red
        "minecraft:red_sandstone_stairs" => Some(Color { r: 173, g: 94, b: 37 }), // Red sandstone stairs orange
        "minecraft:warped_slab" => Some(Color { r: 48, g: 148, b: 134 }), // Warped slab teal
        "minecraft:brown_stained_glass_pane" => Some(Color { r: 102, g: 76, b: 51 }), // Brown stained glass pane brown
        "minecraft:acacia_fence" => Some(Color { r: 185, g: 142, b: 97 }), // Acacia fence brown
        "minecraft:purpur_block" => Some(Color { r: 193, g: 141, b: 199 }), // Purpur block purple
        "minecraft:red_sandstone_slab" => Some(Color { r: 173, g: 94, b: 37 }), // Red sandstone slab orange
        "minecraft:red_concrete_powder" => Some(Color { r: 142, g: 33, b: 33 }), // Red concrete powder red
        "minecraft:ancient_debris" => Some(Color { r: 85, g: 69, b: 69 }), // Ancient debris brown
        "minecraft:red_stained_glass_pane" => Some(Color { r: 142, g: 33, b: 33 }), // Red stained glass pane red
        "minecraft:heavy_weighted_pressure_plate" => Some(Color { r: 173, g: 173, b: 173 }), // Heavy weighted pressure plate gray
        "minecraft:white_bed" => Some(Color { r: 222, g: 222, b: 222 }), // White bed white
        "minecraft:infested_stone_bricks" => Some(Color { r: 119, g: 119, b: 119 }), // Infested stone bricks light gray
        "minecraft:dark_oak_button" => Some(Color { r: 79, g: 64, b: 34 }), // Dark oak button brown
        "minecraft:petrified_oak_slab" => Some(Color { r: 128, g: 96, b: 58 }), // Petrified oak slab brown
        "minecraft:orange_tulip" => Some(Color { r: 240, g: 138, b: 43 }), // Orange tulip orange
        "minecraft:blue_carpet" => Some(Color { r: 52, g: 84, b: 198 }), // Blue carpet blue
        "minecraft:mushroom_stem" => Some(Color { r: 203, g: 188, b: 174 }), // Mushroom stem light gray
        "minecraft:black_bed" => Some(Color { r: 21, g: 21, b: 26 }), // Black bed black
        "minecraft:smooth_sandstone_slab" => Some(Color { r: 218, g: 211, b: 160 }), // Smooth sandstone slab yellow
        "minecraft:red_nether_brick_wall" => Some(Color { r: 70, g: 14, b: 15 }), // Red nether brick wall dark red
        "minecraft:crimson_slab" => Some(Color { r: 141, g: 24, b: 31 }), // Crimson slab dark red
        "minecraft:cyan_carpet" => Some(Color { r: 21, g: 119, b: 136 }), // Cyan carpet cyan
        
        // // _ => Some(Color {r: 255, g: 0, b: 0}), // Unknown block ID
        // id => {
        //     // println!("unknown id: {}", id);
        //     let mut map = MAP.get_or_init(|| Arc::new(Mutex::new(HashMap::new()))).lock().unwrap();
        //     if let std::collections::hash_map::Entry::Vacant(e) = map.entry(String::from(id)) {
        //         e.insert(0);
        //     } else {
        //         *map.get_mut(&String::from(id)).unwrap() += 1;
        //     }
        //     Some(Color {r: 100, g: 100, b: 100}) // Unknown block ID
        // }
        _ => Some(Color {r: 100, g: 100, b: 100}), // Unknown block ID
         
    }
}
