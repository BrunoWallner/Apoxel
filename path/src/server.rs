// use std::thread::sleep;
// use std::time::Duration;

// use async_std::net::TcpStream;
// use async_std::task;
// use common::channel::*;
// use common::event::*;
// use common::io::*;
// use common::User;

// const IP: &str = "localhost:8000";

// async fn get_communicator() -> (OutputCommunicator, InputCommunicator) {
//     loop {
//         let Ok(stream) = TcpStream::connect(IP).await else {
//             sleep(Duration::from_secs(1));
//             continue;
//         };

//         return communicator::init(stream);
//     }
// }

// pub fn allocated() -> usize {
//     server::allocated()
// }

// pub fn start() -> Result<(Receiver<STC>, Sender<CTS>), ()> {
//     let (sucess_tx, sucess_rx) = channel(None);
//     task::spawn(async move {
//         match server::start().await {
//             Ok(()) => sucess_tx.send(true),
//             Err(()) => sucess_tx.send(false),
//         }
//     });

//     let Some(sucess) = sucess_rx.recv() else {
//         return Err(());
//     };
//     if !sucess {
//         return Err(());
//     }

//     let (output, mut input) = task::block_on(async { get_communicator().await });

//     let (input_sender, input_receiver) = channel::<STC>(None);
//     let (output_sender, output_receiver) = channel::<CTS>(None);
//     // let ()

//     task::spawn(async move {
//         while let Ok(Event::STC(incoming)) = input.get_event().await {
//             if input_sender.send(incoming).is_err() {
//                 panic!("failed to send Server event");
//             }
//         }
//     });

//     task::spawn(async move {
//         while let Some(outgoing) = output_receiver.recv() {
//             output.send_cts(outgoing).await;
//         }
//     });

//     Ok((input_receiver, output_sender))
// }

// pub fn login(input_receiver: &Receiver<STC>, output_sender: &Sender<CTS>) -> Result<User, ()> {
//     output_sender
//         .send(CTS::Register {
//             name: String::from("ballner"),
//         })
//         .unwrap();
//     let Some(STC::Token(token)) = input_receiver.recv() else {
//         return Err(());
//     };
//     output_sender.send(CTS::Login { token }).unwrap();
//     let Some(STC::LoggedIn(user_login)) = input_receiver.recv() else {
//         return Err(());
//     };

//     Ok(user_login)
// }
