use std::mem;

use common::blocks::Block;
use common::chunk::Chunk;
use common::types::octree::Corner;
use common::types::octree::Node;
use common::Coord;

const CHUNK_HEADER_SIZE: u32 = 4;

pub fn serialize_chunk(chunk: Chunk, coord: Coord) -> Vec<u8> {
    let now = std::time::Instant::now();

    let mut bytes = ByteArray::new();

    let root = chunk.get_root_node().clone();
    let Some(data) = serialize_node(&root, CHUNK_HEADER_SIZE) else {return Vec::new()};
    let x = coord[0] as i32;
    let y = coord[1] as i32;
    let z = coord[2] as i32;

    bytes.write_u32(data.len() as u32 + CHUNK_HEADER_SIZE); // length
    bytes.write_i32(x);
    bytes.write_i32(y);
    bytes.write_i32(z);
    bytes.append(data);

    let data = bytes.serialize();

    log::info!("chunk serialization took: {:#?}", now.elapsed());

    data
}

pub const NODE_SIZE: u32 = 10;
// All nodes are 40 bytes (10 u32's and 320 bits) aligned
// Group Node memory
// [type, 1][octant, 3][child_mask, 8][padding, 20]
// [parent_index, 32]
// [child_indices, 32 * 8]

// Leaf Node memory
// [type, 1][octant, 3][kind, 4][already_lit, 1][padding, 25]
// [parent_index, 32]
// [color, 32]
// [lighting, 32 * 3]
// [padding, 32 * 3]


// Update Memory
// [is_there, 1][type, 2][padding, 29]
// [position, 32 * 3]


fn serialize_node(node: &Node<Block>, index: u32) -> Option<Vec<u32>> {
    let mut bytes = ByteArray::new();

    match node {
        Node::Group(g) => {
            bytes.write_u32(1); // header with type
            bytes.write_u32(0); // parent index, is set by parent
            bytes.fill(8); // all child indices

            let mut child_index = index + NODE_SIZE;
            let mut has_child = false;
            for octant in Corner::all() {
                let Some(child) = serialize_node(&g[octant as usize], child_index) else {
                    continue;
                };
                let mut child = ByteArray::from(child);
                has_child = true;

                let relative_child_index_position = 2 + octant as usize;
                bytes.set_u32_at(child_index, relative_child_index_position);

                child.apply_or_u32_at((octant as u32 & 7) << 1, 0); // write child octant
                child.set_u32_at(index, 1); // write parent_index to child

                bytes.apply_or_u32_at(1 << (octant as u32 + 4), 0); // write child mask

                child_index += child.len() as u32;
                bytes.append(child.data());
            }
            if !has_child {
                return None;
            }
        }
        Node::Filled(block) => {
            let kind = match block {
                Block::Color(_) => 0,
                Block::Light(..) => 1,
                _ => 2,
            };
            let kind = (kind & 0xF) << 4;
            bytes.write_u32(kind); // header with type, type = 0
            bytes.write_u32(0);    // parent index, is set by parent

            let c = block.to_color();
            // let light = block.to_light();
            let light = match block {
                Block::Color(_) => [0.0; 3],
                Block::Light(r, g, b) => [*r, *g, *b],
                _ => [0.0; 3],
            };
            let light = unsafe {mem::transmute::<[f32; 3], [u32; 3]>(light)};
            let color = (c.b as u32) << 24 | (c.g as u32) << 16 | (c.r as u32) << 8;

            bytes.write_u32(color);
            // bytes.write_u32(u32::MAX); // lighting, calculated on gpu
            bytes.write_u32(light[0]);
            bytes.write_u32(light[1]);
            bytes.write_u32(light[2]);
            bytes.fill(6); // padding
        }
        Node::Empty => return None,
    }

    Some(bytes.data())
}

#[derive(Debug)]
struct ByteArray {
    data: Vec<u32>,
}
impl ByteArray {
    pub fn new() -> Self {
        Self { data: Vec::new() }
    }

    pub fn from(data: Vec<u32>) -> Self {
        Self { data }
    }

    pub fn data(&self) -> Vec<u32> {
        self.data.clone()
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn write_u32(&mut self, value: u32) {
        self.data.push(value);
    }

    pub fn set_u32_at(&mut self, value: u32, index: usize) {
        self.data[index] = value;
    }

    pub fn fill(&mut self, length: usize) {
        for _ in 0..length {
            self.write_u32(0);
        }
    }

    pub fn apply_or_u32_at(&mut self, value: u32, index: usize) {
        self.data[index] |= value;
    }

    pub fn serialize(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        for b in &self.data {
            bytes.append(&mut Vec::from(b.to_le_bytes()));
        }

        bytes
    }

    pub fn write_i32(&mut self, value: i32) {
        self.data.push(u32::from_le_bytes(value.to_le_bytes()));
    }

    pub fn append(&mut self, bytes: Vec<u32>) {
        for b in bytes {
            self.data.push(b);
        }
    }
}
