use crate::importer;

use super::{
    camera::Camera, chunk::ChunkManager, core::Core, entry::{Entry, Group, Storage}, Pass
};

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vars {
    random_seed: u32,
    pub to_remove: u32,
}

pub struct EditPass {
    pub pass: Pass,
    pub vars: Vars,

    camera_storage: Storage,
    var_storage: Storage,
}
impl EditPass {
    pub fn new(core: &Core, chunks: &ChunkManager) -> Self {
        let camera_storage = Storage::new(
            &core.device,
            32,
            true,
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        let var_storage = Storage::new(
            &core.device,
            32,
            false,
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        let shader = importer::shader_from_folder(
            "./shader/compute/",
            "edit",
            &["noise", "ray", "aabb", "chunks"],
        );

        // let entries = vec![vec![
        //     Entry::from(&camera_storage),
        //     Entry::from(&var_storage),
        //     present_texture,
        // ]];
        // let g1 = Group::get(&core.device, &[chunks], wgpu::ShaderStages::COMPUTE);
        let g2 = Group::get(
            &core.device,
            &[&camera_storage, &var_storage],
            wgpu::ShaderStages::COMPUTE,
        );
        let entry = Entry::from_groups(vec![chunks.get_group(core), g2]);

        let pass = Pass::new(core, entry, shader, "main");

        let vars = Vars {
            random_seed: 0,
            to_remove: 0,
        };

        Self {
            camera_storage,
            var_storage,
            pass,
            vars,
        }
    }

    pub fn update(&mut self, core: &Core, _dt: f32, camera: &Camera) {
        // let aspect_ratio = core.size.width as f32 / core.size.height as f32;
        core.queue.write_buffer(
            &self.camera_storage.buffer,
            0,
            bytemuck::cast_slice(&[*camera]),
        );

        self.vars.random_seed += 1;

        core.queue.write_buffer(
            &self.var_storage.buffer,
            0,
            &bytemuck::cast_slice(&[self.vars]),
        );

        self.vars.to_remove = 0;
    }

    pub fn reload(&mut self, core: &Core, chunks: &ChunkManager) {
        let shader = importer::shader_from_folder(
            "./shader/compute/",
            "edit",
            &["noise", "ray", "aabb", "chunks"],
        );

        // let g1 = Group::get(&core.device, &[chunks], wgpu::ShaderStages::COMPUTE);
        let g2 = Group::get(
            &core.device,
            &[&self.camera_storage, &self.var_storage],
            wgpu::ShaderStages::COMPUTE,
        );
        let entry = Entry::from_groups(vec![chunks.get_group(core), g2]);

        self.pass = Pass::new(core, entry, shader, "main");
    }

    pub fn remove_block(&mut self) {
        self.vars.to_remove = 1;
    }

    pub fn add_light(&mut self) {
        self.vars.to_remove = 2;
    }
}
