use wgpu::util::DeviceExt;

use super::EntryTrait;

// only D2 TextureDimension implemented
pub struct Storage {
    pub buffer: wgpu::Buffer,
    read_only: bool,
}
impl Storage {
    pub fn new(
        device: &wgpu::Device,
        size: u64,
        read_only: bool,
        usage: wgpu::BufferUsages,
    ) -> Self {
        let buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size,
            usage,
            mapped_at_creation: false,
        });

        Self { buffer, read_only }
    }

    pub fn new_init(
        device: &wgpu::Device,
        contents: &[u8], 
        read_only: bool,
        usage: wgpu::BufferUsages,
    ) -> Self {
        let buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: None,
            usage,
            contents
        });

        Self { buffer, read_only }
    }
    
}
impl EntryTrait for Storage {
    fn binding_type(&self) -> wgpu::BindingType {
        wgpu::BindingType::Buffer {
            ty: wgpu::BufferBindingType::Storage {
                read_only: self.read_only,
            },
            has_dynamic_offset: false,
            min_binding_size: None,
        }
    }

    fn binding_resource(&self) -> wgpu::BindingResource {
        self.buffer.as_entire_binding()
    }
}
