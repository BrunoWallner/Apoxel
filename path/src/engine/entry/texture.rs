use super::EntryTrait;

// only D2 TextureDimension implemented
pub struct Texture {
    pub texture: wgpu::Texture,
    pub texture_view: wgpu::TextureView,

    access: wgpu::StorageTextureAccess,
}
impl Texture {
    pub fn new(
        device: &wgpu::Device,
        resolution: [u32; 2],
        format: wgpu::TextureFormat,
        usage: wgpu::TextureUsages,
        access: wgpu::StorageTextureAccess,
    ) -> Self {
        let texture_desc = wgpu::TextureDescriptor {
            size: wgpu::Extent3d {
                width: resolution[0],
                height: resolution[1],
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            label: None,
            usage,
            view_formats: &[],
        };
        let texture = device.create_texture(&texture_desc);

        Self::from_texture(texture, access)
    }

    pub fn from_texture(texture: wgpu::Texture, access: wgpu::StorageTextureAccess) -> Self {
        let texture_view = texture.create_view(&wgpu::TextureViewDescriptor::default());

        Self {
            texture,
            access,
            texture_view,
        }
    }
}
impl EntryTrait for Texture {
    fn binding_type(&self) -> wgpu::BindingType {
        wgpu::BindingType::StorageTexture {
            access: self.access,
            format: self.texture.format(),
            view_dimension: wgpu::TextureViewDimension::D2,
        }
    }

    fn binding_resource(&self) -> wgpu::BindingResource {
        wgpu::BindingResource::TextureView(&self.texture_view)
    }
}
