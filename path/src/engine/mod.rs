pub mod camera;

mod chunk;
mod core;
mod edit_pass;
mod entry;
mod gui;
mod main_pass;
mod pass;
mod render;


use std::sync::Arc;
use winit::window::Window;

use pass::Pass;

use crate::app::GuiTrait;

use edit_pass::EditPass;
use main_pass::MainPass;

pub fn compute_work_group_count(
    (width, height): (u32, u32),
    (workgroup_width, workgroup_height): (u32, u32),
) -> [u32; 3] {
    let x = (width + workgroup_width - 1) / workgroup_width;
    let y = (height + workgroup_height - 1) / workgroup_height;

    [x, y, 1]
}

pub struct Engine<'a> {
    pub chunks: chunk::ChunkManager,
    pub core: core::Core<'a>,
    pub camera: camera::CameraController,
    pub render: render::Render,

    pub main_pass: MainPass,
    // pub edit_pass: EditPass,
}
impl<'a> Engine<'a> {
    pub async fn new(window: Arc<Window>, render_resolution: [u32; 2]) -> Self {
        let core = core::Core::new(&window, render_resolution).await;
        let chunks = chunk::ChunkManager::new(&core.device);
        let camera = camera::CameraController::new();

        let egui = gui::EguiRenderer::new(
            &core.device,       // wgpu Device
            core.config.format, // TextureFormat
            None,               // this can be None
            1,                  // samples
            &window,            // winit Window
        );

        let main_pass = MainPass::new(&core, &chunks);
        // let edit_pass = EditPass::new(&core, &chunks);

        let render = render::Render::new(&core, egui);

        let engine = Self {
            core,
            chunks,
            camera,
            render,
            main_pass,
            // edit_pass,
        };

        engine
    }

    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.core.size = new_size;
            self.core.config.width = new_size.width;
            self.core.config.height = new_size.height;
            self.core
                .surface
                .configure(&self.core.device, &self.core.config);
        }
    }

    pub fn update(&mut self) {
        self.chunks.update(&self.core);
    }

    pub fn render<F: GuiTrait>(&mut self, dt: f32, gui: &mut F) -> Result<(), wgpu::SurfaceError> {
        let dispatch_size = compute_work_group_count(
            (
                self.core.render_resolution[0],
                self.core.render_resolution[1],
            ),
            (16, 16),
        );
        self.main_pass.update(&self.core, dt, &self.camera.camera);
        self.main_pass.main_pass.compute(&self.core, dispatch_size);
        self.main_pass.lighting_pass.compute(&self.core, dispatch_size);
        self.main_pass.finalize_pass.compute(&self.core, dispatch_size);

        // self.edit_pass.update(&self.core, dt, &self.camera.camera);
        // self.edit_pass.pass.compute(&self.core, [1, 1, 1]);

        self.render.present(&self.core, gui)
    }

    pub fn reload(&mut self) {
        self.main_pass
            .reload(&self.core,  &self.chunks);
        // self.edit_pass.reload(&self.core, &self.chunks);
    }
}
