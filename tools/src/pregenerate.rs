use storage::chunks::ChunkStorage;
use storage::chunks::StoredChunk;

use common::chunk::Chunk;
use common::Coord;
use generation::foliage;
use generation::generate;

use common::channel::{channel, Receiver, Sender};

use std::thread;
use threadpool::ThreadPool;

const SAVE_INTERVALL: u64 = 5000;
const FEEDBACK_INTERVALL: u64 = 300;

pub fn pregenerate(xz: i64, y: i64, seed: u32, path: String) {
    let orig_y = y;
    foliage::generate(seed);
    let (tx, rx) = channel(Some(SAVE_INTERVALL as usize));
    let (finish_tx, finish_rx) = channel(Some(1));

    let total_chunks = (xz * 2 + 1).pow(2) * (orig_y * 2 + 1);
    init_save(rx, finish_tx, path, total_chunks);

    let threadpool = ThreadPool::new(common::cpu_count());
    let mut progress: u64 = 0;
    for x in -xz..=xz {
        for z in -xz..=xz {
            for y in -y..=y {
                progress += 1;
                let tx_clone = tx.clone();
                threadpool.execute(move || {
                    let chunk = Chunk::default();
                    let super_chunk = generate(chunk, seed, [x, y, z]);
                    for (coord, chunk) in super_chunk.chunks.into_iter() {
                        let coord: [i64; 3] = [coord[0] + x, coord[1] + y, coord[2] + z];

                        let mut stored_chunk = StoredChunk::new(chunk);
                        stored_chunk.modified = true;
                        stored_chunk.fully_loaded = true;

                        tx_clone
                            .send((coord, stored_chunk, progress), true)
                            .unwrap();
                    }
                });
            }
        }
    }
    drop(tx);
    let _ = finish_rx.recv();
}

fn init_save(
    rx: Receiver<(Coord, StoredChunk, u64)>,
    finish_tx: Sender<bool>,
    path: String,
    total_chunks: i64,
) {
    let mut chunks = ChunkStorage::new(path);
    thread::spawn(move || {
        while let Some((coord, mut chunk, progress)) = rx.recv() {
            if let Some(left) = chunks.get(&coord) {
                chunk.chunk.merge(&left.chunk, None);
            }

            chunks.insert(coord, chunk);
            chunks.unload(&coord).unwrap();

            if progress % SAVE_INTERVALL == 0 {
                println!("saving...");
                chunks.save().unwrap();
            }
            if progress % FEEDBACK_INTERVALL == 0 {
                let progress: f32 = progress as f32 / total_chunks as f32;
                println!("{:.4}%", progress * 100.0);
            }
        }
        println!("saving...");
        chunks.save().unwrap();
        finish_tx.send(true, true).unwrap();
        println!("done");
    });
}
